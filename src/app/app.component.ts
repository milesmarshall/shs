import { Component, ViewContainerRef, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';

import { IncidentsService } from '../app/shared/services/incidents.service';
import { LookupsService } from '../app/shared/services/lookups.service';
import { AssetsService } from '../app/shared/services/assets.service';
import { ProvidersService } from '../app/shared/services/providers.service';
import { UsersService } from '../app/shared/services/users.service';
import { ModalsService } from '../app/shared/services/modals.service';

import { SITES } from 'app/app.constants';
import { CATEGORIES } from 'app/app.constants';
import { RGRESOURCES } from 'app/app.constants';
import { RESOURCES } from 'app/app.constants';
import { CUSTOMFIELDS } from 'app/app.constants';
import { IMPACTS } from 'app/app.constants';
import { ROOTCAUSES } from 'app/app.constants';
import { REPORTS } from 'app/app.constants';
import { PREFERENCES } from 'app/app.constants';
import { USERTYPES } from 'app/app.constants';
import { FIELDNAMES } from 'app/app.constants';
import { STATUSES } from 'app/app.constants';

import { INCIDENTS } from 'app/app.constants';
import { INCIDENTS_TODAY } from 'app/app.constants';
import { INCIDENTS_OPEN } from 'app/app.constants';
import { INCIDENTS_HOLD } from 'app/app.constants';
import { INCIDENTS_LENGTH } from 'app/app.constants';

import { INCIDENT_TYPES } from 'app/app.constants';

import { INCIDENT_STATS } from 'app/app.constants';
import { INCIDENT_STATS_LENGTH } from 'app/app.constants';

import { ASSETS_FIXED } from 'app/app.constants';
import { ASSETS_PEOPLE } from 'app/app.constants';
import { ASSETS_FIXED_LENGTH } from 'app/app.constants';
import { ASSETS_PEOPLE_LENGTH } from 'app/app.constants';

import { SERVICE_PROVIDERS } from 'app/app.constants';
import { SERVICE_PROVIDERS_LENGTH }  from 'app/app.constants';
import { AuthService } from './shared/auth/auth.service';
import { RootCauseVO } from './shared/models/rootCauseVO.model';
import { StatusVO } from './shared/models/statusVO.model';
import { ResourceVO } from './shared/models/resourceVO.model';
import { SiteVO } from './shared/models/siteVO.model';
import { PreferencesVO } from './shared/models/preferencesVO.model';
import { CategoryVO } from './shared/models/categoryVO.model';
import { ImpactVO } from './shared/models/impactVO.model';


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent implements OnInit, OnDestroy {

    subscription: Subscription;
    articles;
    users;
    error = '';
    message = null;

    sites = SITES;
    categories = CATEGORIES;
    rgResources = RGRESOURCES;
    resources = RESOURCES;
    customFields = CUSTOMFIELDS;
    impacts = IMPACTS;
    rootCauses = ROOTCAUSES;
    reports = REPORTS;
    preferences = PREFERENCES;
    userTypes = USERTYPES;
    fieldNames = FIELDNAMES;
    statuses = STATUSES;

    incidents = INCIDENTS;
    incidents_today = INCIDENTS_TODAY;
    incidents_open = INCIDENTS_OPEN;
    incidents_hold = INCIDENTS_HOLD;
    incidents_length = INCIDENTS_LENGTH;

    incident_stats = INCIDENT_STATS;
    incident_stats_length = INCIDENT_STATS_LENGTH;

    incident_types = INCIDENT_TYPES;

    assets_fixed = ASSETS_FIXED;
    assets_people = ASSETS_PEOPLE;
    assets_fixed_length = ASSETS_FIXED_LENGTH;
    assets_people_length = ASSETS_PEOPLE_LENGTH;

    service_providers = SERVICE_PROVIDERS;
    service_providers_length = SERVICE_PROVIDERS_LENGTH;

    constructor(
        private router: Router,
        private incidentsService: IncidentsService,
        private lookupsService: LookupsService,
        private assetsService: AssetsService,
        private providersService: ProvidersService,
        private usersService: UsersService,
        private modalsService: ModalsService,
        private authService: AuthService
        ) {
    }

    ngOnInit() {


        console.log('APP COMPONENT - ngOnit--------------------------------');

        // check localStorage for stored userData & auto login if sessionToken still valid
        this.authService.autoLogin();

        this.subscription = this.router.events
            .pipe(
                filter(event => event instanceof NavigationEnd)
            )
            .subscribe(() => window.scrollTo(0, 0));


        // get incidents - MJA - TO DO. THIS NEEDS TO MOVE TO AFTER LOGIN
        // MJ ok but what about when you return back to eh dashboard or the listing page?
        this.incidentsService.getIncidents(true, 1, 1).subscribe((data) => {
            console.log('[inside AppComponent] call to get incidents - this needs to move. Cannot retrieve incidents BEFORE logged in.');
            this.incidents.push(data);

            sessionStorage.setItem('incidents', JSON.stringify(this.incidents));
            sessionStorage.setItem('incident_stats', JSON.stringify(this.incidents[0]['stats']));
        });

        // get incident stats
        /*this.incidentsService.getIncidents('000',40,true,1,0).subscribe((data)=>{
            this.incident_stats.push(data);
            //this.incident_stats_length = this.incident_stats[0]['result']['source'].length;

            sessionStorage.setItem('incident_stats', JSON.stringify(this.incident_stats));
        });*/

        // this.incidents_length = this.incidentsService.getTotalIncidents(this.incidents)
        // console.log('incident length:' + this.incidents_length);

        // get assets fixed MJA - TO DO. THIS NEEDS TO MOVE TO AFTER LOGIN
        this.assetsService.getAssetsFixed('000',40,0,1,'assetsFixed').subscribe((data)=>{
            this.assets_fixed.push(data);
            //this.assets_fixed_length = this.assets_fixed[0]['result'].length;

            sessionStorage.setItem('assets_fixed', JSON.stringify(this.assets_fixed));
        });

        // get assets people MJA - TO DO. THIS NEEDS TO MOVE TO AFTER LOGIN
        this.assetsService.getAssetsPeople('000',40,0,1,'assetsPeople').subscribe((data)=>{
            this.assets_people.push(data);
            //this.assets_people_length = this.assets_people[0]['result'].length;

            sessionStorage.setItem('assets_people', JSON.stringify(this.assets_people));
        });

        // get service providers MJA - TO DO. THIS NEEDS TO MOVE TO AFTER LOGIN
        this.providersService.getServiceProviders('000',0).subscribe((data)=>{
            this.service_providers.push(data);

            sessionStorage.setItem('service_providers', JSON.stringify(this.service_providers));
        });

        // get level 1 sites MJA - TO DO. THIS NEEDS TO MOVE TO AFTER LOGIN
        this.lookupsService.getLookups('sites', '0').subscribe((responseData) => {

            if (responseData.hasOwnProperty('result')) {
                const allRecords = [];

                // convert js responsedata to array of typed model objects
                for (let i = 0; i < responseData['result'].length; i++) {
                  const jsonObj = responseData['result'][i];
                  const newRecord = new SiteVO().deserialize(jsonObj);
                  allRecords.push(newRecord);
                }

                this.sites = allRecords;
            }

            sessionStorage.setItem('sites', JSON.stringify(this.sites));
        });

        // get level 1 categories MJA - TO DO. THIS NEEDS TO MOVE TO AFTER LOGIN
        this.lookupsService.getLookups('categories', '0').subscribe((responseData) => {

            if (responseData.hasOwnProperty('result')) {
                const allRecords = [];

                // convert js responsedata to array of typed model objects
                for (let i = 0; i < responseData['result'].length; i++) {
                  const jsonObj = responseData['result'][i];
                  const newRecord = new CategoryVO().deserialize(jsonObj);
                  allRecords.push(newRecord);
                }

                this.categories = allRecords;
            }

            sessionStorage.setItem('categories', JSON.stringify(this.categories));
        });

        // get ResourceGroupResources MJA - TO DO. THIS NEEDS TO MOVE TO AFTER LOGIN
        this.lookupsService.getLookups('rgResources').subscribe((responseData) => {
            // this.rgResources.push(responseData);

            if (responseData.hasOwnProperty('result')) {
                const allRecords = [];

                // convert js responsedata to array of typed model objects
                for (let i = 0; i < responseData['result'].length; i++) {
                  const jsonObj = responseData['result'][i];
                  const newRecord = new ResourceVO().deserialize(jsonObj);
                  allRecords.push(newRecord);
                }

                this.rgResources = allRecords;
            }

            sessionStorage.setItem('rgResources', JSON.stringify(this.rgResources));
        });

        // get resources MJA - TO DO. THIS NEEDS TO MOVE TO AFTER LOGIN
        this.lookupsService.getLookups('resources').subscribe((responseData) => {
            if (responseData.hasOwnProperty('result')) {
                const allRecords = [];

                // convert js responsedata to array of resource model objects
                for (let i = 0; i < responseData['result'].length; i++) {
                  const jsonObj = responseData['result'][i];
                  const newRecord = new ResourceVO().deserialize(jsonObj);
                  allRecords.push(newRecord);
                }

                this.resources = allRecords;
            }

            sessionStorage.setItem('resources', JSON.stringify(this.resources));
        });

        // get customFields MJA - TO DO. THIS NEEDS TO MOVE TO AFTER LOGIN
        this.lookupsService.getLookups('customFields').subscribe((data) => {
            this.customFields.push(data);

            sessionStorage.setItem('customFields', JSON.stringify(this.customFields));
        });

        // get impacts MJA - TO DO. THIS NEEDS TO MOVE TO AFTER LOGIN
        this.lookupsService.getLookups('impacts').subscribe((data) => {
            this.impacts.push(data);

            sessionStorage.setItem('impacts', JSON.stringify(this.impacts));
        });

        // get rootCauses & save to sessionStorage MJA - TO DO. THIS NEEDS TO MOVE TO AFTER LOGIN
        this.lookupsService.getLookups('rootCauses').subscribe((responseData) => {

            if (responseData.hasOwnProperty('result')) {
                const allRootCauses = [];

                for (let i = 0; i < responseData['result'].length; i++) {
                  const jsonObj = responseData['result'][i];
                  const newRootCause = new RootCauseVO().deserialize(jsonObj);
                  allRootCauses.push(newRootCause);
                }

                this.rootCauses = allRootCauses;
            }

            sessionStorage.setItem('rootCauses', JSON.stringify(this.rootCauses));
        });

        // get reports MJA - TO DO. THIS NEEDS TO MOVE TO AFTER LOGIN
        this.lookupsService.getLookups('reports').subscribe((data) => {
            this.reports.push(data);

            sessionStorage.setItem('reports', JSON.stringify(this.reports));
        });

        // LOAD LOOKUP DATA - NOT FILTERED ON SECURITY ACCESS
        // prefs, impacts, status, userTypes, fieldNames, serviceProviders, eventTypes, assetTypes, thirdPartyClients, thirdPartyClientPushNotifications

        // get preferences
        this.lookupsService.getLookups('preferences').subscribe((responseData) => {

            if (responseData.hasOwnProperty('result')) {
                const jsonObj = responseData['result'];
                this.preferences = new PreferencesVO().deserialize(jsonObj);
            }

            sessionStorage.setItem('preferences', JSON.stringify(this.preferences));
        });

        // get impacts
        this.lookupsService.getLookups('impacts').subscribe((responseData) => {

            if (responseData.hasOwnProperty('result')) {
                const allRecords = [];

                for (let i = 0; i < responseData['result'].length; i++) {
                  const jsonObj = responseData['result'][i];
                  const newRecord = new ImpactVO().deserialize(jsonObj);
                  allRecords.push(newRecord);
                }

                this.impacts = allRecords;
            }

            sessionStorage.setItem('impacts', JSON.stringify(this.impacts));
        });

        // get userTypes
        /* this.lookupsService.getLookups('userTypes').subscribe((data) => {
            this.userTypes.push(data);

            sessionStorage.setItem('userTypes', JSON.stringify(this.userTypes));
        }); */

        // get fieldNames
        /* this.lookupsService.getLookups('000','0','fieldNames').subscribe((data) => {
            this.fieldNames.push(data);

            sessionStorage.setItem('fieldNames', JSON.stringify(this.fieldNames));
        }); */

        // get statuses & save to sessionStorage
        this.lookupsService.getLookups('status').subscribe((responseData) => {
            if (responseData.hasOwnProperty('result')) {
                const allRecords = [];

                for (let i = 0; i < responseData['result'].length; i++) {
                  const jsonObj = responseData['result'][i];
                  const newRecord = new StatusVO().deserialize(jsonObj);
                  allRecords.push(newRecord);
                }

                this.statuses = allRecords;
            }

            sessionStorage.setItem('statuses', JSON.stringify(this.statuses));
        });

        // get incident types
        this.incident_types = this.lookupsService.getIncidentTypes();
    }

    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

}
