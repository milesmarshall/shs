import { environment } from '../environments/environment';
import { Injectable } from '@angular/core';
import { PreferencesVO } from './shared/models/preferencesVO.model';

@Injectable()
export class Constants {
  role: string = 'test';
}

export const GOOGLE_API = environment.googleAPI;

// static lookup data
export const SITES = [];
export const CATEGORIES = [];
export const RGRESOURCES = [];
export const RESOURCES = [];
export const CUSTOMFIELDS = [];
export const IMPACTS = [];
export const ROOTCAUSES = [];
export const REPORTS = [];
export const PREFERENCES = new PreferencesVO();
export const USERTYPES = [];
export const FIELDNAMES = [];
export const STATUSES = [];

export const SITE_URL = '/documents/';
export const FACIAL_IMAGES_URL = '/documents/facialImages/';

// incidents
export const INCIDENTS = [];
export const INCIDENTS_TODAY = [];
export const INCIDENTS_OPEN = [];
export const INCIDENTS_HOLD = [];
export const INCIDENTS_LENGTH = [];

// incident stats
export const INCIDENT_STATS = [];
export const INCIDENT_STATS_LENGTH = [];

// incident types
export const INCIDENT_TYPES = [];
export const INCIDENT_TYPES_FILE = 'incidentTypes.json';

// assets
export const ASSETS_FIXED = [];
export const ASSETS_PEOPLE = [];
export const ASSETS_FIXED_LENGTH = [];
export const ASSETS_PEOPLE_LENGTH = [];

// service providers
export const SERVICE_PROVIDERS = [];
export const SERVICE_PROVIDERS_LENGTH = [];

// URLs
//export const SITE_URL = 'https://dev.myincidentdesk.com';
//export const DOCUMENTS_FOLDER_URL = '/documents/';
//export const FACIAL_IMAGES_URL = '/documents/facialImages/';
