import { PipeTransform, Pipe } from '@angular/core';

@Pipe( {
    name: 'incidentDuration'
})
/**
 * Pipe to determine duration since incident has been created
 */
export class IncidentDurationPipe implements PipeTransform {
    transform(value: any, statusId: number, modifiedDateTimeStamp: string, dateTimeStamp: string) {

        let ret = '';
        let diffSeconds = 0;

        switch (statusId) {
        case 2: // Cancelled
        case 4: // Closed
            if (modifiedDateTimeStamp !== '0000-00-00 00:00:00') {
                // get duration from when incident was created to last time it was edited(closed/cancelled)
                diffSeconds = (new Date(modifiedDateTimeStamp).getTime() - new Date(dateTimeStamp).getTime()) / 1000;
            }
            break;

        default: // all other statusses
            // get duration from when incident was created to now (getTime gets miliseconds)
            diffSeconds = (new Date().getTime() - new Date(dateTimeStamp).getTime()) / 1000;
            break;
        }

        if (diffSeconds > 0) {
            // convert seconds to format 'days hours:minutes' eg. 10d 5:23
            const days = Math.floor(diffSeconds / (3600 * 24));
            diffSeconds  -= days * 3600 * 24;
            const hrs = Math.floor(diffSeconds / 3600);
            diffSeconds  -= hrs * 3600;
            const mnts = Math.floor(diffSeconds / 60);

            ret = days.toString() + 'd ' + hrs.toString() + ':' + mnts.toString();
        }

        return ret;
    }
}
