import { PipeTransform, Pipe } from '@angular/core';

@Pipe( {
    name: 'resourceName'
})
/**
 * Pipe to return a resource name for the supplied resource ID
 */
export class ResourceNamePipe implements PipeTransform {
    transform(value: any) {

        const ret = 'Other';
        const data = JSON.parse(sessionStorage.getItem('resources'));

        for (let i = 0; i < data.length; i++) {
            if (Number(data[i].id) === Number(value)) {
                return data[i].firstName + ' ' + data[i].lastName;
            }
        }

        return ret;
    }
}
