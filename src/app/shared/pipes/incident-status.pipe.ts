import { PipeTransform, Pipe } from '@angular/core';

@Pipe({
    name: 'incidentStatus'
})

  /**
   * Pipe to get the status info from sessionStorage.
   * @param statusId Status ID
   * @param option defaults to description, which will return status desc, otherwise the complete status obj
   * @return status description OR status obj
   */
export class IncidentStatusPipe implements PipeTransform {
    transform(statusId: number, option: string = 'description') {
        const data = JSON.parse(sessionStorage.getItem('statuses'));

        for (let i = 0; i < data.length; i++) {
            if (Number(data[i].id) === Number(statusId)) {
                if (option === 'description') {
                    return data[i].description;
                } else {
                    return data[i];
                }
            }
        }
    }
}
