import { PipeTransform, Pipe } from '@angular/core';

@Pipe( {
    name: 'incidentReportedAssigned'
})
/**
 * Pipe to return the action's ASSIGNED TO, or incident's REPORTED BY resource name for the supplied resource ID
 */
export class IncidentReportedAssignedPipe implements PipeTransform {
    transform(latestActionResourceId: number, reportedByResourceId: number, reportedOther: string) {

        // should we fetch it here, or fetch it once on the parent & pass it into the function?
        const data = JSON.parse(sessionStorage.getItem('resources'));

        // if incident has actions, returned Assigned To of latest action
        // add + in front of latestActionResourceId to ensure nmeric === check does not fail
        if (latestActionResourceId != null && +latestActionResourceId !== 0) {
            for (let i = 0; i < data.length; i++) {
                if (Number(data[i].id) === Number(latestActionResourceId)) {
                    return data[i].firstName + ' ' + data[i].lastName;
                }
            }

        // if not actions, get resource that incident is assigned to
        } else {
            if (reportedByResourceId !== -1) {
                for (let i = 0; i < data.length; i++) {
                    if (Number(data[i].id) === Number(reportedByResourceId)) {
                        return data[i].firstName + ' ' + data[i].lastName;
                    }
                }
            } else {
                // return reportedOther;
                return reportedOther;
            }
        }
    }
}
