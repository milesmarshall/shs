import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { PerfectScrollbarModule } from "ngx-perfect-scrollbar";

// COMPONENTS
import { FooterComponent } from "./footer/footer.component";
import { NavbarComponent } from "./navbar/navbar.component";
import { SidebarComponent } from "./sidebar/sidebar.component";
import { CustomizerComponent } from './customizer/customizer.component';
import { NotificationSidebarComponent } from './notification-sidebar/notification-sidebar.component';

import { HeroesComponent } from '../heroes/heroes.component';
import { SelectSiteComponent } from 'app/incident/add/select-site/select-site.component'
import { SelectCategoryComponent } from 'app/incident/add/select-category/select-category.component'
import { EnterNotificationsComponent } from 'app/incident/add/enter-notifications/enter-notifications.component';
import { PeopleDocumentsComponent } from 'app/incident/add/people-documents/people-documents.component';
import { AdditionalInfoComponent } from 'app/incident/add/additional-info/additional-info.component';
import { EnterActionsComponent } from 'app/incident/add/enter-actions/enter-actions.component';
import { EnterIncidentComponent } from 'app/incident/add/enter-incident/enter-incident.component';
import { SelectAssetsComponent } from 'app/incident/add/select-assets/select-assets.component';
import { NotificationsComponent } from 'app/incident/add/notifications/notifications.component';
//import { ListIncidentStatsComponent } from 'app/incident/list/list-incident-stats/incident-stats.component';
//import { IncidentFilterComponent } from 'app/incident/list/incident-filter/incident-filter.component';
//import { IncidentFilterResultsComponent } from 'app/incident/list/incident-filter-results/incident-filter-results.component';
//import { IncidentDetailsComponent } from 'app/incident/list/incident-details/incident-details.component';

import { StatusesComponent } from "app/shared/dropdowns/statuses/statuses.component";

//DIRECTIVES
import { ToggleFullscreenDirective } from "./directives/toggle-fullscreen.directive";
import { SidebarDirective } from './directives/sidebar.directive';
import { SidebarLinkDirective } from './directives/sidebarlink.directive';
import { SidebarListDirective } from './directives/sidebarlist.directive';
import { SidebarAnchorToggleDirective } from './directives/sidebaranchortoggle.directive';
import { SidebarToggleDirective } from './directives/sidebartoggle.directive';

// COMPONENTS
import { AddEmailModalComponent } from './modals/add-email-modal/add-email-modal.component';
import { AssetsComponent } from './assets/assets.component';
import { LoadingSpinnerComponent } from './loading-spinner/loading-spinner.component';

// PIPES
import { ResourceNamePipe } from './pipes/resource-name.pipe';

@NgModule({
    exports: [
        CommonModule,
        FooterComponent,
        NavbarComponent,
        SidebarComponent,
        CustomizerComponent,
        NotificationSidebarComponent,
        ToggleFullscreenDirective,
        SidebarDirective,
        NgbModule,
        TranslateModule,
        AddEmailModalComponent,
        HeroesComponent,
        SelectSiteComponent,
        SelectCategoryComponent,
        EnterNotificationsComponent,
        PeopleDocumentsComponent,
        AdditionalInfoComponent,
        EnterActionsComponent,
        EnterIncidentComponent,
        SelectAssetsComponent,
        NotificationsComponent,
        StatusesComponent,
        LoadingSpinnerComponent,
        ResourceNamePipe
    ],
    imports: [
        RouterModule,
        CommonModule,
        NgbModule,
        TranslateModule,
        PerfectScrollbarModule
    ],
    declarations: [
        FooterComponent,
        NavbarComponent,
        SidebarComponent,
        CustomizerComponent,
        NotificationSidebarComponent,
        ToggleFullscreenDirective,
        SidebarDirective,
        SidebarLinkDirective,
        SidebarListDirective,
        SidebarAnchorToggleDirective,
        SidebarToggleDirective,
        AddEmailModalComponent,
        AssetsComponent,
        HeroesComponent,
        SelectSiteComponent,
        SelectCategoryComponent,
        EnterNotificationsComponent,
        PeopleDocumentsComponent,
        AdditionalInfoComponent,
        EnterActionsComponent,
        EnterIncidentComponent,
        SelectAssetsComponent,
        NotificationsComponent,
        StatusesComponent,
        LoadingSpinnerComponent,
        ResourceNamePipe
    ]
})
export class SharedModule { }
