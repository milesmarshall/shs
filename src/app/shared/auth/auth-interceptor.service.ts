import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpParams, HttpHeaders } from '@angular/common/http';
import { AuthService } from './auth.service';
import { take, exhaustMap } from 'rxjs/operators';

@Injectable()
export class AuthInterceptorService implements HttpInterceptor {

    // inject authService to get access to sessionToken in user data
    constructor(private authService: AuthService) {}

    intercept(req: HttpRequest<any>, next: HttpHandler) {

        // subscribe to one instance of user observable, auto unsubscribe(via pipe/take), wait for first observable(via exhaustMap) and then subscribe to http observable
        return this.authService.loggedInUser.pipe(
            take(1),
            exhaustMap( userData => {

                // if userData does not exist, return request without trying to append sessionToken
                if (!userData) {
                    return next.handle(req);
                }

                // get sessionToken & securityGroupId to add to http request - use headers for post request (only sessionToken for post)
                const modifiedReq = req.clone({
                    setParams: {
                        sessionToken: userData.sessionToken,
                        securityGroupId: userData.securitygroupId.toString()
                    },
                    headers: req.headers.set('Authorization', userData.sessionToken)
                });

                // Pass the cloned request instead of the original request to the next handle
                return next.handle(modifiedReq);
            })
        );
    }
}
