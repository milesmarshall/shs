import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, UrlTree } from '@angular/router';
import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private authService: AuthService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Promise<boolean | UrlTree> | Observable<boolean | UrlTree> {
    // return this.authService.isAuthenticated();
    return this.authService.loggedInUser.pipe(map( userData => {
      const isAuth = !!userData; // shortcut for isAuth = !user ? false : true

      // if user is valid, return true
      if (isAuth) {
        return true;
      }

      // if user NOT valid, relocate to login screen
      return this.router.createUrlTree(['pages/login']);
    }))
  }
}
