import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { environment } from 'environments/environment';
import { User } from '../models/user.model';

@Injectable()
export class AuthService {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;
  tokenExpirationTimer: any;

  loggedInUser = new BehaviorSubject<User>(null);

  // sessionToken: string = null; //setting the token to null, causes a white page to load
  sessionToken: string;

  constructor(private http: HttpClient, private router: Router) {

      // MJA TO DO. Check is this is used.
      this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
      this.currentUser = this.currentUserSubject.asObservable();
  }

  getLoggedInUser(): User {
    return this.loggedInUser.value;
  }

  login(username: string, password: string) {
    return this.http.post<any>(`${environment.apiUrl}/login`, { username, password, portal: 'idmain' })
      .pipe(tap(responseData => {

        // const tokenExpirationDate: Date = new Date(new Date().getTime() +  +responseData.expiresIn * 1000);
        const tokenExpirationDate: Date = new Date(+responseData.expiryDate * 1000); // x 1000 to get miliseconds
        const user = this.newUserVO(responseData.data, responseData.result, tokenExpirationDate);

        // MJA TO DO. Check is this is used.
        this.currentUserSubject.next(user);

        // emit as currently logged in user
        this.loggedInUser.next(user);

        // start timer to measure sessionToken duration - currently 24h - this will change to check idle time
        const duration = new Date(tokenExpirationDate).getTime() - new Date().getTime();
        this.autoLogout(duration);

         // MJA TO DO - need to get expiresIn as seconds from API - rather than date time stamp........

        // MJ this is wrong, you nned to store userData in sessionStorage, not localStorage as localStorage is more persistent
        // MJA to change when we go live. Useful now during dev.

        // store user details and basic auth credentials in local storage to keep user logged in between page refreshes
        localStorage.setItem('userData', JSON.stringify(user));

        console.log('[inside auth.service - login] user: ');
        console.log(user);

        // MJA: original code - still required?
        responseData.authdata = window.btoa(username + ':' + password); // base-64 encode string
        this.sessionToken = responseData.result;

        return responseData;
      }));
  }

  // Create new userVO obj with data supplied
  newUserVO( userData, sessionToken: string, tokenExpirationDate: Date ) {

    const user = new User(
      userData.id,
      userData.username,
      userData.firstName,
      userData.lastName,
      userData.securitygroupId,
      userData.resourcegroupId,
      userData.typeId,
      userData.isActive,
      userData.allowCRM,
      userData.allowAdd,
      userData.allowAssets,
      userData.allowPredictiveAnalysis,
      userData.allowEasyRoster,
      userData.allowPublicCommunication,
      userData.allowFaceScan,
      userData.allowResourceMap,
      userData.allowRegisterHumanAsset,
      userData.limitStatusChange,
      userData.updatePassword,
      userData.allowDepartmentReports,
      userData.systemResource,
      userData.acceptTermsDate,
      userData.securityGroup,
      userData.locationDelta,
      userData.locationActive,
      userData.lastKnownLatitude,
      userData.lastKnownLongitude,
      userData.locationTimestamp,
      userData.idNumber,
      userData.gender,
      userData.staffnumber,
      userData.email,
      userData.contactNumberMain,
      userData.contactNumberAlt,
      userData.cellNumber,
      userData.employmentType,
      userData.employmentStatus,
      userData.jobTitle,
      userData.resourceGroupName,
      userData.managerAccess,
      userData.blacklisted,
      userData.userEnabled,
      userData.pushNotificationId,
      userData.activationcode,
      userData.activationcodeEmail,
      userData.cell911One,
      userData.cell911Two,
      userData.cell911Three,
      userData.email911One,
      userData.email911Two,
      userData.email911Three,
      userData.popiAcceptanceTimestamp,
      sessionToken,
      tokenExpirationDate
    );

    return user;

  }

  logout() {

    // remove userdata from local storage
    localStorage.removeItem('userData');
    localStorage.removeItem('currentUser');

    // localStorage.clear(); // remove everything from localStorage - perhaps this is better for multiple/different user logins on the same machine??

    // clear user subject
    this.currentUserSubject.next(null);
    this.loggedInUser.next(null);

    // clear tokenExpiration timer if running
    if (this.tokenExpirationTimer) {
      clearTimeout(this.tokenExpirationTimer);
    }
    this.tokenExpirationTimer = null;

    this.sessionToken = null;

    console.log('you have been logged out');
    this.router.navigate(['/pages/login']);
  }

  // Auto login user if localStorage data for user exists. This ensures session is not lost if user refreshes browser.
  autoLogin() {
    // get data from local storage and convert back to JS obj
    const storedUserData = JSON.parse(localStorage.getItem('userData'));

    // if data does not exist, abort
    if (!storedUserData) {
      console.log('[INSIDE AUTOLOGIN] - no stored data' );
      return;
    }

    const loadedUser = this.newUserVO(storedUserData, storedUserData._sessionToken, new Date(storedUserData._tokenExpirationDate));

    // user getting function in model, which checks validity of token before passing it along
    if (loadedUser.sessionToken) {
      // if token is valid, call user subject and emit storedUser as currently logged in user
      this.loggedInUser.next(loadedUser);

      // start timer to measure sessionToken duration
      const duration = new Date(storedUserData._tokenExpirationDate).getTime() - new Date().getTime();
      this.autoLogout(duration);
    }
  }

  // Auto logout user once token has expired - requires token duration in miliseconds
  autoLogout(expirationDuration: number) {
    this.tokenExpirationTimer = setTimeout(() => {
        this.logout();
      }, expirationDuration)
  }

  getToken() {
    return this.sessionToken;
  }

  isAuthenticated() {
    return this.sessionToken === null ? false : true;
  }
}
