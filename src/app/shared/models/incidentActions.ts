export class IncidentActions {
    actionType: number = 0;
    actualDuration: number = 0;
    assignedResources: null
    dateTimeStamp: string;
    description: string;
    estimatedDuration: number = 0;
    geofencedSiteId: number = 0;
    id: number = 0;
    incidentId: number = 0;
    isActive: number = 0;
    isInternal: number = 0;
    loggedUserId: number = 0;
    notes: string;
    notifyPublic: string;
    publcCommReportedByResourceId: string;
    publicSmsEmail: string;
    requiredTimestamp: number = 0;
    resourceId: number = 0;
    resourceOther: string;
    scheduleInstanceId: number = 0;
    scheduledUUID: number = 0;
    siteName: string;
    ssrResourceId: string;
    ssrStatus: string;
    status: number = 0;
}