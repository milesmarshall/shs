import { Deserializable } from './deserializable.model';

export class CategoryVO implements Deserializable {
    id: number = 0;
    name: string;
    parentId: number;
    idKey: string;
    description: string;
    isActive: number;
    referenceColor: string;
    icon: string;
    isEmergency: number;
    costCode: string;
    sites = [];
    treeStatus: string = 'collapsed'; // used for tree display
    childrenPopulated: boolean = false; // used for tree display

    deserialize(input: any) {
        Object.assign(this, input);
        return this;
    }
}
