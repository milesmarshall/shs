import { Deserializable } from './deserializable.model';

export class StatusVO implements Deserializable {
    id: number = 0;
    description: string;
    indicatorColor: string;

    deserialize(input: any) {
        Object.assign(this, input);
        return this;
    }
}
