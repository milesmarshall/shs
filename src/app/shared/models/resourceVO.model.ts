import { Deserializable } from './deserializable.model'

export class ResourceVO implements Deserializable {
    id: number = 0;
    username: string;
    firstName: string = '';
    lastName: string = '';
    securitygroupId: number;
    resourcegroupId: number;
    typeId: number;
    isActive: number;
    allowCRM: number = 0;
    allowAdd: number = 0;
    allowAssets: number = 0;
    allowPredictiveAnalysis: number = 0;
    allowEasyRoster: number = 0;
    allowPublicCommunication: number = 0;
    allowFaceScan: number = 0; // Enable face scanner tab
    allowResourceMap: number = 0; // Enable Resource map
    allowRegisterHumanAsset: number = 0; // Enable user to register a human asset based on returned face data
    limitStatusChange: number = 0;
    updatePassword: number;
    allowDepartmentReports: number = 0;
    systemResource: number = 0;
    acceptTermsDate: object;
    securityGroup: object; //  (TO DO - update type to VO) SecurityGroupVO
    locationDelta: number = 0;
    locationActive: number = 0;
    lastKnownLatitude: number = 0;
    lastKnownLongitude: number = 0;
    locationTimestamp: number = 0;

    idNumber?: string;
    gender?: object;
    staffnumber?: string;
    email?: string;
    contactNumberMain?: string;
    contactNumberAlt?: string;
    cellNumber?: string;
    employmentType?: string;
    employmentStatus?: string;
    jobTitle?: string;
    resourceGroupName?: string;
    managerAccess?: number;
    blacklisted?: number;
    userEnabled?: number;
    pushNotificationId?: string;
    activationcode?: string;
    activationcodeEmail?: string;
    cell911One?: string;
    cell911Two?: string;
    cell911Three?: string;
    email911One?: string;
    email911Two?: string;
    email911Three?: string;
    popiAcceptanceTimestamp?: number;
    // authdata?: string;

    private _sessionToken?: string;
    private _tokenExpirationDate?: Date;

    deserialize(input: any) {
        Object.assign(this, input);
        return this;
    }

    // user getter to prevent user from overwriting value & also enable validation at time of retrieval
    get sessionToken() {

        // if tokenDate does not exist, or date is AFTER now, return null for token
        if (!this._tokenExpirationDate || new Date() > this._tokenExpirationDate ) {
            return null
        }
        return this._sessionToken
    }

    get tokenExpirationDate() {
        return this._tokenExpirationDate
    }

}
