import { Deserializable } from './deserializable.model';

export class ActionVO implements Deserializable {
    id: number = 0;
    incidentId: number = 0;
    scheduleInstanceId: number = 0;
    description: string = '';
    resourceId: number = 0;
    loggedUserId: number = 0;
    resourceOther: string;
    dateTimeStamp: string = '';
    isInternal: number = 0;
    isActive: number = 1;
    actionType: number = 0;     // 0: Action, 1: Service Request
    requiredTimestamp: number = 0;
    estimatedDuration: number = 0;
    actualDuration: number = 0;
    status: number = 0;
    notes: string;
    notifyPublic: number = 0;
    publicSmsEmail: string;
    publcCommReportedByResourceId: number = 0;
    siteName: string = '';
    geofencedSiteId: number = 0;
    scheduledUUID: number = 0;
    assignedResources = [];
    ssrResourceId: number = 0;
    ssrStatus: number = 0;

    deserialize(input: any) {
        Object.assign(this, input);
        return this;
    }
}
