export class IncidentGPS {
    actionId: number = 0;
    assetId: number = 0;
    id: number = 0;
    incidentId: number = 0;
    isFloorplan: number = 0;
    latitude: number = 0;
    longitude: number = 0;
    notes: string;
    radius: number = 0;
    resourceId: number = 0;
    siteId: number = 0;
    timestamp: string;
}