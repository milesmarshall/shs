import { Deserializable } from './deserializable.model';

export class AssetFixedVO implements Deserializable {
    assetId: number = 0;
    assetTypeId: number = 0; // fixed
    name: string = '';
    resourceId: number = 0;
    description: string = '';
    serialNumber: string = '';
    assetNumber: string = '';
    make: string = '';
    assetModel: string = '';
    notes: string = '';
    locationThreshold: number = 0;
    customFieldValues = [];

    deserialize(input: any) {
        Object.assign(this, input);
        return this;
    }
}
