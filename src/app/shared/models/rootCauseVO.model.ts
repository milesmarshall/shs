import { Deserializable } from './deserializable.model';

export class RootCauseVO implements Deserializable {
    id: number = 0;
    description: string;
    categories: [];

    deserialize(input: any) {
        Object.assign(this, input);
        return this;
    }
}
