import { Deserializable } from './deserializable.model';

export class AssetPeopleVO implements Deserializable {
    assetId: number = 0;
    assetTypeId: number = 0; // fixed
    name: string = '';
    resourceId: number = 0;
    lastName: string = '';
    idNumber: string = '';
    dateOfBirth: string = '';
    gender: string = '';
    ethnicity: string = '';
    resAddress: string = '';
    resAddressCity: string = '';
    resAddressPostalCode: string = '';
    postAddressPoBox: string = '';
    postAddressCity: string = '';
    postAddressPostalCode: string = '';

    popiResourceId: string = '';
    popiAcceptanceTimestamp: string = '';

    // facial recognition data
    faceToken: string;
    image: string;
    rawData: string;
    // personData.imageURL = model.siteURL + "/documents/facialImages/" + model.luAssets[h].facialRecognitionData[i].image;

    customFieldValues = [];

    deserialize(input: any) {
        Object.assign(this, input);
        return this;
    }
}
