import { Deserializable } from './deserializable.model';

export class PreferencesVO implements Deserializable {
    id: number;
    restrictResourceIncidents: number;
    maxSites: number;
    maxCategories: number;
    maxResources: number;
    customerLogo: string;
    smtpUsername: string;
    smtpPassword: string;
    smtpHost: string;
    smtpPort: string;
    emailFrom: string;
    profileName: string;
    maxAssets: number;
    pollInterval: number;
    forceFullLoad: number;
    useActiveDirectoryAuthentication: number;
    activateEasyRoster: number;
    activateSefeko: number;
    sefekoDefaultImpactId: number;
    siteAdminResourceId: number;
    activatePublicCommunication: number;
    publcCommReportedByResourceId: number;
    publcCommDefaultImpactId: number;
    publcCommContactCustomFieldId: number;
    maxReportIncidents: number;
    mapCountry: string;
    defaultLatitude: number;
    defaultLongitude: number;
    catchAllSiteId: number;
    publicSecurityGroupId: number;
    publicResourceGroupId: number;
    emergencyPollCategories: number;
    linkedIncidentCustomFieldId: number;
    notificationEmailSubject: string;
    maxCustomers: number;
    predictiveAnalysisCrimeCategoryId: number;
    predictiveAnalysisLowProbabilityRangeEnd: number;
    predictiveAnalysisMediumProbabilityRangeEnd: number;
    predictiveAnalysisMaximumItemValue: number;
    maxFaceScanUsers: number;
    maxHumanAssetRegistrationUsers: number;
    maxResourceTrackingUsers: number;
    customFieldStatusId: number;
    customFieldImpactId: number;
    enableResourceTracking: number;

    deserialize(input: any) {
        Object.assign(this, input);
        return this;
    }
}
