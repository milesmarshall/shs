import { Deserializable } from './deserializable.model';

export class UserVO implements Deserializable {
    id: number = 0;
    firstName: string;
    lastName: string;
    email: string;
    cellNumber: Number = 0;
    contactNumberMain: Number = 0;
    contactNumberAlt: Number = 0;
    managerAccess: Number = 0;
    gender: string;

    deserialize(input: any) {
        Object.assign(this, input);
        return this;
    }
}
