import { Deserializable } from './deserializable.model';

export class ImpactVO implements Deserializable {
    id: number = 0;
    description: string;

    // [ArrayElementType("com.id.vo.ImpactAutoEscalationVO")]
    autoEscalations: [];
    // [ArrayElementType("com.id.vo.ImpactStatusEscalationVO")]
    statusEscalations: [];

    deserialize(input: any) {
        Object.assign(this, input);
        return this;
    }
}
