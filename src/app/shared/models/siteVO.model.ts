import { Deserializable } from './deserializable.model';

export class SiteVO implements Deserializable {
    id: number = 0;
    name: string;
    description: string;
    clientId: number;
    regionId: number;
    parentId: number;
    isActive: number;
    easyRosterSiteId: number;
    siteGPS = [];
    cellphoneShortCode: number;
    idKey: string;
    pcEmail: string;
    isGeoLocked: number;
    useParentGeoFence: number;
    useFloorPlan: number;
    address: string;
    googleMapsURL: string;
    securityGroupId: number;
    treeStatus: string = 'collapsed'; // used for tree display
    childrenPopulated: boolean = false; // used for tree display

    deserialize(input: any) {
        Object.assign(this, input);
        return this;
    }
}
