export class IncidentHistory {
    auditEventId: number = 0;
    auditId: number = 0;
    dateTimeStamp: string;
    item: string;
    loggedResourceId: number = 0;
    notes: string;
    statusId: number = 0;
    uniqueTableId: number = 0;
}