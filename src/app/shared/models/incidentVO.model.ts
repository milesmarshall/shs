import { Deserializable } from './deserializable.model';

export class IncidentVO implements Deserializable {

    incidentId: number = 0;
    loggedResourceId: Number = 0;
    siteId: number = 0;
    siteName: string;
    locationId: number = 0;
    categoryId: number = 0;
    categoryName: string;
    thirdPartyClientId: number = 0; // ID of Third Party client that incident was logged for (usually logged via REST API)
    reportedResourceId: number = 0;
    reportedOther: string;
    description: string = '';
    summary: string = '';
    actionSummary: string = '';
    dateTimeStamp: string;  // date that incident was logged
    incidentDateTime: string = ''; // date that incident occurred
    impactId: number;
    impact: string;
    rootCauseId: number = 0;
    subCategoryId: number = 0;
    statusId: number = 1; // efault to open incident
    modifiedResourceId: number = 0;
    modifiedDateTimeStamp: string = ''; // date that incident was modified

    incidentType: number = 1; // There are two types of incidents: 1=incident(default), 2=scheduled task
    incidentName: string = '';
    scheduleFrequency: string = '';
    scheduleStartDateTime: string = '';
    scheduleEndDateTime: string = '';
    scheduleLeadWindow: number = 0;
    partnerId: number = 0; // Id of Partner e.g. COCT

    // The instanceID included here as all scheduled instances are selected as an individual incident to show on the incidents list.
    // As instances share a incidentID, we need a unique identifier if only once instance needs to be updated.
    scheduleInstanceId: number = 0;

    /*The actual instances are only populated when an incident is created or the ENTIRE schedule updated*/
    scheduleInstances = [];

    // MJA TO DO - alter query to fetch separate data sets for fixed and people assets
    assets = [];    // merged array of fixed & people assets (required by backend)
    assetsFixed = [];
    assetsPeople = [];

    actionsTaken = [];
    escalation = [];
    documents = [];
    statusHistory = [];
    resources = [];
    statuses = [];
    customFieldValues = [];
    loggedWork = [];
    incidentGPS = [];

    // Third Party Push Notifications - populated on front end when incident is saved
    thirdPartyPushNotifications = [];

    communicationId: number = 0; // only used when incident is saved from PC notification to pass along communicationID to php service
    linkedIncidentDesc: string; // Description of incident that is linked to this one via custom field 'Linked Incident ID'.
    statusModified: number = 0; // time that current status was activated
    impactStatusEscalationProcessed: number; // ID of last impact status escalation that was processed
    facialRecognitionData = [];
    latestActionResourceId?: number = 0; // resource that newest action is assigned to - used in incident list

    // assignedResources = [];
    // incidentIdHold: number = 0; // temp holder for a new incident temp ID - used for uploding the dcumnets prior to saving the incident

    incidentActionsCount: number = 0;
    incidentNotifyCount: number = 0;
    incidentAssetsCount: number = 0;
    incidentDocumentsCount: number = 0;
    incidentWorkCount: number = 0;
    incidentPeopleCount: number = 0;

    selected = [];

    deserialize(input: any) {
        Object.assign(this, input);
        return this;
    }
}
