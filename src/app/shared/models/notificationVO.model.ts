import { Deserializable } from './deserializable.model';

export class NotificationVO implements Deserializable {
    id: number = 0;
    incidentId: number = 0;
    actionId: number = 0;
    description: string = ''; // Incident or action to notify of. When incident is created, actions don't have id's yet, so action description is saved to identify specific action to notify of until it has an id.
    resourceId: number = 0;
    loggedUserId: number = 0; // Used in PHP. Populated inside incident's saveData()
    email: number = 0;
    sms: number = 0;
    timeStamp: string = '';
    escalationType: number = 1; // 1=manual, 2=impact auto notification, 3=impact status escalation, 4=PC notification
    sendForScheduleInstance: number = 0; // only send once sched task instance is activated

    deserialize(input: any) {
        Object.assign(this, input);
        return this;
    }
}
