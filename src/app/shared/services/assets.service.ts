import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'environments/environment';
import { AuthService } from '../auth/auth.service';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class AssetsService {
  detailsTabData: any;

  constructor(
    private httpClient: HttpClient,
    private authService: AuthService
    ) { }

  public getAssetsFixed(sessionToken, securityGroupId, resourceGroupId, assetTypeId, action) {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');

    const params = new HttpParams()
      .set('sessionToken', sessionToken)
      .set('securityGroupId', securityGroupId)
      .set('resourceGroupId', resourceGroupId)
      .set('assetTypeId', assetTypeId)
      .set('action', action);

    return this.httpClient.get(`${environment.apiUrl}/lookups`, { headers: headers, params: params });
  }

  public getAssetsPeople(sessionToken, securityGroupId, resourceGroupId, assetTypeId, action) {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');

    const params = new HttpParams()
      .set('sessionToken', sessionToken)
      .set('securityGroupId', securityGroupId)
      .set('resourceGroupId', resourceGroupId)
      .set('assetTypeId', assetTypeId)
      .set('action', action);

    return this.httpClient.get(`${environment.apiUrl}/lookups`, { headers: headers, params: params });
  }

  // Get all FIXED assets for selected site & cat and asset type
  getFixedAssetsForSiteCat(assetTypeId: number, siteId: number, catId: number) {
    const params = new HttpParams()
            .set('action', 'getFixedAssetsForSiteCat')
            .set('siteId', siteId.toString())
            .set('categoryId', catId.toString());

            return this.httpClient.get(`${environment.apiUrl}/assets`, { params: params });
  }

  // Get all PEOPLE assets for selected cat that belong to same resource group as logged in user
  getPeopleForRGCat(catId: number) {

    const loggedInUser: User = this.authService.getLoggedInUser();

    const params = new HttpParams()
            .set('action', 'getPeopleForRGCat')
            .set('resourceGroupId', loggedInUser.resourcegroupId.toString())
            .set('categoryId', catId.toString());

            return this.httpClient.get(`${environment.apiUrl}/assets`, { params: params });
  }

}
