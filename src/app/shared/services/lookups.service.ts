import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'environments/environment';

import { INCIDENT_TYPES } from 'app/app.constants';
import { INCIDENT_TYPES_FILE } from 'app/app.constants';

declare var require: any;

@Injectable({
  providedIn: 'root'
})
export class LookupsService {

  categories:any = [];
  statuses:any = [];
  resources:any = [];
  rootCauses:any = [];
  impacts:any = [];
  reports:any = [];
  incident_types = INCIDENT_TYPES;
  incident_types_file = INCIDENT_TYPES_FILE;

  constructor(private httpClient: HttpClient) { }

    // Query database to get lookup data set specified via action
    public getLookups( action:any=null, id:any=null, siteId:any=null) {
      const headers = new HttpHeaders();
      headers.append('Content-Type', 'application/json');

      let params;

      if (action === 'sites') {
        params = new HttpParams()
          .set('action', action)
          .set('siteId', id);
      } else if (action === 'categories') {
        params = new HttpParams()
          .set('action', action)
          .set('categoryId', id)
          .set('siteId', siteId);
      } else {
        params = new HttpParams()
          .set('action', action)
      }

      return this.httpClient.get(`${environment.apiUrl}/lookups`, { headers: headers, params: params });
    }

    public getCategories() {
      let temp = JSON.parse(sessionStorage.getItem('categories') || '[]');
          temp = temp[0]['result'];

      for (let i = 0; i < temp.length; i++) {
        this.categories.push(temp[i]['description']);
      }

      return this.categories;
    }

    public getCategoryFromId(categoryId:any = null, value:any = null) {
      // return sessionStorage.getItem("categories").findIndex(categoryId);
      // this.temp_data[0]['result'][i].statusId

      let data = JSON.parse(sessionStorage.getItem('categories') || '[]');

      // console.log(data);

      for (let i = 0; i < data.length; i++) {
          if (data[i].categoryId === categoryId) {
            if (value === 'description')
              return data[i].description;
            else
              return data[i];
          }
      }

      return '???';
    }

    // Get root causes from session storage
    getRootCauses() {
      const sessionRootCauses = JSON.parse(sessionStorage.getItem('rootCauses') || '[]');
      this.rootCauses = sessionRootCauses;
      return this.rootCauses;
    }

    // Get statuses from session storage
    getStatuses() {
      const sessionStatuses = JSON.parse(sessionStorage.getItem('statuses') || '[]');
      this.statuses = sessionStatuses;
      return this.statuses;
      /*this.lookupsService.getLookups('000','0','status').subscribe((data)=>{
              this.statuses.push(data);

              sessionStorage.setItem('statuses', JSON.stringify(this.statuses));
          });*/
    }

    // Get resources from session storage
    getResources() {
      const sessionResources = JSON.parse(sessionStorage.getItem('resources') || '[]');
      this.resources = sessionResources;
      return this.resources;
    }

    getImpacts() {
      let temp = JSON.parse(sessionStorage.getItem('impacts') || '[]');
      temp = temp[0]['result'];

      for (let i = 0; i < temp.length; i++) {
        this.impacts.push(temp[i]['description']);
      }

      return this.impacts;
    }

    getReports() {
      let temp = JSON.parse(sessionStorage.getItem('reports') || '[]');
      temp = temp[0]['result'];

      for (let i = 0; i < temp.length; i++) {
        this.reports.push(temp[i]['description']);
      }

      return this.reports;
    }

    public getCustomFieldFromId(fieldId:any = null) {
      let data = JSON.parse(sessionStorage.getItem('customFields') || '[]');
          data = data[0]['result'];

      for (let i = 0; i < data.length; i++) {
          if (Number(data[i].id) === Number(fieldId))
            return data[i];
      }
    }

    public getIncidentTypes() {
      let data : any = require('../../shared/data/' + this.incident_types_file);

      return data;
    }

    public getIncidentTypeFromId(id:any = null, value:any = null) {
      if (this.incident_types) {
        let data : any = require('../../shared/data/' + this.incident_types_file);

        for (let i = 0; i < data.length; i++) {
          if (Number(data[i].id) === Number(id)) {
            if (value === 'description')
              return data[i].description;
            else
              return data[i];
          }
        }
      }
    }

    public getStatusFromId(id:any = null, value:any = null) {
      const data = JSON.parse(sessionStorage.getItem('statuses') || '[]');

      for (let i = 0; i < data.length; i++) {
        if (Number(data[i].id) === Number(id)) {
          if (value === 'description')
            return data[i].description;
          else
            return data[i];
        }
      }
    }

    public getResourceFromId(id:any = null, value:any = null) {
      const data = JSON.parse(sessionStorage.getItem('rgResources') || '[]');

      for (let i = 0; i < data.length; i++) {
        if (Number(data[i].id) === Number(id)) {
          if (value === 'name')
            return data[i].firstName + ' ' + data[i].lastName;
          else
            return data[i];
        }
      }
    }

    public getAssetFromId(id:any = null, value:any = null) {
      let data = JSON.parse(sessionStorage.getItem('assets_people') || '[]');
          data = data[0]['result'];

      for (let i = 0; i < data.length; i++) {
        console.log(data[i].assetId +','+ Number(id));
        if (Number(data[i].assetId) === Number(id))
            return data[i];
      }
    }

}
