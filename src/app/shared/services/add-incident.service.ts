import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'environments/environment';
import { WorkflowService } from 'app/incident/add-incident/workflow/workflow.service';
import { IncidentVO } from '../models/incidentVO.model';
import { STEPS } from 'app/incident/add-incident/workflow/workflow.model';
import { IncidentsService } from './incidents.service';

@Injectable({
    providedIn: 'root'
  })
  export class AddIncidentService {
    private isSiteTabFormValid: boolean = false;
    private isCategoryTabFormValid: boolean = false;
    private isLocationTabFormValid: boolean = false;
    private isDetailsTabFormValid: boolean = false;
    private isPeopleTabFormValid: boolean = false;
    private isActionsTabFormValid: boolean = false;
    private isNotificationsTabFormValid: boolean = false;
    private isAssetsTabFormValid: boolean = false;
    private isAdditionalInfoTabFormValid: boolean = false;

    formData = {
            incident: new IncidentVO(),
            siteTreeRows: [],
            selectedSite: [],
            categoryTreeRows: [],
            selectedCategory: [],
            fileToUpload: null
        }
    // private formData: FormData = new FormData();

    constructor(private workflowService: WorkflowService, private incidentsService: IncidentsService) {}

    saveIncident() {
        this.incidentsService.saveIncident(this.formData.incident, this.formData.fileToUpload).subscribe( responseData => {

            console.log('[saveIncident]');
            console.log(responseData);

            // this.isFetching = false;
            // this.incidents = results;
          });
    }

    // Get data for SITES tab
    getSiteTabData() {

        let date = '';
        let time = '';

        // get date from saved form data
        if (this.formData.incident.incidentDateTime !== '') {
            const incidentDateTime = this.formData.incident.incidentDateTime.split(' ', 2);
            date = incidentDateTime[0];
            time = incidentDateTime[1];
        // if date is not set, default to today
        } else {
            date = new Date().toISOString().substring(0, 10);
        }

        const siteTabData = {
            incidentDate: date,
            incidentTime: time,
            siteId: this.formData.incident.siteId,
            siteTreeRows: this.formData.siteTreeRows,
            selectedSite: this.formData.selectedSite
        };

        return siteTabData;
    }

    // Store SITE tab form data
    setSiteTabData(data: any) {

        // Update the form data only once it has been validated
        this.isSiteTabFormValid = true;

        this.formData.incident.incidentDateTime = data.incidentDate + ' ' + data.incidentTime;
        this.formData.incident.siteId = data.siteId;
        this.formData.siteTreeRows = data.siteTreeRows;
        this.formData.selectedSite = data.selectedSite;

        // Set this step as validated in Workflow service
        this.workflowService.validateStep(STEPS.sites);
    }

    // Get CATEGORY tab data
    getCategoryTabData() {

        const categoryTabData = {
            categoryId: this.formData.incident.categoryId,
            categoryTreeRows: this.formData.categoryTreeRows,
            selectedCategory: this.formData.selectedCategory
        };

        return categoryTabData;
        // return this.newIncident.categoryId;
    }

    // Store CATEGORY tab form data
    setCategoryTabData(data: any) {

        // Update the form data only once it has been validated
        this.isCategoryTabFormValid = true;
        this.formData.incident.categoryId = data.categoryId;

        console.log('cat data - catID: ' + this.formData.incident.categoryId);

        // Set this step as validated in Workflow service
        this.workflowService.validateStep(STEPS.categories);
    }

    // Get LOCATION/MAP tab form data
    getLocationTabData() {
        const locationTabData = {
            incidentGPS: this.formData.incident.incidentGPS
        };

        return locationTabData;
    }

    // Store LOCATION/MAP tab form data
    setLocationTabData(data: any) {

        // Update the form data only once it has been validated
        this.isLocationTabFormValid = true;
        this.formData.incident.incidentGPS = data.incidentGPS;

        // Set this step as validated in Workflow service
        this.workflowService.validateStep(STEPS.location);
    }

    // Get DETAILS tab form data
    getDetailsTabData() {
        const detailsTabData = {
            description: this.formData.incident.description,
            rootCauseId: this.formData.incident.rootCauseId,
            impactId: this.formData.incident.impactId,
            statusId: this.formData.incident.statusId,
            reportedResourceId: this.formData.incident.reportedResourceId,
            fileToUpload: this.formData.fileToUpload
        };

        return detailsTabData;
    }

    // Store DETAILS tab form data
    setDetailsTabData(data: any) {

        // Update the form data only once it has been validated
        this.isDetailsTabFormValid = true;

        // TO DO: update incident values...
        this.formData.incident.description = data.description;
        this.formData.incident.rootCauseId = data.rootCauseId;
        this.formData.incident.impactId = data.impactId;
        this.formData.incident.statusId = data.statusId;
        this.formData.incident.reportedResourceId = data.reportedResourceId;
        this.formData.fileToUpload = data.fileToUpload;


        // Set this step as validated in Workflow service
        this.workflowService.validateStep(STEPS.details);
    }

    // Get PEOPLE tab form data
    getPeopleTabData() {
        const peopleTabData = {
            assetsPeople: this.formData.incident.assetsPeople
        };

        return peopleTabData;
    }

    // Store PEOPLE tab form data
    setPeopleTabData(data: any) {

        // Update the form data only once it has been validated
        this.isPeopleTabFormValid = true;
        this.formData.incident.assetsPeople = data.assetsPeople;

        // Set this step as validated in Workflow service
        this.workflowService.validateStep(STEPS.people);
    }

    getAdditionalInfoTabData() {
        const additionalInfoTabData = {
            customFieldValues: this.formData.incident.customFieldValues
        };

        return additionalInfoTabData;
    }

    // Store ADDITIONAL INFO tab form data
    setAdditionalInfoTabData(data: any) {

        this.isAdditionalInfoTabFormValid = true;
        this.formData.incident.customFieldValues = data.customFieldValues;

        // Set this step as validated in Workflow service
        this.workflowService.validateStep(STEPS.additionalInfo);
    }

    // Get ACTIONS tab form data
    getActionsTabData() {
        const actionsTabData = {
            actions: this.formData.incident.actionsTaken
        };

        return actionsTabData;
    }

    // Store ACTIONS tab form data
    setActionsTabData(data: any) {

        this.isActionsTabFormValid = true;
        this.formData.incident.actionsTaken = data.actions;

        // Set this step as validated in Workflow service
        this.workflowService.validateStep(STEPS.actions);
    }

    // Get NOTIFICATIONS tab form data
    getNotificationsTabData() {
        const notificationsTabData = {
            notifications: this.formData.incident.escalation
        };

        return notificationsTabData;
    }

    // Store NOTIFICATIONS tab form data
    setNotificationsTabData(data: any) {

        this.isNotificationsTabFormValid = true;
        this.formData.incident.escalation = data.notifications;

        // Set this step as validated in Workflow service
        this.workflowService.validateStep(STEPS.notifications);
    }

    // Get ASSETS tab form data
    getAssetsTabData() {
        const assetsTabData = {
            assets: this.formData.incident.assetsFixed
        };

        return assetsTabData;
    }

    // Store ASSETS tab form data
    setAssetsTabData(data: any) {

        this.isAssetsTabFormValid = true;
        this.formData.incident.assetsFixed = data.assets;

        console.log(this.formData.incident);

        // Set this step as validated in Workflow service
        this.workflowService.validateStep(STEPS.assets);
    }

    resetFormData() {
        // Reset the workflow
        this.workflowService.resetSteps();

        // Clear form data - do not clear site/cat tree values, as it can be re-used if user wants to enter another incidnet
        this.formData.incident = new IncidentVO();
        this.formData.selectedSite = [];
        this.formData.selectedCategory = [];
        this.formData.fileToUpload = null;

        // reset all form steps to false
        this.isSiteTabFormValid
        = this.isCategoryTabFormValid
        = this.isLocationTabFormValid
        = this.isDetailsTabFormValid
        = this.isPeopleTabFormValid
        = this.isActionsTabFormValid
        = this.isNotificationsTabFormValid
        = this.isAssetsTabFormValid
        = false;
    }
  }
