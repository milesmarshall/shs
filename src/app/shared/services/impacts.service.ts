import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'environments/environment';

import { INCIDENT_TYPES } from 'app/app.constants';
import { INCIDENT_TYPES_FILE } from 'app/app.constants';
import { ImpactVO } from '../models/impactVO.model';

declare var require: any;

@Injectable({
  providedIn: 'root'
})
export class ImpactsService {

    luImpacts: ImpactVO[] = [];

    constructor(private httpClient: HttpClient) { }

    // Query database to get lookup data set specified via action
    getLookups( action, id=null, siteId=null) {
      const headers = new HttpHeaders();
      headers.append('Content-Type', 'application/json');

      let params;

      if (action === 'sites') {
        params = new HttpParams()
          .set('action', action)
          .set('siteId', id);
      } else if (action === 'categories') {
        params = new HttpParams()
          .set('action', action)
          .set('categoryId', id)
          .set('siteId', siteId);
      } else {
        params = new HttpParams()
          .set('action', action)
      }

      return this.httpClient.get(`${environment.apiUrl}/lookups`, { headers: headers, params: params });
    }

    getImpactAutoNotificationResources(impactId: number, siteId: number, categoryId: number) {
        const params = new HttpParams()
            .set('action', 'getImpactAutoNotificationResources')
            .set('impactId', impactId.toString())
            .set('siteId', siteId.toString())
            .set('categoryId', categoryId.toString());

            return this.httpClient.get(`${environment.apiUrl}/impacts`, { params: params });
    }

}
