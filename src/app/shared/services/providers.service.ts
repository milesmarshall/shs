import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProvidersService {

  constructor(private httpClient: HttpClient) { }

  public getServiceProviders(sessionToken, serviceProviderId){
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    
    let params = new HttpParams().set("sessionToken",sessionToken).set("serviceProviderId",serviceProviderId);

    return this.httpClient.get(`${environment.apiUrl}/serviceProviders`, { headers: headers, params: params });
  }
}
