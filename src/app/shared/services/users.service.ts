import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private httpClient: HttpClient) { }

    public getUsers() {
      // to do - replace permanent session token with login access token
      return this.httpClient.get(`${environment.apiUrl}/resources?sessionToken=565eb2bcc283b0.06665848`);
      return this.httpClient.get('https://apiangulardev.myincidentdesk.com/lookups?sessionToken=000&action=status');
    }
}
