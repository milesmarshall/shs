import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';

@Injectable({
    providedIn: 'root'
  })

  export class RootCausesService {

    constructor(
        private httpClient: HttpClient
        ) { }

    // Get all root causes assets for selected cat
    getRootCausesForCat(catId: number) {

        const params = new HttpParams()
            .set('action', 'getRootCausesForCat')
            .set('categoryId', catId.toString());

        return this.httpClient.get(`${environment.apiUrl}/rootCauses`, { params: params });
    }
}
