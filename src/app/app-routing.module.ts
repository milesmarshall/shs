import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';

import { FullLayoutComponent } from './layouts/full/full-layout.component';
import { ContentLayoutComponent } from './layouts/content/content-layout.component';
import { NewsComponent } from './news/news.component';

import { Full_ROUTES } from './shared/routes/full-layout.routes';
import { CONTENT_ROUTES } from './shared/routes/content-layout.routes';

import { AuthGuard } from './shared/auth/auth-guard.service';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard/dashboard',
    pathMatch: 'full',
  },
  { path: '', component: FullLayoutComponent, data: { title: 'Pages with sidebar etc' }, children: Full_ROUTES, canActivate: [AuthGuard] },
  // do not call AuthGaurd on ContentLayoutComponent as these pages need to be accessible if logged out - eg login, forgot password etc
  { path: '', component: ContentLayoutComponent, data: { title: 'Pages without sidebar etc' }, children: CONTENT_ROUTES },
  { path: 'news', component: NewsComponent },
  {
    path: '**',
    redirectTo: 'pages/error'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
