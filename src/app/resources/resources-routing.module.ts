import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TrackerComponent } from "./tracker/tracker.component";

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'tracker',
        component: TrackerComponent,
        data: {
          title: 'Resource Tracker'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResourcesRoutingModule { }
