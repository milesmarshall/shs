import { Component, OnInit, ViewChild, ViewEncapsulation, ChangeDetectionStrategy, TemplateRef} from '@angular/core';
import { DatatableComponent } from "@swimlane/ngx-datatable";
import { NgbTabChangeEvent } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-fixed',
  templateUrl: './fixed.component.html',
  styleUrls: ['./fixed.component.css']
})
export class FixedComponent implements OnInit {

  constructor() { }

  t: any;
  currentJustify = 'start';
  currentOrientation = 'horizontal';

  public beforeChange($event: NgbTabChangeEvent) {
    if ($event.nextId === 'bar') {
      $event.preventDefault();
    }
  };
  
  ngOnInit(): void {
  }

}
