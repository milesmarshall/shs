import { Component, OnInit, ViewChild, ViewEncapsulation, ChangeDetectionStrategy, TemplateRef} from '@angular/core';
import { NgbTooltip } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-options',
  templateUrl: './options.component.html',
  styleUrls: ['./options.component.css']
})
export class OptionsComponent implements OnInit {
  @ViewChild('x') public tooltip: NgbTooltip;

  constructor() { }

  public show:boolean = false;
  
  toggle() {
    this.show = !this.show;
  }

  ngOnInit(): void {
  }

}
