import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PublicComponent } from "./public/public.component";

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'public',
        component: PublicComponent,
        data: {
          title: 'Public Communications'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommunicationsRoutingModule { }
