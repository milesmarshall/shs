import { Component, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent } from "@swimlane/ngx-datatable";

import { LookupsService } from 'app/shared/services/lookups.service';

declare var require: any;
const data: any = require('../../shared/data/company.json');

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {
  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor(
    private lookupsService:LookupsService
  ) { 
    this.temp = [...data];
    this.rows = data;
  }

  categories = [];

  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp.filter(function (d) {
        return d.name.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.rows = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  isCollapsed = false;
  public show:boolean = false;

  rows = [];

  temp = [];

  // Table Column Titles
  columns = [
        { prop: 'name' },
        { name: 'Company' },
        { name: 'Gender' }
  ];

  toggle() {
    this.show = !this.show;
  }

  selectCategory(val: any){
    console.log(val);
  }

  ngOnInit(): void {
    this.lookupsService.getCategories();
  }

}
