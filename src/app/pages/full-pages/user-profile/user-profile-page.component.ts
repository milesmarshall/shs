import { Component, OnInit } from '@angular/core';

import { UserVO } from 'app/shared/models/userVO.model';

@Component({
    selector: 'app-user-profile-page',
    templateUrl: './user-profile-page.component.html',
    styleUrls: ['./user-profile-page.component.scss']
})

export class UserProfilePageComponent implements OnInit {
  userVO: UserVO = new UserVO();

  //Variable Declaration
  currentPage: string = "About"

  ngOnInit() {
    this.userVO = JSON.parse(localStorage.getItem('userData'));
    console.log('this.userVO');
    console.log(this.userVO);
  }

  showPage(page: string) {
      this.currentPage = page;
  }
}
