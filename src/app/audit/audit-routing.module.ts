import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LogComponent } from "./log/log.component";

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'log',
        component: LogComponent,
        data: {
          title: 'Audit Log'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuditRoutingModule { }