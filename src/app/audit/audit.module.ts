import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuditRoutingModule } from './audit-routing.module';
import { LogComponent } from './log/log.component';


@NgModule({
  declarations: [LogComponent],
  imports: [
    CommonModule,
    AuditRoutingModule
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AuditModule { }
