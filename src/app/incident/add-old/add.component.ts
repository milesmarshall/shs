import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
//import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})

export class AddComponent implements OnInit {

  closeResult: string;

  constructor(
    //private modalService: NgbModal,
    private router: Router,
    private route: ActivatedRoute
    ) { }

  // Open default modal
  /*open(content) {
      this.modalService.open(content).result.then((result) => {
          this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
  }*/

  // This function is used in open
  /*private getDismissReason(reason: any): string {
      if (reason === ModalDismissReasons.ESC) {
          return 'by pressing ESC';
      } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
          return 'by clicking on a backdrop';
      } else {
          return `with: ${reason}`;
      }
  }*/

  // Open modal with dark section
  //openModal(customContent) {
  //    this.modalService.open(customContent, { windowClass: 'dark-modal' });
  //}

  // Open content with dark section
  /*openContent() {
      const modalRef = this.modalService.open(NgbdModalContent);
      modalRef.componentInstance.name = 'World';
  }*/

  ngOnInit() {
    //this.router.navigate(['/forms/ngx/wizard'], { skipLocationChange: true });
    this.router.navigate(['/incident/add'], { skipLocationChange: true });
  }

}
