import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AddRoutingModule } from './add-routing.module';

/* App Root */
import { AddComponent } from './add.component';
import { NavbarComponent } from './navbar/navbar.component';

/* Feature Components */

/* Shared Service */
import { FormDataService } from './data/formData.service';
import { WorkflowService } from './workflow/workflow.service';
import { CommonModule } from "@angular/common";

@NgModule({
  declarations: [AddComponent, NavbarComponent],
  imports: [
    CommonModule,
    AddRoutingModule,
    FormsModule
  ],
  exports: [
    NavbarComponent
  ],
  providers: [{ provide: FormDataService, useClass: FormDataService },
    { provide: WorkflowService, useClass: WorkflowService }],
})
export class AddModule { }