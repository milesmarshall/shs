import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddComponent } from "./add.component";
//import { PersonalComponent }  from './personal/personal.component';
//import { WorkComponent }      from './work/work.component';
//import { AddressComponent }   from './address/address.component';
//import { ResultComponent }    from './result/result.component';

//import { AddComponent } from './archwizard/archwizard.component';

const routes: Routes = [
  {
    path: '',
     component: AddComponent,
    data: {
      title: 'add-wizard'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddRoutingModule { }

export const routedComponents = [AddComponent];
