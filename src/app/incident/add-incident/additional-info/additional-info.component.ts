import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AddIncidentService } from 'app/shared/services/add-incident.service';
import { WorkflowService } from '../workflow/workflow.service';
import { STEPS } from '../workflow/workflow.model';

@Component({
  selector: 'app-additional-info',
  templateUrl: './additional-info.component.html',
  styleUrls: ['./additional-info.component.css']
})
export class AdditionalInfoComponent implements OnInit {
  title = 'Please enter additional info.';
  additionalInfoTabData: any;
  form: any;
  buttonDisabled = true;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private addIncidentService: AddIncidentService,
    private workflowService: WorkflowService,
  ) { }

  ngOnInit(): void {
    // get tab data from stored formdata - in case user is returning to tab
    this.additionalInfoTabData = this.addIncidentService.getAdditionalInfoTabData();
  }

  // save form data
  saveData(form: any, saveToDatabase = false) {

    if (!form.valid) {
      return;
    }

    // store step data in service
    this.addIncidentService.setAdditionalInfoTabData(this.additionalInfoTabData);

    const firstState = this.workflowService.getFirstInvalidStep(STEPS.people);

    if (saveToDatabase) {
      this.router.navigate(['result'], { relativeTo: this.route.parent, skipLocationChange: true });
    } else {
       // proceed to next step in wizard
      this.router.navigate(['actions'], { relativeTo: this.route.parent, skipLocationChange: true });
    }
  }

  // Return to previous step
  previousStep() {
    this.router.navigate(['people'], { relativeTo: this.route.parent, skipLocationChange: true });
  }

  // Cancel & return to first step
  cancelStep() {
    this.addIncidentService.resetFormData();
    this.router.navigate(['sites'], { relativeTo: this.route.parent, skipLocationChange: true });
  }

}
