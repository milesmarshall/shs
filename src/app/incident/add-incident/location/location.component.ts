import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AddIncidentService } from 'app/shared/services/add-incident.service';
import { STEPS } from '../workflow/workflow.model';
import { WorkflowService } from '../workflow/workflow.service';

@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.css']
})
export class LocationComponent implements OnInit {

  title = 'Please select the incident location';
  locationTabData: any;   // workType: string;
  form: any;
  buttonDisabled = false; // MJA TO DO - once map has been installed - set to false

  // Google map lat-long - temp values to show map
  // lat: number = -33.918861;
  // lng: number = 18.423300;

  lat: number = 51.678418;
  lng: number = 7.809007;


  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private addIncidentService: AddIncidentService,
    private workflowService: WorkflowService,
    private cd: ChangeDetectorRef) {}

  ngOnInit(): void {
    this.locationTabData = this.addIncidentService.getLocationTabData();
  }

  // save location data
  saveData(form: any) {
    if (!form.valid) {
      return;
    }

    // TO DO - implement map with GPS points etc....

    this.addIncidentService.setLocationTabData(this.locationTabData);

    const firstState = this.workflowService.getFirstInvalidStep(STEPS.location);
    // proceed to next step in wizard
    this.router.navigate(['details'], { relativeTo: this.route.parent, skipLocationChange: true });
}

// Cancel wizard
cancelStep() {
    // return to first step of wizard
    this.router.navigate(['sites'], { relativeTo: this.route.parent, skipLocationChange: true });
}

}
