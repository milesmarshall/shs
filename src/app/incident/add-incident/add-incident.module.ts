import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'app/shared/shared.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { AgmCoreModule } from '@agm/core';


/* Module Root */
import { AddIncidentComponent } from './add-incident.component';
import { AddIncidentRoutingModule } from './add-incident.routing.module';
import { AddIncidentNavbarComponent } from './add-incident-navbar/add-incident-navbar.component';

/* Feature Components */
import { SitesComponent } from './sites/sites.component';
import { CategoriesComponent } from './categories/categories.component';
import { LocationComponent } from './location/location.component';
import { DetailsComponent } from './details/details.component';
import { PeopleComponent } from './people/people.component';

/* Shared Services & Components */
/*
import { FormDataService } from './data/formData.service';*/
import { WorkflowService } from './workflow/workflow.service';
import { ResultComponent } from './result/result.component';
import { ActionsComponent } from './actions/actions.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { AssetsComponent } from './assets/assets.component';
import { AdditionalInfoComponent } from './additional-info/additional-info.component';

/* Pipes */



@NgModule({
  declarations: [
    AddIncidentComponent,
    CategoriesComponent,
    SitesComponent,
    LocationComponent,
    DetailsComponent,
    PeopleComponent,
    AddIncidentNavbarComponent,
    ResultComponent,
    ActionsComponent,
    NotificationsComponent,
    AssetsComponent,
    AdditionalInfoComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    NgxDatatableModule,
    FormsModule,
    ReactiveFormsModule,
    AddIncidentRoutingModule,
    AgmCoreModule // Google maps
  ],
  providers: [
    // { provide: FormDataService, useClass: FormDataService },
    { provide: WorkflowService, useClass: WorkflowService }
  ],
  // providers: [{ provide: FormDataService, useClass: FormDataService },
  // schemas: [ CUSTOM_ELEMENTS_SCHEMA ] MJA does this need to be here?
})

export class AddIncidentModule { }
