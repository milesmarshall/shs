export const STEPS = {
    sites: 'sites',
    categories: 'categories',
    location: 'location',
    details: 'details',
    people: 'people',
    additionalInfo: 'additionalInfo',
    actions: 'actions',
    notifications: 'notifications',
    assets: 'assets',
    result: 'result'
}
