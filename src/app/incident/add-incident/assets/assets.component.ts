import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AssetFixedVO } from 'app/shared/models/assetFixedVO.model';
import { AddIncidentService } from 'app/shared/services/add-incident.service';
import { AssetsService } from 'app/shared/services/assets.service';
import { map } from 'rxjs/operators';
import { STEPS } from '../workflow/workflow.model';
import { WorkflowService } from '../workflow/workflow.service';

@Component({
  selector: 'app-assets',
  templateUrl: './assets.component.html',
  styleUrls: ['./assets.component.css']
})
export class AssetsComponent implements OnInit {
  title = 'Please select fixed assets(optional)';
  assetsTabData: any;
  form: any;
  allAssets = [];
  allAssetsForSiteCat: AssetFixedVO[] = [];
  selectedSiteId: number;
  selectedCatId: number;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private addIncidentService: AddIncidentService,
    private workflowService: WorkflowService,
    private assetsService: AssetsService
  ) { }

  ngOnInit(): void {
    // get tab data from stored formdata - in case user is returning to tab
    this.assetsTabData = this.addIncidentService.getAssetsTabData();

    // get selected site, cat to fetch assets
    const allFormData = this.addIncidentService.formData;
    this.selectedSiteId = allFormData.incident.siteId;
    this.selectedCatId = allFormData.incident.categoryId;

    // Get all FIXED assets for selected site & cat
    this.assetsService.getFixedAssetsForSiteCat(1, this.selectedSiteId, this.selectedCatId)
        .pipe(
          map(responseData => {

            const allFixedAssets = [];

            if (responseData.hasOwnProperty('result')) {
              // convert js responsedata to array of typed model objects
              for (let i = 0; i < responseData['result'].length; i++) {
                const jsonObj = responseData['result'][i];
                const fixedAsset = new AssetFixedVO().deserialize(jsonObj);
                allFixedAssets.push(fixedAsset);
              }
          }
            return allFixedAssets;
          })
        )
        .subscribe((allFixedAssets) => {
            this.allAssetsForSiteCat = allFixedAssets;
            console.log(this.allAssetsForSiteCat);
            // this.cd.detectChanges();
        });
  }

  // add asset to incident data
  addAsset(form: NgForm) {
    const formValues = form.value;
    const selectedAsset: AssetFixedVO = this.allAssetsForSiteCat.find(i => i.assetId === formValues.selectedAsset);
    this.allAssets.push(selectedAsset);

    form.resetForm();
  }

  // save form data
  saveData() {
    this.assetsTabData.assets = this.allAssets;

    // store step data in service
    this.addIncidentService.setAssetsTabData(this.assetsTabData);

    const firstState = this.workflowService.getFirstInvalidStep(STEPS.notifications);

    this.router.navigate(['result'], { relativeTo: this.route.parent, skipLocationChange: true });

  }

  // Return to previous step
  previousStep() {
    this.router.navigate(['notifications'], { relativeTo: this.route.parent, skipLocationChange: true });
  }

  // Cancel & return to first step
  cancelStep() {
    this.addIncidentService.resetFormData();
    this.router.navigate(['sites'], { relativeTo: this.route.parent, skipLocationChange: true });
  }

}
