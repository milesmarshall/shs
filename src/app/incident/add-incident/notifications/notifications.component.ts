import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'app/shared/auth/auth.service';
import { ActionVO } from 'app/shared/models/actionVO.model';
import { NotificationVO } from 'app/shared/models/notificationVO.model';
import { ResourceVO } from 'app/shared/models/resourceVO.model';
import { User } from 'app/shared/models/user.model';
import { AddIncidentService } from 'app/shared/services/add-incident.service';
import { ImpactsService } from 'app/shared/services/impacts.service';
import { map } from 'rxjs/operators';
import { STEPS } from '../workflow/workflow.model';
import { WorkflowService } from '../workflow/workflow.service';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {
  title = 'Please enter notifications (optional)';
  notificationsTabData: any;
  form: any;
  newNotifications = [];
  notifyOfList = []; // list of incident/actions user can be notified of. When incident is created, actions don't have id's yet, so action description is saved to identify specific action to notify of until it has an id that can be saved in db
  luRgResources: ResourceVO[] = [];
  loggedInUser;
  disableEmail = true;
  disableSms = true;
  selectedSiteId: number;
  selectedCatId: number;
  selectedImpactId: number;
  impactAutoNotifications = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private addIncidentService: AddIncidentService,
    private workflowService: WorkflowService,
    private authService: AuthService,
    private impactsService: ImpactsService
  ) { }

  ngOnInit(): void {

    // get tab data from stored formdata - in case user is returning to tab
    this.notificationsTabData = this.addIncidentService.getNotificationsTabData();
    this.newNotifications = this.notificationsTabData.notifications;

    console.log('this.newNotifications:');
    console.log(this.newNotifications);

    const loggedInUser: User = this.authService.getLoggedInUser();
    this.loggedInUser = loggedInUser.id;

    // get selected site, cat & impact to filter data
    const allFormData = this.addIncidentService.formData;
    this.selectedSiteId = allFormData.incident.siteId;
    this.selectedCatId = allFormData.incident.categoryId;
    this.selectedImpactId = allFormData.incident.impactId;

    /* Get list of items user can be notified of (main incident + all actions)
    When incident is created, actions don't have id's yet, so action description is saved to identify
    specific action to notify of until it has an id.*/
    this.notifyOfList = [];
    this.notifyOfList.push('Main Incident');
    const actionsTabData = this.addIncidentService.getActionsTabData();
    const allActions =  actionsTabData.actions;

    for (let i = 0; i < allActions.length; i++) {
      this.notifyOfList.push(allActions[i].description);
    }


    // TO DO: get resources for selected site & cat & excl any that are auto notified
    // get resources for dropdown - to do get filtered resources
    this.luRgResources = JSON.parse(sessionStorage.getItem('rgResources'));

    // Get resources that will be auto notified
    this.impactsService.getImpactAutoNotificationResources(this.selectedImpactId, this.selectedSiteId, this.selectedCatId)
        .pipe(
          map(responseData => {
            const autoNotifications: {name: string, email: string, cellNumber: string, method: string}[] = [];

            if (responseData.hasOwnProperty('result')) {
              // convert js responsedata to array of typed model objects
              for (let i = 0; i < responseData['result'].length; i++) {
                const jsonObj = responseData['result'][i];
                const notification = {
                  'name': jsonObj.firstName + ' ' + jsonObj.lastName,
                  'email':  jsonObj.email,
                  'cellNumber': jsonObj.cellNumber,
                  'method': jsonObj.method // 0=email, 1=cell, 2=both
                }

                autoNotifications.push(notification);
              }
          }

            return autoNotifications;
          })
        )
        .subscribe((autoNotifications) => {
            this.impactAutoNotifications = autoNotifications;

            console.log(this.impactAutoNotifications);
            // this.cd.detectChanges();
        });

  }

  // Onselect of resource, enable/disable available notification methods
  selectResource(selectedResourceId: number) {

    const selectedResource: ResourceVO = this.luRgResources.find(i => i.id === selectedResourceId);

    // disable/enable checkboxes based on user's setup
    this.disableEmail = selectedResource.email === null || selectedResource.email === '' ? true : false;
    this.disableSms = selectedResource.cellNumber === null || selectedResource.cellNumber === '' ? true : false;
  }

  // add new notification to list
  addNotification(form: NgForm) {
    const formValues = form.value;

    const newNotification = new NotificationVO();
    newNotification.resourceId = formValues.notifyResource;
    newNotification.description = formValues.notifyOf;
    newNotification.email = formValues.notifyEmail === true ? 1 : 0;
    newNotification.sms = formValues.notifySms === true ? 1 : 0;
    newNotification.timeStamp = new Date().toString();
    this.newNotifications.push(newNotification);

    form.resetForm();
    this.disableEmail = true;
    this.disableSms = true;
  }

  // Save form data
  saveData(saveToDatabase = false) {

    this.notificationsTabData.notifications = this.newNotifications;

    // store step data in service
    this.addIncidentService.setNotificationsTabData(this.notificationsTabData);

    const firstState = this.workflowService.getFirstInvalidStep(STEPS.actions);

    if (saveToDatabase) {
      this.router.navigate(['result'], { relativeTo: this.route.parent, skipLocationChange: true });
    } else {
       // proceed to next step in wizard
      this.router.navigate(['assets'], { relativeTo: this.route.parent, skipLocationChange: true });
    }
  }

  // Return to previous step
  previousStep() {
    this.router.navigate(['actions'], { relativeTo: this.route.parent, skipLocationChange: true });
  }

  // Cancel & return to first step
  cancelStep() {
    this.addIncidentService.resetFormData();
    this.router.navigate(['sites'], { relativeTo: this.route.parent, skipLocationChange: true });
  }

}
