import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddIncidentComponent } from './add-incident.component';
import { SitesComponent } from './sites/sites.component';
import { CategoriesComponent } from './categories/categories.component';
import { LocationComponent } from './location/location.component';
import { DetailsComponent } from './details/details.component';
import { PeopleComponent } from './people/people.component';
import { ResultComponent } from './result/result.component';
import { ActionsComponent } from './actions/actions.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { AssetsComponent } from './assets/assets.component';
import { AdditionalInfoComponent } from './additional-info/additional-info.component';


const routes: Routes = [
  {
    path: '',
    component: AddIncidentComponent,
    data: {
      title: 'incident-wizard'
    },
    children: [
      {
        path: 'sites',
        component: SitesComponent,
        data: {
          title: 'Sites'
        }
      },
      {
        path: 'categories',
        component: CategoriesComponent,
        data: {
          title: 'Categories'
        }
      },
      {
        path: 'location',
        component: LocationComponent,
        data: {
          title: 'Location'
        }
      },
      {
        path: 'details',
        component: DetailsComponent,
        data: {
          title: 'Details'
        }
      },
      {
        path: 'people',
        component: PeopleComponent,
        data: {
          title: 'People'
        }
      },
      {
        path: 'additionalInfo',
        component: AdditionalInfoComponent,
        data: {
          title: 'Additional Info'
        }
      },
      {
        path: 'actions',
        component: ActionsComponent,
        data: {
          title: 'Actions'
        }
      },
      {
        path: 'notifications',
        component: NotificationsComponent,
        data: {
          title: 'Notifications'
        }
      },
      {
        path: 'assets',
        component: AssetsComponent,
        data: {
          title: 'Assets'
        }
      },
      {
        path: 'result',
        component: ResultComponent,
        data: {
          title: 'Result'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddIncidentRoutingModule { }

export const routedComponents = [AddIncidentComponent];
