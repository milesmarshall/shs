import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-add-incident',
  templateUrl: './add-incident.component.html',
  styleUrls: ['./add-incident.component.css']
})

export class AddIncidentComponent implements OnInit {

    constructor(private cd: ChangeDetectorRef, private router: Router, private route: ActivatedRoute) {}

    ngOnInit() {
      // when component opens, immediately go to specified path / step 1 in wizard
      this.router.navigate(['/incident/add-incident/sites'], { skipLocationChange: true });
    }

}
