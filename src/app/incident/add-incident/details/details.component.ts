import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'app/shared/auth/auth.service';
import { ImpactVO } from 'app/shared/models/impactVO.model';
import { ResourceVO } from 'app/shared/models/resourceVO.model';
import { RootCauseVO } from 'app/shared/models/rootCauseVO.model';
import { StatusVO } from 'app/shared/models/statusVO.model';
import { User } from 'app/shared/models/user.model';
import { AddIncidentService } from 'app/shared/services/add-incident.service';
import { RootCausesService } from 'app/shared/services/root-causes.service';
import { Observable } from 'rxjs';
import { STEPS } from '../workflow/workflow.model';
import { WorkflowService } from '../workflow/workflow.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {

  title = 'Please enter the incident details & upload any relevant documents';
  detailsTabData: any;
  form: any;
  buttonDisabled = true;
  luRootCauses: RootCauseVO[] = [];
  luImpacts: ImpactVO[] = [];
  luRgResources: ResourceVO[] = [];
  luStatuses: StatusVO[] = [];

  // file upload
  selectedFiles: FileList;
  currentFile: File;
  progress = 0;
  message = '';
  fileInfo: Observable<any>;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private addIncidentService: AddIncidentService,
    private workflowService: WorkflowService,
    private cd: ChangeDetectorRef,
    private authService: AuthService,
    private rootCausesService: RootCausesService
  ) { }

  ngOnInit(): void {
    // get incident data from stored formdata - in case user is returning to tab
    this.detailsTabData = this.addIncidentService.getDetailsTabData();

    // reported by - default to logged in user
    console.log('[details comp] init - reportedResourceId: ' + this.detailsTabData.reportedResourceId);
    if (this.detailsTabData.reportedResourceId === 0) {
      const loggedInUser: User = this.authService.getLoggedInUser();
      this.detailsTabData.reportedResourceId = loggedInUser.id;
    }

    // get selected cat from stored form data
    const allFormData = this.addIncidentService.formData;

    // Get all rootcauses for selected cat
    this.rootCausesService.getRootCausesForCat(allFormData.incident.categoryId)
        .pipe(
          map(responseData => {

            const allRootCauses = [];

            if (responseData.hasOwnProperty('result')) {
              // convert js responsedata to array of typed model objects
              for (let i = 0; i < responseData['result'].length; i++) {
                const jsonObj = responseData['result'][i];
                const rootCause = new RootCauseVO().deserialize(jsonObj);
                allRootCauses.push(rootCause);
              }
          }
            return allRootCauses;
          })
        )
        .subscribe((allRootCauses) => {
            this.luRootCauses = allRootCauses;
            console.log(this.luRootCauses);
            // this.cd.detectChanges();
        });

    this.luImpacts = JSON.parse(sessionStorage.getItem('impacts'));
    this.luStatuses = JSON.parse(sessionStorage.getItem('statuses'));
    this.luRgResources = JSON.parse(sessionStorage.getItem('rgResources')); // filter resources??

    this.cd.markForCheck();
  }

  selectFile(event): void {
    this.selectedFiles = event.target.files;

    console.log(this.selectedFiles);
  }

  // save details tab data & move to next step
  saveData(form: any, saveToDatabase = false) {
    if (!form.valid) {
      return;
    }

    // this.detailsTabData.fileToUpload = this.selectedFiles.item(0);
    this.addIncidentService.setDetailsTabData(this.detailsTabData);

    const firstState = this.workflowService.getFirstInvalidStep(STEPS.details);

    if (saveToDatabase) {
      // this.addIncidentService.saveIncident();
      // TO DO - move save function to result component - call ON SHOW of component.....
      this.router.navigate(['result'], { relativeTo: this.route.parent, skipLocationChange: true });
    } else {
       // proceed to next step in wizard
      this.router.navigate(['people'], { relativeTo: this.route.parent, skipLocationChange: true });
    }
  }

  // Return to previous step
  previousStep() {
    this.router.navigate(['location'], { relativeTo: this.route.parent, skipLocationChange: true });
  }

  // Cancel & return to first step
  cancelStep() {
    this.addIncidentService.resetFormData();
    this.router.navigate(['sites'], { relativeTo: this.route.parent, skipLocationChange: true });
  }

}
