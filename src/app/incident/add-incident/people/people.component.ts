import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AssetPeopleVO } from 'app/shared/models/assetPeopleVO.model';
import { AddIncidentService } from 'app/shared/services/add-incident.service';
import { AssetsService } from 'app/shared/services/assets.service';
import { map } from 'rxjs/operators';
import { STEPS } from '../workflow/workflow.model';
import { WorkflowService } from '../workflow/workflow.service';

import { SITE_URL } from 'app/app.constants';
//import { FACIAL_IMAGES_URL } from 'app/app.constants';

@Component({
  selector: 'app-people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.css']
})
export class PeopleComponent implements OnInit {

  title = 'Please add any relevant people(optional)';
  peopleTabData: any;
  form: any;
  searchString: string = '';
  allPeople: AssetPeopleVO[] = [];
  allPeopleCashed: AssetPeopleVO[] = []; // keep cached version of full dataset - used in filtering
  selectedCatId: number;
  selectedPeople = [];
  //facialImagesUrl = FACIAL_IMAGES_URL;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private addIncidentService: AddIncidentService,
    private workflowService: WorkflowService,
    private cd: ChangeDetectorRef,
    private assetsService: AssetsService
  ) { }

  ngOnInit(): void {
    this.peopleTabData = this.addIncidentService.getPeopleTabData();

    // get selected cat to fetch assets
    const allFormData = this.addIncidentService.formData;
    this.selectedCatId = allFormData.incident.categoryId;

    // Get all PEOPLE assets for selected cat in same resourceGroup as logged-in user
    this.assetsService.getPeopleForRGCat(this.selectedCatId)
      .pipe(
        map(responseData => {

          const allPeopleAssets = [];

          if (responseData.hasOwnProperty('result')) {
            // convert js responsedata to array of typed model objects
            for (let i = 0; i < responseData['result'].length; i++) {
              const jsonObj = responseData['result'][i];
              const peopleAsset = new AssetPeopleVO().deserialize(jsonObj);
              allPeopleAssets.push(peopleAsset);
            }
          }
          return allPeopleAssets;
        })
      )
      .subscribe((allPeopleAssets) => {
        this.allPeople = allPeopleAssets;
        this.allPeopleCashed = allPeopleAssets;
        console.log(this.allPeople);
        // this.cd.detectChanges();
      });
  }

  // Add selected person to list of people assets for incident
  addPerson(selectedAssetId: number) {

    const selectedPerson: AssetPeopleVO = this.allPeople.find(i => i.assetId === selectedAssetId);
    this.selectedPeople.push(selectedPerson);

    this.cd.detectChanges();

    console.log(this.selectedPeople.length);
    console.log(this.selectedPeople);
  }

  removePerson(selectedAssetId: number) {

    let i = this.selectedPeople.length;
    while (i--) {
      if (this.selectedPeople[i].assetId === selectedAssetId) {
        this.selectedPeople.splice(i, 1);
      }
    }
  }

  // People list filter
  filterPeople(event = null) {

    const freeText = event.target.value.toLowerCase();

    if (freeText === '') {
      this.allPeople = this.allPeopleCashed;
    } else {
      // apply free text filter criteria to each person in list
      const filteredData = this.allPeople.filter(function (data) {

        const idNumber = data.idNumber ? data.idNumber.toLowerCase() : '';

        return (data.name.toLowerCase().indexOf(freeText) !== -1
          || data.lastName.toLowerCase().indexOf(freeText) !== -1
          || idNumber.indexOf(freeText) !== -1
          || !freeText);
      });

      this.allPeople = filteredData;
    }
  }

  // save people data
  saveData(form: any) {
    if (!form.valid) {
      return;
    }

    // MJA TO DO - investigate saving ALL PEOPLE in form data - could save initial query if user returns to tab
    // will need to clear if cat changes
    this.addIncidentService.setPeopleTabData(this.peopleTabData);

    const firstState = this.workflowService.getFirstInvalidStep(STEPS.people);
    // proceed to next step in wizard
    this.router.navigate(['additionalInfo'], { relativeTo: this.route.parent, skipLocationChange: true });
  }

  // Return to previous step
  previousStep() {
    this.router.navigate(['details'], { relativeTo: this.route.parent, skipLocationChange: true });
  }

  // Cancel & return to first step
  cancelStep() {
    this.addIncidentService.resetFormData();
    this.router.navigate(['sites'], { relativeTo: this.route.parent, skipLocationChange: true });
  }

}
