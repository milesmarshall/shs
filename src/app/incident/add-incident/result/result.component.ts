import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { id } from '@swimlane/ngx-charts';
import { AddIncidentService } from 'app/shared/services/add-incident.service';
import { IncidentsService } from 'app/shared/services/incidents.service';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent implements OnInit {
  title = 'Saving Incident...';
  isSaving = false;
  isSaved = false;


  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private addIncidentService: AddIncidentService,
      private incidentsService: IncidentsService) { }

  ngOnInit(): void {

    this.isSaving = true;
    // this.addIncidentService.saveIncident();

    const formData = this.addIncidentService.formData;

    console.log('INCIDENT DATA TO SAVE: ');
    console.log(formData);

    this.incidentsService.saveIncident(formData.incident, formData.fileToUpload)
      .subscribe(
          responseData => {
            console.log('[RESULT - incident saved]');
            console.log(responseData);
            this.isSaving = false;

            if (responseData.status === true) {
              // TO DO ------ REPLACE!!!!!!!!!!!!!!!!!!!!!!!!!!!!
              // this.addIncidentService.resetFormData();
              this.isSaved = true;
              this.title = 'Your incident has been saved!';
            }

        });
  }

 // clear form data after save
  resetForm() {
    this.addIncidentService.resetFormData();
  }

  // Return to first step
  backToStart() {
    this.router.navigate(['sites'], { relativeTo: this.route.parent, skipLocationChange: true });
  }

  // Return to first step
  goToList() {
    this.router.navigate(['/incident/list']);
  }

}
