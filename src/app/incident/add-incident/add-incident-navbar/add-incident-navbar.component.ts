import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-add-incident-navbar',
  templateUrl: './add-incident-navbar.component.html',
  styleUrls: ['./add-incident-navbar.component.css']
})
export class AddIncidentNavbarComponent implements OnInit {

  // set SITES as default first page
  page = 'Sites';

    constructor(private router: Router,
      private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.router.events.pipe(
            filter(event => event instanceof NavigationEnd))
            .subscribe(event => {
                let currentRoute = this.route.root;
                while (currentRoute.children[0] !== undefined) {
                    currentRoute = currentRoute.children[0];
                }
                this.page = currentRoute.snapshot.data['title'];
            })
    }

}
