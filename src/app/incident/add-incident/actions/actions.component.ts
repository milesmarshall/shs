import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AddIncidentService } from 'app/shared/services/add-incident.service';
import { WorkflowService } from '../workflow/workflow.service';
import { STEPS } from '../workflow/workflow.model';
import { ResourceVO } from 'app/shared/models/resourceVO.model';
import { NgForm } from '@angular/forms';
import { ActionVO } from 'app/shared/models/actionVO.model';
import { User } from 'app/shared/models/user.model';
import { AuthService } from 'app/shared/auth/auth.service';
// import { ResourceNamePipe } from 'app/shared/pipes/resource-name.pipe';

@Component({
  selector: 'app-actions',
  templateUrl: './actions.component.html',
  styleUrls: ['./actions.component.css']
})
export class ActionsComponent implements OnInit {
  title = 'Please enter required actions(optional)';
  actionsTabData: any;
  form: any;
  newActions = [];
  luRgResources: ResourceVO[] = [];
  loggedInUser;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private addIncidentService: AddIncidentService,
    private workflowService: WorkflowService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    // get tab data from stored formdata - in case user is returning to tab
    this.actionsTabData = this.addIncidentService.getActionsTabData();
    this.newActions = this.actionsTabData.actions;

    this.luRgResources = JSON.parse(sessionStorage.getItem('rgResources'));

    this.loggedInUser = this.authService.getLoggedInUser();
    // this.loggedInUser = loggedInUser.id;
  }

  addAction(form: NgForm) {

    /*const dateNow = new Date();
    const dateNowString = new Date(dateNow.getTime() - (dateNow.getTimezoneOffset() * 60000 ))
                          .toISOString()
                          .split('T')[0];*/

    const dateNow = new Date();
    const dateNowString = new Date(new Date(dateNow.toISOString()).getTime() -
                          (dateNow.getTimezoneOffset() * 60 * 1000)).toISOString().substr(0, 19);


    const formValues = form.value;

    const newAction = new ActionVO();
    newAction.description = formValues.actionDesc;
    newAction.resourceId = formValues.actionAssignTo;
    newAction.loggedUserId = this.loggedInUser.id;
    newAction.dateTimeStamp = dateNowString; // new Date().toString();
    this.newActions.push(newAction);

    form.resetForm();
  }

  // save form data
  saveData(saveToDatabase = false) {

    this.actionsTabData.actions = this.newActions;

    // store step data in service
    this.addIncidentService.setActionsTabData(this.actionsTabData);

    const firstState = this.workflowService.getFirstInvalidStep(STEPS.people);

    if (saveToDatabase) {
      this.router.navigate(['result'], { relativeTo: this.route.parent, skipLocationChange: true });
    } else {
       // proceed to next step in wizard
      this.router.navigate(['notifications'], { relativeTo: this.route.parent, skipLocationChange: true });
    }
  }

  // Return to previous step
  previousStep() {
    this.router.navigate(['details'], { relativeTo: this.route.parent, skipLocationChange: true });
  }

  // Cancel & return to first step
  cancelStep() {
    this.addIncidentService.resetFormData();
    this.router.navigate(['sites'], { relativeTo: this.route.parent, skipLocationChange: true });
  }

}
