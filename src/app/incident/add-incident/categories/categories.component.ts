import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AddIncidentService } from 'app/shared/services/add-incident.service';
import { STEPS } from '../workflow/workflow.model';
import { WorkflowService } from '../workflow/workflow.service';
import { DatatableComponent, NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ColumnMode, SelectionType } from '@swimlane/ngx-datatable'; // site selector
import { LookupsService } from 'app/shared/services/lookups.service';
import { map } from 'rxjs/operators';
import { CategoryVO } from 'app/shared/models/categoryVO.model';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {

  title = 'Please select a category';
  categoryTabData: any;   // workType: string;
  form: any;
  buttonDisabled = true;
  // @ViewChild(DatatableComponent) catTree: DatatableComponent;

  // category tree
  categoryRows = [];
  ColumnMode = ColumnMode;
  selectedCategory = [];
  SelectionType = SelectionType;
  messages = {emptyMessage: 'Loading categories for site...'};

  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private addIncidentService: AddIncidentService,
      private workflowService: WorkflowService,
      private cd: ChangeDetectorRef,
      private lookupsService: LookupsService) {
  }

  ngOnInit() {

      this.categoryTabData = this.addIncidentService.getCategoryTabData();

      // if user returned to tab, fetch tree from stored form data
      if (this.categoryTabData.categoryTreeRows.length > 0) {
        this.categoryRows = this.categoryTabData.siteTreeRows;
        this.selectedCategory = this.categoryTabData.selectedSite;
        this.cd.markForCheck();
      // ON PAGE OPEN - fetch level 1 cats from local storage
      } else {
        // get level 1 cats filtered on selected site
        const selectedSiteId = this.addIncidentService.formData.incident.siteId;
        this.lookupsService.getLookups('categories', 0, selectedSiteId)
        .pipe(
          map(responseData => {
            const childNodes = [];

            if (responseData.hasOwnProperty('result')) {
              // convert js responsedata to array of typed model objects
              for (let i = 0; i < responseData['result'].length; i++) {
                const jsonObj = responseData['result'][i];
                const newChild = new CategoryVO().deserialize(jsonObj);
                newChild.treeStatus = 'collapsed';
                childNodes.push(newChild);
              }
          }

            if (childNodes.length === 0) {
              this.messages = {emptyMessage: 'No categories found.'};
            }

            return childNodes;
          })
        )
        .subscribe((childNodes) => {
            this.categoryRows = [...childNodes];
            this.cd.detectChanges();
        });
      }
  }

  // save category data
  saveData(form: any) {
      if (!form.valid) {
        return;
      }

      // store tree data in case user returns to tab
      this.categoryTabData.categoryTreeRows = this.categoryRows;
      this.categoryTabData.selectedCategory = this.selectedCategory;
      this.addIncidentService.setCategoryTabData(this.categoryTabData);

      const firstState = this.workflowService.getFirstInvalidStep(STEPS.categories);
      // proceed to next step in wizard
      this.router.navigate(['location'], { relativeTo: this.route.parent, skipLocationChange: true });
  }

  // Cancel wizard
  cancelStep() {
      // return to first step of wizard
      this.router.navigate(['sites'], { relativeTo: this.route.parent, skipLocationChange: true });
  }

  // onClick of tree item
  onTreeAction(event: any) {
    const index = event.rowIndex;
    const row = event.row;

    // if node currently collapsed, open & load children
    if (row.treeStatus === 'collapsed') {
      row.treeStatus = 'loading';

      // if children has not already been loaded, fetch from db
      if (!row.childrenPopulated) {
        const selectedSiteId = this.addIncidentService.formData.incident.siteId;
        // investigate - can ONLY child cat have access to sites? If so, fetching data onclick won't work, as parent without access won't be in tree
        this.lookupsService.getLookups('categories', row.id, selectedSiteId)
        .pipe(
          map(responseData => {

            console.log('GET CHILD CATS');
            console.log(responseData);

            const childNodes = [];

            if (responseData.hasOwnProperty('result')) {
              // convert js responsedata to array of typed model objects
              for (let i = 0; i < responseData['result'].length; i++) {
                const jsonObj = responseData['result'][i];
                const newChild = new CategoryVO().deserialize(jsonObj);
                newChild.treeStatus = 'collapsed';
                childNodes.push(newChild);
              }
          }

            return childNodes;
          })
        )
        .subscribe((childNodes) => {
            row.treeStatus = 'expanded';
            row.childrenPopulated = true;
            // add child nodes to existing tree nodes
            this.categoryRows = [...this.categoryRows, ...childNodes];
            this.cd.detectChanges();
        });
      // if children has already been fetched, open node
      } else {
        row.treeStatus = 'expanded';
        this.categoryRows = [...this.categoryRows];
        this.cd.detectChanges();
      }

    // if node currently open, collapse node
    } else {
      row.treeStatus = 'collapsed';
      this.categoryRows = [...this.categoryRows];
      this.cd.detectChanges();
    }
  }

  // On SELECT of cat
  onSelect({ selected }) {
    this.selectedCategory.splice(0, this.selectedCategory.length);
    this.selectedCategory.push(...selected);

    console.log('SELECTED CAT: ');
    console.log(this.selectedCategory[0].id);

    this.categoryTabData.categoryId = this.selectedCategory[0].id;

    // enable NEXT button in wizard
    this.buttonDisabled = false;
  }

}
