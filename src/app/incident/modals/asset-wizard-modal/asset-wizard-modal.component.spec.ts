import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetWizardModalComponent } from './asset-wizard-modal.component';

describe('AssetWizardModalComponent', () => {
  let component: AssetWizardModalComponent;
  let fixture: ComponentFixture<AssetWizardModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssetWizardModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetWizardModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
