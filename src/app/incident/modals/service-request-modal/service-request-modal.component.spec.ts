import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceRequestModalComponent } from './service-request-modal.component';

describe('ServiceRequestModalComponent', () => {
  let component: ServiceRequestModalComponent;
  let fixture: ComponentFixture<ServiceRequestModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServiceRequestModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceRequestModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
