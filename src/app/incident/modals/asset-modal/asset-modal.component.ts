import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DatatableComponent } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-asset-modal',
  templateUrl: './asset-modal.component.html',
  styleUrls: ['./asset-modal.component.css']
})

export class AssetModalComponent implements OnInit {

  @ViewChild('modalContent', { static: true }) modalContent: ElementRef;
  @ViewChild(DatatableComponent) table: DatatableComponent;
  rows = [];
  temp = [];

  // Table Column Titles
  columns = [
    { prop: 'Name' },
    { name: 'Company' },
    { name: 'Gender' }
  ];

  /* Modal */
  closeResult: string;

  constructor(
    private modalService: NgbModal
    ) { }

  ngOnInit(): void {
  }

  // This function is used in open
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  // Open email modal
  openModal() { // content: string, size: string
    this.modalService.open(this.modalContent, { size: 'l' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp.filter(function (d) {
        return d.name.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.rows = temp;

    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }
}
