import { SharedModule } from './../shared/shared.module';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CommonModule } from '@angular/common';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { IncidentRoutingModule } from './incident-routing.module';
import { ListComponent } from './list/list.component';
import { AddComponent } from './add/add.component';

import { AgmCoreModule } from '@agm/core';
import { EmailModalComponent } from './modals/email-modal/email-modal.component';
import { AssetModalComponent } from './modals/asset-modal/asset-modal.component';
import { PersonModalComponent } from './modals/person-modal/person-modal.component';
import { ActionModalComponent } from './modals/action-modal/action-modal.component';
import { ServiceRequestModalComponent } from './modals/service-request-modal/service-request-modal.component';
import { NoticeModalComponent } from './modals/notice-modal/notice-modal.component';
import { AssetWizardModalComponent } from './modals/asset-wizard-modal/asset-wizard-modal.component';
import { PopiModalComponent } from './modals/popi-modal/popi-modal.component';
import { IncidentStatsComponent } from './list/incident-stats/incident-stats.component';
import { IncidentFilterComponent } from './list/incident-filter/incident-filter.component';
import { IncidentFilterResultsComponent } from './list/incident-filter-results/incident-filter-results.component';
import { IncidentDetailsComponent } from './list/incident-details/incident-details.component';
import { IncidentPublicCommsComponent } from './list/incident-public-comms/incident-public-comms.component';
import { IncidentResearchTrackerComponent } from './list/incident-research-tracker/incident-research-tracker.component';
import { IncidentAuditLogComponent } from './list/incident-audit-log/incident-audit-log.component';
import { IncidentFaceSearchComponent } from './list/incident-face-search/incident-face-search.component';
import { IncidentMainComponent } from './list/incident-main/incident-main.component';
import { IncidentMapComponent } from './list/incident-map/incident-map.component';
import { IncidentFloorPlanComponent } from './list/incident-floor-plan/incident-floor-plan.component';
import { IncidentCalendarComponent } from './list/incident-calendar/incident-calendar.component';
import { IncidentReportsComponent } from './list/incident-reports/incident-reports.component';
import { IncidentReportBuilderComponent } from './list/incident-report-builder/incident-report-builder.component';
import { IncidentAssetsComponent } from './list/incident-assets/incident-assets.component';
import { IncidentListComponent } from './list/incident-list/incident-list.component';
import { IncidentDurationPipe } from 'app/shared/pipes/incident-duration.pipe';
import { IncidentStatusPipe } from 'app/shared/pipes/incident-status.pipe';
// import { ResourceNamePipe } from 'app/shared/pipes/resource-name.pipe';
import { IncidentReportedAssignedPipe } from 'app/shared/pipes/incident-reported-assigned.pipe';

@NgModule({
  declarations: [
    ListComponent,
    AddComponent,
    EmailModalComponent,
    AssetModalComponent,
    PersonModalComponent,
    ActionModalComponent,
    ServiceRequestModalComponent,
    NoticeModalComponent,
    AssetWizardModalComponent,
    PopiModalComponent,
    IncidentStatsComponent,
    IncidentFilterComponent,
    IncidentFilterResultsComponent,
    IncidentDetailsComponent,
    IncidentPublicCommsComponent,
    IncidentResearchTrackerComponent,
    IncidentAuditLogComponent,
    IncidentFaceSearchComponent,
    IncidentMainComponent,
    IncidentMapComponent,
    IncidentFloorPlanComponent,
    IncidentCalendarComponent,
    IncidentReportsComponent,
    IncidentReportBuilderComponent,
    IncidentAssetsComponent,
    IncidentListComponent,
    IncidentDurationPipe,
    IncidentStatusPipe,
    // ResourceNamePipe,
    IncidentReportedAssignedPipe
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IncidentRoutingModule,
    NgbModule,
    SharedModule,
    NgxDatatableModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAFgM81Qz-SwfTzUsr4F51AgDj0HdN88CQ'
    })
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class IncidentModule { }
