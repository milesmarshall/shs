import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnterNotificationsComponent } from './enter-notifications.component';

describe('EnterNotificationsComponent', () => {
  let component: EnterNotificationsComponent;
  let fixture: ComponentFixture<EnterNotificationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnterNotificationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnterNotificationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
