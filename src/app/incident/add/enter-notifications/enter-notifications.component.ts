import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-enter-notifications',
  templateUrl: './enter-notifications.component.html',
  styleUrls: ['./enter-notifications.component.css']
})
export class EnterNotificationsComponent implements OnInit {

  constructor() { }

  /* Map */
  // Google map lat-long
  lat: number = 51.678418;
  lng: number = 7.809007;

  ngOnInit(): void {
  }

}
