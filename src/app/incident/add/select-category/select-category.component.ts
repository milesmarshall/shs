import { Component, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent } from "@swimlane/ngx-datatable";

@Component({
  selector: 'app-select-category',
  templateUrl: './select-category.component.html',
  styleUrls: ['./select-category.component.css']
})
export class SelectCategoryComponent implements OnInit {
  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor(
  ) { }

  categories = [];

  rows = [
    { name: 'Austin', gender: 'Male', company: 'Swimlane' },
    { name: 'Dany', gender: 'Male', company: 'KFC' },
    { name: 'Molly', gender: 'Female', company: 'Burger King' }
  ];
  columns = [{ prop: 'name' }, { name: 'Gender' }, { name: 'Company' }];

  ngOnInit(): void {
    this.getCategories();
  }

  getCategories(){
    this.categories = JSON.parse(sessionStorage.getItem('categories'));
    this.categories = this.categories[0]['result'];

    console.log(this.categories);
  }

}
