import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectAssetsComponent } from './select-assets.component';

describe('SelectAssetsComponent', () => {
  let component: SelectAssetsComponent;
  let fixture: ComponentFixture<SelectAssetsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectAssetsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectAssetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
