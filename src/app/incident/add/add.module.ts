import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AddRoutingModule } from './add-routing.module';
import { AddComponent } from './add.component';
import { SelectSiteComponent } from './select-site/select-site.component';
import { SelectCategoryComponent } from './select-category/select-category.component';
import { EnterNotificationsComponent } from './enter-notifications/enter-notifications.component';
import { EnterIncidentComponent } from './enter-incident/enter-incident.component';
import { PeopleDocumentsComponent } from './people-documents/people-documents.component';
import { AdditionalInfoComponent } from './additional-info/additional-info.component';
import { EnterActionsComponent } from './enter-actions/enter-actions.component';
import { SelectAssetsComponent } from './select-assets/select-assets.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  declarations: [
    AddComponent,
    SelectSiteComponent,
    SelectCategoryComponent,
    EnterNotificationsComponent,
    EnterIncidentComponent,
    PeopleDocumentsComponent,
    AdditionalInfoComponent,
    EnterActionsComponent,
    SelectAssetsComponent,
    NotificationsComponent
  ],
  imports: [
    CommonModule,
    AddRoutingModule,
    NgxDatatableModule,
    FormsModule,
    ReactiveFormsModule
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AddModule { }
