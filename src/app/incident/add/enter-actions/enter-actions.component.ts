import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-enter-actions',
  templateUrl: './enter-actions.component.html',
  styleUrls: ['./enter-actions.component.css']
})
export class EnterActionsComponent implements OnInit {

  resources = [];

  constructor() { }

  ngOnInit(): void {
    this.getResources();
  }

  getResources() {
    this.resources = JSON.parse(sessionStorage.getItem('rgResources'));
  }

}
