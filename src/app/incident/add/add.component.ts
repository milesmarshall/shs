import { Component, OnInit, ViewChild, ElementRef, QueryList, ViewChildren, ChangeDetectorRef } from '@angular/core';

import { NgbSlideEvent, NgbSlideEventSource } from '@ng-bootstrap/ng-bootstrap';
import { NgbCarousel } from '@ng-bootstrap/ng-bootstrap';
import { FormControl } from '@angular/forms';
import { AgmCoreModule } from '@agm/core';

import { GOOGLE_API } from 'app/app.constants';
import { INCIDENTS } from 'app/app.constants';

import { IncidentsService } from 'app/shared/services/incidents.service';

import { IncidentVO } from 'app/shared/models/incidentVO.model';

import { ColumnMode, SelectionType } from '@swimlane/ngx-datatable'; // site selector
import { LookupsService } from 'app/shared/services/lookups.service';
import { SiteVO } from 'app/shared/models/siteVO.model';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})

export class AddComponent implements OnInit {
  @ViewChild('ngcarousel', { static: true }) ngCarousel: NgbCarousel;

  incident: IncidentVO = new IncidentVO();
  googleAPI: string = GOOGLE_API;
  carouselStep;
  EmployeeName = new FormControl();

  // SITE SELECTOR
  siteRows = [];
  lastIndex = 15;
  ColumnMode = ColumnMode;
  selected = [];
  SelectionType = SelectionType;
  // END OF SITE SELECTOR

  constructor(private cd: ChangeDetectorRef, private lookupsService: LookupsService) {
  }

  ngOnInit() {
    this.updateEmployeeName();

    this.ngCarousel.pause();
    this.ngCarousel.pauseOnHover = true;

    console.log('ngOnInit');

    // write new incident to local storage
    this.incident.incidentId = 0;
    localStorage.setItem('incident', JSON.stringify(this.incident));

    // SITE SELECTOR
    this.siteRows = JSON.parse(sessionStorage.getItem('sites'));

    console.log('SITES');
    console.log(this.siteRows);

    this.cd.markForCheck();
    // END OF SITE SELECTOR
  }

  // Move to specific slide
  /*navigateToSlide(item) {
    this.ngCarousel.select(item);
  }*/

  // Move to previous slide
  getToPrev() {
    console.log('getToPrev');
    this.ngCarousel.prev();
  }

  // Move to next slide
  goToNext() {
    console.log('goToNext');

    if (Number(this.ngCarousel.activeId) === 1)
      this.updateIncidentStep0();
    else if (Number(this.ngCarousel.activeId) === 2)
      this.updateIncidentStep1();

    // update local storage
    localStorage.setItem('incident', JSON.stringify(this.incident));

    this.ngCarousel.next();
  }

  updateEmployeeName(){
    this.EmployeeName.setValue('miles');
  }

  updateIncidentStep0(){
    console.log('update category');

    //console.log(this.createForDefault.nativeElement);
    //console.log(document.getElementById('createForAsset').innerHTML);

    //console.log(document.getElementById('createDate').nodeValue);
    //console.log(document.getElementById('createTime').innerHTML);
    //this.incident.description = 'test';
  }

  ngAfterViewInit() {
    //console.log(this.createDate);
    //this.createDate.nativeElement.innerHTML = "I am changed by ElementRef & ViewChild";
  }

  updateIncidentStep1(){
    console.log('update 1');
  }

  // Pause slide
  stopCarousel(ngbSlideEvent: NgbSlideEvent) {
    console.log('stopCarousel');
    this.ngCarousel.pause();

    console.log(ngbSlideEvent.current);
    console.log(ngbSlideEvent.paused == true);

    console.log(ngbSlideEvent.source);
    console.log(ngbSlideEvent.paused);
    console.log(NgbSlideEventSource.INDICATOR);
    console.log(NgbSlideEventSource.ARROW_LEFT);
    console.log(NgbSlideEventSource.ARROW_RIGHT);
  }

  pauseCarousel() {
    this.ngCarousel.pause();
  }

  // Restart carousel
  restartCarousel() {
    this.ngCarousel.cycle();
  }

  /*slideActivate(ngbSlideEvent: NgbSlideEvent) {
    console.log(ngbSlideEvent.source);
    console.log(ngbSlideEvent.paused);
    console.log(NgbSlideEventSource.INDICATOR);
    console.log(NgbSlideEventSource.ARROW_LEFT);
    console.log(NgbSlideEventSource.ARROW_RIGHT);
  }*/

  /* Map */
  // Google map lat-long
  //lat: number = 51.678418;
  //lng: number = 7.809007;

  // SITE SELECTOR - to move back to child component ----------------------------------------
  onTreeAction(event: any) {

    console.log(event);

    const index = event.rowIndex;
    const row = event.row;

    console.log('row.childrenPopulated: ' + row.childrenPopulated);

    // if currently collapsed, open & load children
    if (row.treeStatus === 'collapsed') {
      row.treeStatus = 'loading';

      // if children has not already been populated, fetch now
      if (!row.childrenPopulated) {
        this.lookupsService.getLookups('sites', row.id)
        .pipe(
          map(responseData => {

            console.log('GET CHILD SITES');
            console.log(responseData);

            const childNodes = [];

            if (responseData.hasOwnProperty('result')) {
              // convert js responsedata to array of typed model objects
              for (let i = 0; i < responseData['result'].length; i++) {
                const jsonObj = responseData['result'][i];
                const newChild = new SiteVO().deserialize(jsonObj);
                newChild.treeStatus = 'collapsed';
                childNodes.push(newChild);
              }
          }

            return childNodes;
          })
        )
        .subscribe((childNodes) => {
            // this.sitesTemp = [...allRecords]; // cache our list - used for filtering
            // this.rows = allRecords; // push our inital complete list
            // this.cd.markForCheck(); // add this to ensure new data is shown on screen

            row.treeStatus = 'expanded';
            row.childrenPopulated = true;
            this.siteRows = [...this.siteRows, ...childNodes];
            this.cd.detectChanges();
        });
      // if children has already been fetched, open node
      } else {
        row.treeStatus = 'expanded';
        this.siteRows = [...this.siteRows];
        this.cd.detectChanges();
      }

    // if currently open, collapse node
    } else {
      row.treeStatus = 'collapsed';
      this.siteRows = [...this.siteRows];
      this.cd.detectChanges();
    }
  }

  // On SELECT of tree
  onSelect({ selected }) {
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);

    // console.log(this.selected);

    // this.buttonDisabled = false;

    if (this.selected.length === 0) {
      // this.buttonDisabled = true;
    }
  }
  // END OF SITE SELECTOR ----------------------------------

}
