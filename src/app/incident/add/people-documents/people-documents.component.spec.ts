import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PeopleDocumentsComponent } from './people-documents.component';

describe('PeopleDocumentsComponent', () => {
  let component: PeopleDocumentsComponent;
  let fixture: ComponentFixture<PeopleDocumentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PeopleDocumentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PeopleDocumentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
