import { Component, OnInit } from '@angular/core';
import { NGXToastrService } from 'app/components/extra/toastr/toastr.service';

import { IncidentsService } from 'app/shared/services/incidents.service';

import { IncidentVO } from 'app/shared/models/incidentVO.model';
import { IncidentAssets } from 'app/shared/models/incidentAssets';
import { IncidentDocuments } from 'app/shared/models/incidentDocuments';

@Component({
  selector: 'app-people-documents',
  templateUrl: './people-documents.component.html',
  styleUrls: ['./people-documents.component.css'],
  providers: [ NGXToastrService ]
})
export class PeopleDocumentsComponent implements OnInit {

  incident: IncidentVO = new IncidentVO();
  incidentAssets: IncidentAssets = new IncidentAssets();
  incidentDocuments: IncidentDocuments = new IncidentDocuments();

  constructor(
    private incidentsService: IncidentsService,
    private toastrService: NGXToastrService
  ) { }

  ngOnInit(): void {
    this.getIncidentData();

    this.getIncidentAssets();
  }

  // get incident details
  getIncidentData(): void {
    this.incident.incidentId = 296020;  // 295763; //296020; //295763; //295815; //296104; // temp hardcoding of var

    this.incidentsService.getIncidentDetails('detail', this.incident.incidentId).subscribe((data) => {

      this.incident = data;

      this.getIncidentAssets();
      this.getIncidentDocuments();
    });
  }

  // incident assets
  getIncidentAssets(){
    this.incidentAssets = this.incident['assets']['source'];
    console.log(this.incidentAssets);
  }

  // incident documents
  getIncidentDocuments(){
    this.incidentDocuments = this.incident['documents'];
  }

  addIncidentDocument($event){
    console.log($event);

    this.toastrService.typeFileUploadSuccess();
  }

  addIncidentAsset($event){
    console.log('add people');

    var element = new IncidentAssets();

    element.name = "test";
    element.dateOfBirth = "11/10/2020";

    //this.incidentAssets.push(element);

  }

}
