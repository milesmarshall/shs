import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
// import { NgxDatatableModule } from '@swimlane/ngx-datatable';
// import { DatatableComponent, SelectionType } from '@swimlane/ngx-datatable';
import { ColumnMode } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-select-site',
  templateUrl: './select-site.component.html',
  styleUrls: ['./select-site.component.css']
  // providers: [NgxDatatableModule]  // add NgxDatatableModule to the component providers
})
export class SelectSiteComponent implements OnInit {

  incidentDate;
  rows = [];
  lastIndex = 15;

  ColumnMode = ColumnMode;

  sites = []; // temp - to be removed


  // TEMP TO BE REMOVED ----------------
   /* historyRows:any = [];
    historyData:any = [];
    historyTemp:any = [];
    historySelected: any[] = [];
    historyColumns = [
      { prop: 'status' },
      { prop: 'duration' },
      { prop: 'resource' }
    ];*/
// -----------------------------------------

  constructor(private cd: ChangeDetectorRef) {
    this.fetch(data => {
      data = data.slice(1, this.lastIndex);
      this.rows = data.map(d => {
        d.treeStatus = 'collapsed';
        d.parentId = null;
        return d;
      });
    });
  }

  ngOnInit(): void {
    this.incidentDate = new Date().toISOString().substring(0, 10);
    console.log('today: ' + this.incidentDate);

    this.rows = JSON.parse(sessionStorage.getItem('sites'));
    this.sites = JSON.parse(sessionStorage.getItem('sites')); // temp - to be removed

    console.log('SITES');
    console.log(this.sites);

    this.cd.markForCheck();
  }

  fetch(cb) {
    const req = new XMLHttpRequest();
    req.open('GET', `assets/data/100k.json`);

    req.onload = () => {
      setTimeout(() => {
        cb(JSON.parse(req.response));
      }, 500);
    };

    req.send();
  }

  onTreeAction(event: any) {
    const index = event.rowIndex;
    const row = event.row;
    if (row.treeStatus === 'collapsed') {
      row.treeStatus = 'loading';
      this.fetch(data => {
        data = data.slice(this.lastIndex, this.lastIndex + 3).map(d => {
          d.treeStatus = 'collapsed';
          d.parentId = row.id;
          return d;
        });
        this.lastIndex = this.lastIndex + 3;
        row.treeStatus = 'expanded';
        this.rows = [...this.rows, ...data];
        this.cd.detectChanges();
      });
    } else {
      row.treeStatus = 'collapsed';
      this.rows = [...this.rows];
      this.cd.detectChanges();
    }
  }

}
