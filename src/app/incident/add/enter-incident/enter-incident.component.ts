import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-enter-incident',
  templateUrl: './enter-incident.component.html',
  styleUrls: ['./enter-incident.component.css']
})
export class EnterIncidentComponent implements OnInit {

  constructor(
  ) { }

  rootCauses = [];
  impacts = [];
  statuses = [];
  resources = [];

  ngOnInit(): void {
    this.getStatuses();
    //this.getResources();
    this.getRootCauses();
    this.getImpacts();
  }

  getStatuses(){
    this.statuses = JSON.parse(sessionStorage.getItem('statuses'));
    this.statuses = this.statuses[0]['result'];
  }

  /*getResources(){
    this.resources = JSON.parse(sessionStorage.getItem('rgResources'));
    this.resources = this.resources[0]['result'];
  }*/

  getRootCauses() {
    this.rootCauses = JSON.parse(sessionStorage.getItem('rootCauses'));
    this.rootCauses = this.rootCauses[0]['result'];
  }

  getImpacts() {
    this.impacts = JSON.parse(sessionStorage.getItem('impacts'));
    this.impacts = this.impacts[0]['result'];
  }

}
