import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnterIncidentComponent } from './enter-incident.component';

describe('EnterIncidentComponent', () => {
  let component: EnterIncidentComponent;
  let fixture: ComponentFixture<EnterIncidentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnterIncidentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnterIncidentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
