import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncidentCalendarComponent } from './incident-calendar.component';

describe('IncidentCalendarComponent', () => {
  let component: IncidentCalendarComponent;
  let fixture: ComponentFixture<IncidentCalendarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncidentCalendarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncidentCalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
