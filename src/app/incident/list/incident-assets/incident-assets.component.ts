import { Component, OnInit, ViewChild} from '@angular/core';
import { DatatableComponent } from "@swimlane/ngx-datatable";

declare var require: any;
const data: any = require('app/shared/data/company.json');

@Component({
  selector: 'app-incident-assets',
  templateUrl: './incident-assets.component.html',
  styleUrls: ['./incident-assets.component.css']
})
export class IncidentAssetsComponent implements OnInit {
  @ViewChild(DatatableComponent) table: DatatableComponent;

  rows = [];

  temp = [];

  // Table Column Titles
  columns = [
        { prop: 'name' },
        { name: 'Company' },
        { name: 'Gender' }
  ];

  constructor() {
    this.temp = [...data];
    this.rows = data;
   }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp.filter(function (d) {
        return d.name.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.rows = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  isCollapsed = false;
  public show:boolean = false;

  ngOnInit(): void {
    console.log('assets onInit');
  }

}
