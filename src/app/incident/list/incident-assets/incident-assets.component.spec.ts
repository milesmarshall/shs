import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncidentAssetsComponent } from './incident-assets.component';

describe('IncidentAssetsComponent', () => {
  let component: IncidentAssetsComponent;
  let fixture: ComponentFixture<IncidentAssetsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncidentAssetsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncidentAssetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
