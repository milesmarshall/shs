import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncidentFloorPlanComponent } from './incident-floor-plan.component';

describe('IncidentFloorPlanComponent', () => {
  let component: IncidentFloorPlanComponent;
  let fixture: ComponentFixture<IncidentFloorPlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncidentFloorPlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncidentFloorPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
