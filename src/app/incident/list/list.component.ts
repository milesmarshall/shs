import { Component, OnInit, ViewChild, ViewEncapsulation, ChangeDetectionStrategy, TemplateRef, ElementRef, OnDestroy} from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { NgbModal, ModalDismissReasons, NgbActiveModal, NgbNavChangeEvent } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateStruct, NgbDatepickerI18n, NgbCalendar} from '@ng-bootstrap/ng-bootstrap';
import { NgbTooltip } from '@ng-bootstrap/ng-bootstrap';
import { NgbCarousel } from '@ng-bootstrap/ng-bootstrap';
import { NgbTabChangeEvent } from '@ng-bootstrap/ng-bootstrap';
import { startOfDay, endOfDay, subDays, addDays, endOfMonth, isSameDay, isSameMonth, addHours } from 'date-fns';
import { Subject, Subscription } from 'rxjs';
import { CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent } from 'angular-calendar';

import { GOOGLE_API } from 'app/app.constants';
import { SharedModule } from '../../shared/shared.module';
import { AssetsComponent } from 'app/shared/assets/assets.component';
import { ModalsService } from 'app/shared/services/modals.service';
import { LookupsService } from 'app/shared/services/lookups.service';
import { IncidentsService } from 'app/shared/services/incidents.service';

declare var require: any;
const data: any = require('app/shared/data/company.json');

/* calendar */
const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  }
}

/* date picker */
const now = new Date();
const I18N_VALUES = {
  en: {
    weekdays: ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'],
    months: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
  },
};

// Range datepicker Start
const equals = (one: NgbDateStruct, two: NgbDateStruct) =>
  one && two && two.year === one.year && two.month === one.month && two.day === one.day;

const before = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day
    ? false : one.day < two.day : one.month < two.month : one.year < two.year;

const after = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day
    ? false : one.day > two.day : one.month > two.month : one.year > two.year;
// Range datepicker Ends

/* end date picker */

export class NgbdModalContent {
  constructor(
    public activeModal: NgbActiveModal,
    private assetsComponent: AssetsComponent,
    private modalsService: ModalsService,
    private lookupsService: LookupsService,
    private incidentsService: IncidentsService
    ) { }
}

@Component({
  selector: 'app-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})

export class ListComponent implements OnInit, OnDestroy {
  @ViewChild('ngcarousel', { static: true }) ngCarousel: NgbCarousel;
  @ViewChild(DatatableComponent) table: DatatableComponent;
  @ViewChild('x') public tooltip: NgbTooltip;
  @ViewChild('modalContent') modalContent: TemplateRef<any>;
  @ViewChild('nav') nav; // reference to tabNavigator

  active = 1; // position of active tab in tabNavigator
  private showIncidentDetailSub: Subscription;

  // static data
  rootCauses = [];
  statuses = [];

  // data tables
  rows = [];
  temp = [];

  // Table Column Titles
  columns = [
        { prop: 'name' },
        { name: 'Company' },
        { name: 'Gender' }
  ];

  googleAPI: string = GOOGLE_API;

  /* Map */
  // Google map lat-long
  lat: number = 51.678418;
  lng: number = 7.809007;

  /* calendar */
  view: string = 'month';

  newEvent: CalendarEvent;

  viewDate: Date = new Date();

  modalData: {
    action: string;
    event: CalendarEvent;
  };

  actions: CalendarEventAction[] = [
    {
      label: '<i class="fa fa-fw fa-pencil"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.handleEvent('Edit this event', event);
      }
    },
    {
      label: '<i class="fa fa-fw fa-times"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.events = this.events.filter(iEvent => iEvent !== event);
        this.handleEvent('This event is deleted!', event);
      }
    }
  ];

  refresh: Subject<any> = new Subject();

  events: CalendarEvent[] = [
    {
      start: subDays(startOfDay(new Date()), 1),
      end: addDays(new Date(), 1),
      title: 'A 3 day event',
      color: colors.red,
      actions: this.actions
    },
    {
      start: startOfDay(new Date()),
      title: 'An event with no end date',
      color: colors.yellow,
      actions: this.actions
    },
    {
      start: subDays(endOfMonth(new Date()), 3),
      end: addDays(endOfMonth(new Date()), 3),
      title: 'A long event that spans 2 months',
      color: colors.blue
    },
    {
      start: addHours(startOfDay(new Date()), 2),
      end: new Date(),
      title: 'A draggable and resizable event',
      color: colors.yellow,
      actions: this.actions,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      draggable: true
    }
  ];

  activeDayIsOpen: boolean = true;

  constructor(
    private modalService: NgbModal,
    private sharedModule: SharedModule,
    private incidentsService: IncidentsService
    ) {
      this.temp = [...data];
      this.rows = data;
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp.filter(function (d) {
        return d.name.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.rows = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  isCollapsed = false;
  public show:boolean = false;

  /* date picker */
  // Variable declaration
  d: any;
  d2: any;
  d3: any;
  model: NgbDateStruct;
  popupModel;
  date: {year: number, month: number};
  displayMonths = 2;
  navigation = 'select';
  disabledModel: NgbDateStruct = {year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate()};
  disabled = true;
  customModel: NgbDateStruct;

  configModal;    // Global configuration of datepickers

  // Range datepicker start
  hoveredDate: NgbDateStruct;

  fromDate: NgbDateStruct;
  toDate: NgbDateStruct;

  ngOnInit(): void {
    console.log('[inside listComponent]');
    this.selectToday();

    // subscribe to subject that will emmit if user double clicks on incident list
    this.showIncidentDetailSub = this.incidentsService.showIncidentDetailEmitter.subscribe(responseData => {
      // open indicent detail tab
      this.setSelectedTab(2);
    });

    //this.ngCarousel.pause();
  }

  // Range datepicker starts
  onDateChange(date: NgbDateStruct) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && after(date, this.fromDate)) {
      this.toDate = date;
    } else {
      this.toDate = null;
      this.fromDate = date;
    }
  }

  isHovered = date => this.fromDate && !this.toDate && this.hoveredDate && after(date, this.fromDate) && before(date, this.hoveredDate);
  isInside = date => after(date, this.fromDate) && before(date, this.toDate);
  isFrom = date => equals(date, this.fromDate);
  isTo = date => equals(date, this.toDate);
  // Range datepicker ends

  // Selects today's date
  selectToday() {
    this.model = {year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate()};
  }

  // Custom Day View Starts
  isWeekend(date: NgbDateStruct) {
    const d = new Date(date.year, date.month - 1, date.day);
    return d.getDay() === 0 || d.getDay() === 6;
  }

  isDisabled(date: NgbDateStruct, current: {month: number}) {
    return date.month !== current.month;
  }
  // Custom Day View Ends
  /* end date picker */

  // Move to previous slide
  getToPrev() {
    this.ngCarousel.prev();
  }

  // Move to next slide
  goToNext() {
    this.ngCarousel.next();
  }

  // Pause slide
  stopCarousel() {
    this.ngCarousel.pause();
  }

  toggle() {
    this.show = !this.show;
  }

  /* Modal */
  closeResult: string;

  // This function is used in open
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  showModal( theModal ) {
    if (theModal = 'asset') {
      // this.assetModalComponent.openModal();
    }
  }

  // Open email modal
  openModal(content, size) {
    this.modalService.open(content, { size: size }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  /* calendar */
  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
        this.viewDate = date;
      }
    }
  }

  eventTimesChanged({
    event,
    newStart,
    newEnd
    }: CalendarEventTimesChangedEvent): void {
    event.start = newStart;
    event.end = newEnd;
    this.handleEvent('Dropped or resized', event);
    this.refresh.next();
  }

  handleEvent(action: string, event: CalendarEvent): void {
    this.modalData = { event, action };
    //.open(this.modalContent, { size: 'lg' });
  }

  addEvent(): void {
    this.newEvent = {
      title: 'New event',
      start: startOfDay(new Date()),
      end: endOfDay(new Date()),
      color: colors.red,
      draggable: true,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      actions: this.actions,
    }
    this.events.push(this.newEvent);

    // this.refresh.next();
    this.handleEvent('Add new event', this.newEvent);
    this.refresh.next();
  }

  /**
   * data & populating fields
   * **/

  // incident data
  /*public incident: Incident = new Incident();

  getIncidentDetails(){
    this.incident.incidentId = 295815; //296104; // temp hardcoding of var

    this.incidentsService.getIncidentDetails('000', 40, 'detail', this.incident.incidentId).subscribe((data) => {
      this.incident = data;
      console.log('INSIDE getIncidentDetails')
      console.log(this.incident);
      console.log('desc: ' + this.incident.description);

      this.getIncidentActions();
      this.getIncidentAssets();
      this.getIncidentDocuments();
    });
  }

  // incident actions
  public incidentActions = [];
  public incidentActionsLength = 0;

  getIncidentActions(){
    this.incidentActions = this.incident['actionsTaken']['source'];
    console.log(this.incidentActions);
    this.incidentActionsLength = this.incidentActions.length;
    console.log(this.incidentActionsLength);
  }

  // incident assets
  public incidentAssets = [];

  getIncidentAssets(){
    this.incidentAssets = this.incident['assets']['source'];
    console.log(this.incidentAssets);
  }

  // incident documents
  public incidentDocuments = [];

  getIncidentDocuments(){
    this.incidentDocuments = this.incident['docuemnts'];
    console.log(this.incidentDocuments);
  }

  // incident map
  public incidentMap= [];

  getIncidentMap(){
    this.incidentMap = this.incident['incidentGPS']['source'];
    console.log(this.incidentMap);
  }*/


  // Function gets called when tab is clicked
  onTabChange(changeEvent: NgbNavChangeEvent) {

    console.log('[inside listComponent] onTabChange');

    // example: prevent click of 3rd tab
    if (changeEvent.nextId === 3) {
      // changeEvent.preventDefault();
    }
  }

  // Set specific tab as selected
  setSelectedTab(tabPosition: number) {
    console.log('[inside listComponent] setSelectedTab');
    this.nav.select(tabPosition);
  }

  // unsubscribe from subject subscriptions to clean up
  ngOnDestroy(): void {
    this.showIncidentDetailSub.unsubscribe();
  }
}
