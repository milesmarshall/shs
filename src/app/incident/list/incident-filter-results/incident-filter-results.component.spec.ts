import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncidentFilterResultsComponent } from './incident-filter-results.component';

describe('IncidentFilterResultsComponent', () => {
  let component: IncidentFilterResultsComponent;
  let fixture: ComponentFixture<IncidentFilterResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncidentFilterResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncidentFilterResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
