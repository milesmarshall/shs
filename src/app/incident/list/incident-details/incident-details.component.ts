import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy, OnDestroy, ViewEncapsulation, Input } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder } from '@angular/forms';

import { LookupsService } from 'app/shared/services/lookups.service';
import { IncidentsService } from 'app/shared/services/incidents.service';

import { ListComponent } from '../list.component';

import { IncidentVO } from 'app/shared/models/incidentVO.model';
import { IncidentAssets } from 'app/shared/models/incidentAssets';
import { IncidentActions } from 'app/shared/models/incidentActions';
import { IncidentNotifications } from 'app/shared/models/incidentNotifications';
import { IncidentDocuments } from 'app/shared/models/incidentDocuments';
import { IncidentGPS } from 'app/shared/models/incidentGPS';
import { IncidentWork } from 'app/shared/models/incidentWork';
import { IncidentHistory } from 'app/shared/models/incidentHistory';

import { ColumnMode } from 'public-api';
import { Subscription } from 'rxjs';

declare var require: any;
const data: any = require('app/shared/data/company.json');

//@Directive()
/*export class NgbdModalContent {
  @Input() name;
  constructor(public activeModal: NgbActiveModal) { }
}*/

@Component({
  selector: 'app-incident-details',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './incident-details.component.html',
  styleUrls: ['./incident-details.component.css'],
  encapsulation: ViewEncapsulation.None,
})

export class IncidentDetailsComponent implements OnInit, OnDestroy {
  incident: IncidentVO = new IncidentVO();
  incidentAsset: IncidentAssets = new IncidentAssets();
  incidentAssets: IncidentAssets = new IncidentAssets();
  incidentActions: IncidentActions = new IncidentActions();
  incidentNotifications: IncidentNotifications = new IncidentNotifications();
  incidentDocuments: IncidentDocuments = new IncidentDocuments();
  incidentGPS: IncidentGPS = new IncidentGPS();
  incidentWork: IncidentWork = new IncidentWork();
  incidentHistory: IncidentHistory = new IncidentHistory();

  // lookup data
  luRootCauses = [];
  luStatuses = [];
  luResources = [];

  private showIncidentDetailSub: Subscription;

  customField281 = [];
  customField282 = [];

  incidentActionsLength = 0;

  constructor(
    private lookupsService: LookupsService,
    private listComponent: ListComponent,
    private incidentsService: IncidentsService,
    private cd: ChangeDetectorRef,
    private modalService: NgbModal,
    private fb: FormBuilder,
  ) {
    this.fetch(data => {
      data = data.slice(1, this.lastIndex);
      this.rows_test = data.map(d => {
        d.treeStatus = 'collapsed';
        d.parentId = null;
        return d;
      });
    });
  }

  ngOnInit(): void {
    console.log('[incident-details] ngOnInit');

    // subscribe to subject that will emmit if user double clicks on incident list
    this.showIncidentDetailSub = this.incidentsService.showIncidentDetailEmitter.subscribe(receivedIncidentId => {
      this.incident = new IncidentVO();
      this.incident.incidentId = receivedIncidentId;

      console.log('[incident-details] get data for incidentId: ' + this.incident.incidentId);

      this.incidentsService.getIncidentDetails('detail', this.incident.incidentId).subscribe((responseData) => {
        this.incident = new IncidentVO().deserialize(responseData);
        this.cd.markForCheck(); // add this to ensure new data is shown on screen

        this.getIncidentActions();
        this.getIncidentNotifications();
        this.getIncidentAssets();
        this.getIncidentDocuments();
        this.getIncidentGPS();
        this.getIncidentWork();
        this.getIncidentHistory();

        //console.log(this.incident);

        this.incident.incidentActionsCount = this.incident.actionsTaken['source'].length;
        this.incident.incidentNotifyCount = this.incident.escalation['source'].length;

        let incidentAssetsCount = 0;
        for (let i = 0; i < this.incident.assets['source'].length; i++) {
          if (Number(this.incident.assets['source'][i].assetTypeId) === 1)
            incidentAssetsCount = incidentAssetsCount + 1;
        }
        this.incident.incidentAssetsCount = incidentAssetsCount;

        this.incident.incidentDocumentsCount = this.incident.documents.length;
        this.incident.incidentWorkCount = this.incident.actionsTaken['source'].length;

        let incidentPeopleCount = 0;
        for (let i = 0; i < this.incident.assets['source'].length; i++) {
          if (Number(this.incident.assets['source'][i].assetTypeId) === 2)
            incidentPeopleCount = incidentPeopleCount + 1;
        }
        this.incident.incidentPeopleCount = incidentPeopleCount;

        // get lookup data for dropdowns from session storage - is this the best place to fetch it? Every time an incident is opened, refetching... ?
        // MM -> MJ why not just get the data from the seesion object? shouldnt have to requery all the time
        // also, should this just be referenced on incidents object itself
        this.luRootCauses = this.lookupsService.getRootCauses();
        this.luStatuses = this.lookupsService.getStatuses();
        this.luResources = this.lookupsService.getResources();
      });
    });
  }

  selectStatus(val: any) {
    console.log(val);
  }

  selectRootCause(val: any) {
    console.log('[incident-details] selectRootCause ');
    console.log(val);
  }

  getCustomFields() {
    /* just hard-coded to test, this will have to come from an array or object of items */
    this.customField281 = this.lookupsService.getCustomFieldFromId(281);
    this.customField281['listValues'] = this.customField281['listValues'].split(',');

    this.customField282 = this.lookupsService.getCustomFieldFromId(282);
    this.customField282['listValues'] = this.customField282['listValues'].split(',');
  }

  // incident actions
  actionsRows: any = [];
  actionsData: any = [];
  actionsTemp: any = [];

  actionsSelected: any[] = [];

  // DataTable Content Titles
  actionsColumns = [
    { prop: 'Date Time' },
    { prop: 'Action' },
    { prop: 'Assigned To' },
    { prop: 'Public Notice' },
  ];

  getIncidentActions() {
    this.incidentActions = this.incident['actionsTaken']['source'];

    this.actionsRows = this.incidentActions;

    for (let i = 0; i < this.actionsRows.length; i++) {
      if (this.incidentActions[i].actionType == 0) { // only showing type=0, since type=1 is system admin
        this.actionsData.push({
          'Date Time': this.incidentActions[i].dateTimeStamp,
          'Action': this.incidentActions[i].actionType,
          'Assigned To': this.lookupsService.getResourceFromId(this.incidentActions[i].loggedUserId, 'name'),
          'Public Notice': this.incidentActions[i].description
        });
      }
    }

    this.actionsTemp = [...this.actionsData];
    this.actionsRows = this.actionsData;
  }

  // ations table select
  onActionsTableSelect(event) {
    console.log('onActionsTableSelect');
  }

  // actions table activate
  onActionsTableActivate(event) {

  }

  // incident notifications
  notifyRows: any = [];
  notifyData: any = [];
  notifyTemp: any = [];

  notifySelected: any[] = [];

  // incident columns
  notifyColumns = [
    { prop: 'Date Time' },
    { prop: 'Action' },
    { prop: 'Notified Resource' },
    { prop: 'Method' },
    { prop: 'Type' }
  ];

  // incident table select
  onNotifyTableSelect(event) {
    console.log('onNotifyTableSelect');
  }

  // incident table activate
  onNotifyTableActivate(event) {

  }

  // test
  rows_test = [];
  lastIndex = 15;

  ColumnMode = ColumnMode;

  fetch(cb) {
    const req = new XMLHttpRequest();
    req.open('GET', `http://localhost:4200/assets/data/100k.json`);

    req.onload = () => {
      setTimeout(() => {
        cb(JSON.parse(req.response));
      }, 500);
    };

    req.send();
  }

  onTreeAction(event: any) {
    const index = event.rowIndex;
    const row = event.row;
    if (row.treeStatus === 'collapsed') {
      row.treeStatus = 'loading';
      this.fetch(data => {
        data = data.slice(this.lastIndex, this.lastIndex + 3).map(d => {
          d.treeStatus = 'collapsed';
          d.parentId = row.id;
          return d;
        });
        this.lastIndex = this.lastIndex + 3;
        row.treeStatus = 'expanded';
        this.rows_test = [...this.rows_test, ...data];
        this.cd.detectChanges();
      });
    } else {
      row.treeStatus = 'collapsed';
      this.rows_test = [...this.rows_test];
      this.cd.detectChanges();
    }
  }
  // end test

  getIncidentNotifications() {
    this.incidentNotifications = this.incident['escalation']['source'];

    this.notifyRows = this.incidentNotifications;

    for (let i = 0; i < this.notifyRows.length; i++) {
      this.notifyData.push({
        'Date Time': this.incidentNotifications[i].timestamp,
        'Action': this.incidentNotifications[i].description,
        'Notified Resource': this.lookupsService.getResourceFromId(this.incidentNotifications[i].resourceId, 'name'),
        'Method': (this.incidentNotifications[i].email === 1) ? 'Email' : 'Email/SMS',
        'Type': '???'
      });
    }

    this.notifyTemp = [...this.notifyData];
    this.notifyRows = this.notifyData;
  }

  // incident assets
  getIncidentAssets() {
    this.incidentAssets = this.incident['assets']['source'];
  }

  // incident documents
  getIncidentDocuments() {
    this.incidentDocuments = this.incident['documents'];
  }

  // incident map
  getIncidentGPS() {
    this.incidentGPS = this.incident['incidentGPS']['source'][0];
  }

  // incident work
  workRows: any = [];
  workData: any = [];
  workTemp: any = [];

  workSelected: any[] = [];

  // DataTable Content Titles
  workColumns = [
    { prop: 'Date Time' },
    { prop: 'Action' },
    { prop: 'Assigned To' },
    { prop: 'Public Notice' },
  ];

  getIncidentWork() {
    this.incidentActions = this.incident['actionsTaken']['source'];

    this.workRows = this.incidentActions;

    for (let i = 0; i < this.workRows.length; i++) {
      if (this.incidentActions[i].actionType == 1) { // only showing type=1, since type=1 is system admin
        this.workData.push({
          'Date Time': this.incidentActions[i].dateTimeStamp,
          'Action': this.incidentActions[i].actionType,
          'Assigned To': this.lookupsService.getResourceFromId(this.incidentActions[i].loggedUserId, 'name'),
          'Public Notice': this.incidentActions[i].description
        });
      }
    }

    this.workTemp = [...this.workData];
    this.workRows = this.workData;
  }

  // work table select
  onWorkTableSelect(event) {
    console.log('onWorkTableSelect');
  }

  // work table activate
  onWorkTableActivate(event) {

  }

  // incident history
  historyRows: any = [];
  historyData: any = [];
  historyTemp: any = [];

  historySelected: any[] = [];

  // DataTable Content Titles
  historyColumns = [
    { prop: 'status' },
    { prop: 'duration' },
    { prop: 'resource' }
  ];

  getIncidentHistory() {
    this.incidentHistory = this.incident['statusHistory']['source'];

    this.historyRows = this.incidentHistory;

    for (let i = 0; i < this.historyRows.length; i++) {
      this.historyData.push({
        'status': this.lookupsService.getStatusFromId(this.incidentHistory[i].statusId, 'description'),
        'duration': this.incidentHistory[i].dateTimeStamp,
        'resource': this.lookupsService.getResourceFromId(this.incidentHistory[i].loggedResourceId, 'name')
      });
    }

    this.historyTemp = [...this.historyData];
    this.historyRows = this.historyData;
  }

  // history table select
  onHistoryTableSelect(event) {
    console.log('onHistoryTableSelect');
  }

  // history table activate
  onHistoryTableActivate(event) {

  }

  ngOnDestroy(): void {
    // if component gets destroyed, clean up subscriptions
    this.showIncidentDetailSub.unsubscribe();
  }

  /* modal */
  closeResult: string;

  // Open default modal
  open(content) {
    //const modalRef = this.modalService.open(content, { size: 'xl' });
    //modalRef.componentInstance.my_modal_title = 'title test';
    //modalRef.componentInstance.my_modal_content = 'content test';

    this.modalService.open(content, { size: 'xl' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  // This function is used in open
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  openModalTest(targetModal, assetId) {
    this.modalService.open(targetModal, {
      centered: true,
      backdrop: 'static',
      size: 'xl',
    });

    // match assetId to record
    this.incidentAsset = this.lookupsService.getAssetFromId(assetId, '');

    this.cd.markForCheck(); // add this to ensure new data is shown on screen
  }

}
