import { Component, OnInit, ViewChild, ViewEncapsulation, ChangeDetectionStrategy, TemplateRef} from '@angular/core';
import { DatatableComponent } from "@swimlane/ngx-datatable";
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateStruct, NgbDatepickerI18n, NgbCalendar} from '@ng-bootstrap/ng-bootstrap';
import { NgbTooltip } from '@ng-bootstrap/ng-bootstrap';
import { NgbCarousel } from '@ng-bootstrap/ng-bootstrap';

import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours
} from 'date-fns';
import { Subject } from 'rxjs';
import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent
} from 'angular-calendar';

import { GOOGLE_API } from 'app/app.constants';

declare var require: any;
const data: any = require('../../../shared/data/company.json');

@Component({
  selector: 'app-incident-map',
  templateUrl: './incident-map.component.html',
  styleUrls: ['./incident-map.component.css']
})


export class IncidentMapComponent implements OnInit {

  constructor() { }

  googleAPI: string = GOOGLE_API;

  /* Map */
  // Google map lat-long
  lat: number = 51.678418;
  lng: number = 7.809007;

  refresh: Subject<any> = new Subject();

  public show:boolean = false;

  ngOnInit(): void {
  }

  toggle() {
    this.show = !this.show;
  }

}
