import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncidentAuditLogComponent } from './incident-audit-log.component';

describe('IncidentAuditLogComponent', () => {
  let component: IncidentAuditLogComponent;
  let fixture: ComponentFixture<IncidentAuditLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncidentAuditLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncidentAuditLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
