import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncidentPublicCommsComponent } from './incident-public-comms.component';

describe('IncidentPublicCommsComponent', () => {
  let component: IncidentPublicCommsComponent;
  let fixture: ComponentFixture<IncidentPublicCommsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncidentPublicCommsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncidentPublicCommsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
