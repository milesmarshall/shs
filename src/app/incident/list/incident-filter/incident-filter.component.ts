import { Component, OnInit } from '@angular/core';

import { ListComponent } from '../list.component';

@Component({
  selector: 'app-incident-filter',
  templateUrl: './incident-filter.component.html',
  styleUrls: ['./incident-filter.component.css']
})
export class IncidentFilterComponent implements OnInit {

  constructor(
    private listComponent:ListComponent
  ) { }

  ngOnInit(): void {
  }

}