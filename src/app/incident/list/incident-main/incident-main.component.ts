import { Component, OnInit, ViewChild, ChangeDetectorRef, ElementRef } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DatatableComponent, SelectionType } from '@swimlane/ngx-datatable';
import { Observable, BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';

import { IncidentsService } from 'app/shared/services/incidents.service';
import { LookupsService } from 'app/shared/services/lookups.service';

import { INCIDENTS } from 'app/app.constants';
import { map } from 'rxjs/operators';
//import { constants } from 'buffer';
import { formatDate } from '@angular/common';
import { environment } from 'environments/environment';

import { IncidentVO } from 'app/shared/models/incidentVO.model';
import { Page } from 'app/shared/models/page.model';
import { IncidentAssets } from 'app/shared/models/incidentAssets';
import { ResourceVO } from 'app/shared/models/resourceVO.model';

declare var require: any;
// const test_data: any = require('../../../shared/data/company.json');

@Component({
  selector: 'app-incident-main',
  templateUrl: './incident-main.component.html',
  styleUrls: ['./incident-main.component.css'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class IncidentMainComponent implements OnInit {
  @ViewChild(DatatableComponent) table: DatatableComponent;
  @ViewChild('checkInclCancelled', { static: true }) checkInclCancelled: ElementRef;
  @ViewChild('checkInclClosed', { static: true }) checkInclClosed: ElementRef;

  dateStart = '';
  dateEnd = '';

  incident: IncidentVO = new IncidentVO();
  incidents: any = INCIDENTS;
  incidentAssets: IncidentAssets = new IncidentAssets();

  isCollapsed = false;
  public show = false;
  showDateSelector = false;
  loading = false;
  buttonDisabled = true;
  isFetching = false;

  // Incident table
  rows:any[] = new Array<IncidentVO>();
  data:any[] = [];
  temp:any[] = []; // used by free text filter
  selected:any[] = [];
  SelectionType = SelectionType;
  loadingIndicator = false;
  lastClick = 0; // used to determine dblclick on table
  page = new Page(); // required for paging

  /* filter */
  sites:any[] = [];
  categories:any[] = [];
  statuses:any[] = [];
  resources:any[] = [];
  documents:any[] = [];
  rootCauses:any[] = [];
  impacts:any[] = [];
  reports:any[] = [];
  escalation:any[] = [];

  public errorMsg;

  constructor(
    private incidentsService: IncidentsService,
    private lookupsService: LookupsService,
    private cd: ChangeDetectorRef,
    private modalService: NgbModal,
    private http: HttpClient,
  ) {
    this.page.pageNumber = 0;
    this.page.size = 50;
  }

  toggle() {
    this.show = !this.show;
  }

  ngOnInit(): void {
    this.loading = true;

    console.log('[incidentMain ngOnInit]');

    this.getIncidentsData();

    /* get lookup data for filters */
    this.getSites();
    this.getCategories();
    this.getStatuses();
    this.getResources();
    this.getRootCauses();
    this.getStatuses();
    this.getImpacts();
    this.getReports();
  }

  // Get incidents data for table - function called onInit & also by table paging(only table paging passes event with page info)
  getIncidentsData(event:any = null) {
    console.log('[inside getIncidentsData]');

    // event is specified(when called by table paging), get page number from event data, else default to 0
    this.page.pageNumber = (event) ? event.offset : 0; // event.offset;
    this.isFetching = true;
    this.loadingIndicator = true; // displays table loading indicator

    // get checkbox selections
    const inclClosed = <HTMLInputElement> document.getElementById('chkInclClosed');
    const inclCancelled = <HTMLInputElement> document.getElementById('chkInclCancelled');

    // MJA TO DO - when paging, clear checkbox selection
    if (event) {}

    // getIncidents(securityGroupId, isLite, getCount, pageToGet)
    // MJA TO DO - replace hardcoded securitygroupID
    const pageToGet:number = this.page.pageNumber + 1;

    this.incidentsService.getIncidents(true, 1, pageToGet, inclClosed.checked, inclCancelled.checked, this.dateStart, this.dateEnd)
      .pipe(
        map(responseData => {

          // .map((res: Response) => res.json().response.map((user: User) => new User().deserialize(user)));
          // convert js responsedata obj to an array
          const incidentsData:any[] = [];

          if (responseData.hasOwnProperty('result')) {
            for (let i = 0; i < responseData['result'].length; i++) {
              // convert json to VO obj and add to array
              const jsonObj = responseData['result'][i];
              const newIncident = new IncidentVO().deserialize(jsonObj);
              incidentsData.push(newIncident); // MJ : why this not using 'incidents' to get wider component scope ?
              this.incidents.push(newIncident); // MJ : shouldnt need this
            }
          }

          // set pagination vars
          this.page.totalElements = responseData['count'];
          this.page.totalPages = this.page.totalElements / this.page.size;

          console.log('incidentsData: ');
          console.log(incidentsData);

          return incidentsData;
        })
      )
      .subscribe((incidentsData) => {
          this.isFetching = false;
          this.loadingIndicator = false;
          this.temp = [...incidentsData]; // cache our list - used for filtering
          this.rows = incidentsData; // push our inital complete list
          this.cd.markForCheck(); // add this to ensure new data is shown on screen

          this.loading = false;
      });
  }

  // If 'Incl Closed' or 'Incl Cancelled' has been checked, show the date selector
  toggleShowDateSelector() {
    if (this.checkInclCancelled.nativeElement.checked || this.checkInclClosed.nativeElement.checked) {
      this.showDateSelector = true;
    } else {
      this.showDateSelector = false;
      this.dateStart = '';
      this.dateEnd = '';
      this.getIncidentsData();
    }
  }

  // date range is required if 'incl closed' or 'incl cancelled' was checked
  setDateSelection(event:any = null) {
    if (event.target.id === 'dateStart') {
      this.dateStart = event.target.value;
    } else if (event.target.id === 'dateEnd') {
      this.dateEnd = event.target.value;
    }
  }

  getSites() {
    this.sites = JSON.parse(sessionStorage.getItem('sites') || '[]');
  }

  getCategories() {
    //this.categories = JSON.parse(sessionStorage.getItem('categories'));
    //this.categories = this.categories[0]['result'];
  }

  getStatuses() {
    this.statuses = JSON.parse(sessionStorage.getItem('statuses') || '[]');
  }

  getResources() {
    this.resources = JSON.parse(sessionStorage.getItem('rgResources') || '[]');
  }

  getRootCauses() {
    this.rootCauses = this.lookupsService.getRootCauses();
  }

  getImpacts() {
    this.impacts = JSON.parse(sessionStorage.getItem('impacts') || '[]');
    this.impacts = this.impacts[0]['result'];
  }

  getReports() {
    this.reports = JSON.parse(sessionStorage.getItem('reports') || '[]');
    this.reports = this.reports[0]['result'];
  }

  // Incident list filter - used by free text search & checkbox filters
  filterIncidents(event:any = null) {
    let freeText = '';
    let isMatchAssignedTo = false;

    // function is called by freetext search which passes event, or checkbox filters which doesn't
    if (event) {
      freeText = event.target.value.toLowerCase();
    } else {
      const txtFreeText = <HTMLInputElement> document.getElementById('textFilter');
      freeText = txtFreeText.value;
    }

    const storedUserData = JSON.parse(localStorage.getItem('userData') || '[]');
    const loggedByMe = <HTMLInputElement> document.getElementById('chkLoggedByMe');
    const assignedToMe = <HTMLInputElement> document.getElementById('chkAssignedToMe');

    // apply free text & checkboxes filter criteria to each incident in list
    const temp = this.temp.filter(function (rowData) {

        // MJA TO DO: Flex seach also searches over custom fields, impact & rootcause
        // MJA TO DO: apply resource restrictions - see utils.applyIncidentSearchFilterCriteria

        // if ASSIGNED TO checked, validate action resources
        if (assignedToMe.checked && rowData.assignedResources.source.length > 0) {

          const data = rowData.assignedResources.source;
          isMatchAssignedTo = false;

          for (let i = 0; i < data.length; i++) {
            if (Number(data[i].resourceId) === Number(storedUserData.id)) {
                isMatchAssignedTo = true;
                break;
            }
          }
        }

        return (rowData.description.toLowerCase().indexOf(freeText) !== -1
            || rowData.incidentId.toString().toLowerCase().indexOf(freeText) !== -1
            || (rowData.scheduleInstanceId != null && rowData.scheduleInstanceId.toString().toLowerCase().indexOf(freeText) !== -1)
            || rowData.description.toLowerCase().indexOf(freeText) !== -1
            || rowData.siteName.toLowerCase().indexOf(freeText) !== -1
            || rowData.categoryName.toLowerCase().indexOf(freeText) !== -1
            // || rowData.status.toLowerCase().indexOf(val) !== -1  ---------------- MJA. TO DO
            // || rowData.reportedBy.toLowerCase().indexOf(val) !== -1 ------------- MJA. TO DO
            || !freeText)
            // apply LOGGED BY ME filter
            && (!loggedByMe.checked || loggedByMe.checked && +rowData.loggedResourceId === +storedUserData.id)
            // apply ASSIGNED TO ME filter
            && (!assignedToMe.checked || isMatchAssignedTo)
            ;
    });

    // update the rows
    this.rows = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  // Add coloured status indicator to table column
  styleStatusIndicator(statusId: number): any {
    for (let i = 0; i < this.statuses.length; i++) {
      if (Number(this.statuses[i].id) === Number(statusId)) {
          return {color: '#' + this.statuses[i].indicatorColor, opacity: 1}
      }
    }
}

  // Get status indicator color - used in incident list
  getStatusIndicatorColor(statusId: number) {
    for (let i = 0; i < this.statuses.length; i++) {
      if (Number(this.statuses[i].id) === Number(statusId)) {
          return '#' + this.statuses[i].indicatorColor;
      }
    }
  }

  // On CHECK of checkbox
  onSelect({ selected }) {
    // console.log('Select Event', selected, this.selected);

    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);

    console.log('selected');
    console.log(this.selected);

    this.buttonDisabled = false;

    if (this.selected.length === 0) {
      this.buttonDisabled = true;
    }
  }

  // TEMP debug functions - logs events that get fired when interacting with table
  // checkbox documentation: https://github.com/swimlane/ngx-datatable/blob/master/src/app/selection/selection-chkbox.component.ts
  onActivate(event:any = null) {
    // console.log('Activate Event', event);

    // Catch double click
    if (event.type === 'click') {
        if (this.lastClick > 0 && Date.now() - this.lastClick <= 500) {
            console.log('open incident: ' + event.row.incidentId);
             // double click - emit event that listComponent is listening to to open the IncidentDetail tab
            this.incidentsService.showIncidentDetailEmitter.next(event.row.incidentId);
        // single click
        } else {
            this.lastClick = Date.now();
        }
    } else {
        this.lastClick = 0;
    }
  }

  displayCheck(row) {
    return row.incidentId;
  }

  // view/print modal
  openModalViewPrint(targetModal) {
    console.log('openModalViewPrint');

    // flatten and get each incidentId from selectted
    let incidentIds = this.selected.map(x => x.incidentId).join(",");

    this.incidentsService.getIncidentDetails('detail', incidentIds, 'modal').subscribe((responseData) => {
      //this.incident = new IncidentVO().deserialize(responseData);
      this.incidents = (responseData);
      console.log(this.incidents);

      // MJ : note this is just storage to the lookup data; risk each selected incident will have same objects
      this.incident.resources.push(this.resources);
      this.incident.documents.push(this.documents);
      //this.incident.escalation.push(this.escalation);
      //this.incident.assets.push(this.incident['assets']['source']);

      //console.log(this.incident);

      //this.incident.selected = this.selected;

      this.cd.markForCheck(); // add this to ensure new data is shown on screen

      this.modalService.open(targetModal, {
        centered: true,
        backdrop: 'static',
        size: 'xl',
       });
    });
  } // end view/print modal

  // bulk edit modal
  openModalBulk(targetModal) {
    this.modalService.open(targetModal, {
      centered: true,
      backdrop: 'static',
      size: 'l',
    });

    this.incident.selected = this.selected;

    this.incident.statuses.push(JSON.parse(sessionStorage.getItem('statuses')));

    this.cd.markForCheck(); // add this to ensure new data is shown on screen
  } // end bulk edit modal

  // email modal
  openModalEmail(targetModal) {
    console.log('openModalEmail');

    this.incidentsService.getIncidentDetails('detail', this.selected[0].incidentId, 'modal').subscribe((responseData) => {
      this.incident = new IncidentVO().deserialize(responseData);

      this.incident.resources.push(this.resources);

      this.cd.markForCheck(); // add this to ensure new data is shown on screen
    });

    this.modalService.open(targetModal, {
      centered: true,
      backdrop: 'static',
      size: 'l',
    });
  } // end bulk edit modal

  print(event:any = null) {
    let printContents = document.getElementById('incident-print-section').innerHTML;
    let originalContents = document.body.innerHTML;

    document.body.innerHTML = printContents;

    window.print();

    document.body.innerHTML = originalContents;
  }

  email(event:any = null) {

    let printStr = '';

    console.log(this.incidents);

    for (let index = 0; index < this.incidents.length; index++) {
      /*printStr += '<tr><td width="150px"><strong>INCIDENT DESK PROFILE: </strong></td><td>???DEVTEAMTEST???</td></tr>';
      printStr += '<tr><td colspan="2">&nbsp;</td></tr>';
      printStr += '<tr><td width="150px"><strong>INCIDENT DATA</strong></td><td>&nbsp;</td></tr>';
      printStr += '<tr><td width="150px"><strong>INCIDENT ID: strong></td><td>'+this.incidents[index].incidentId+'</td></tr>';
      printStr += '<tr><td width="150px"><strong>DATE/TIME: strong></td><td>'+this.incidents[index].incidentDateTime+'</td></tr>';
      printStr += '<tr><td width="150px"><strong>TYPE: strong></td><td>'+this.incidents[index].incidentType+'</td></tr>';
      printStr += '<tr><td width="150px"><strong>REPORTED BY: strong></td><td>'+this.incidents[index].reportedResourceId+'</td></tr>';
      printStr += '<tr><td width="150px"><strong>LOGGED BY: strong></td><td>'+this.incidents[index].loggedResourceId+'</td></tr>';
      printStr += '<tr><td width="150px"><strong>STATUS: strong></td><td>'+this.incidents[index].statsuId+'</td></tr>';
      printStr += '<tr><td width="150px"><strong>SITE: strong></td><td>'+this.incidents[index].siteName+'</td></tr>';
      printStr += '<tr><td width="150px"><strong>CATEGORY: strong></td><td>'+this.incidents[index].categoryName+'</td></tr>';
      printStr += '<tr><td width="150px"><strong>ROOT CAUSE: strong></td><td>'+this.incidents[index].rootCauseId+'</td></tr>';
      printStr += '<tr><td width="150px"><strong>IMPACT: strong></td><td>'+this.incidents[index].impact+'</td></tr>';
      printStr += '<tr><td width="150px"><strong>DETAILS: strong></td><td>'+this.incidents[index].description+'</td></tr>';
*/
      /*printStr += '<tr><td colspan="2">&nbsp;</td></tr>';
      printStr += '<tr><td width="150px"><strong>ADDITIONAL INFORMATION</strong></td><td>&nbsp;</td></tr>';

      for (let i = 0; i < this.incidents[index].customFieldValues.source.length; i++) {
        printStr += '<tr><td width="150px"><strong>'+this.incidents[index].customFieldValues.source[i].fieldId+': strong></td><td>'+this.incidents[index].customFieldValues.source[i].labelText+'</td></tr>';
      }*/

      /*printStr += '<tr><td colspan="2">&nbsp;</td></tr>';
      printStr += '<tr><td width="150px"><strong>STATUS HISTORY</strong></td><td>&nbsp;</td></tr>';

      for (let i = 0; i < this.incidents[index].statusHistory.source.length; i++) {
        printStr += '<tr><td width="150px">'+this.incidents[index].statusHistory.source[i].statusId+'</td><td>'+this.incidents[index].statusHistory.source[i].dateTimeStamp+' : '+this.incidents[index].statusHistory.source[i].loggedResourceId+'</td></tr>';
      }*/

      printStr += '<tr><td colspan="2">&nbsp;</td></tr>';
      printStr += '<tr><td width="150px"><strong>FIXED ASSETS</strong></td><td>&nbsp;</td></tr>';

      for (let i = 0; i < this.incidents[index].assets.source.length; i++) {
        if (this.incidents[index].assets.source[i].assetTypeId == 1)
          printStr += '<tr><td width="150px">'+this.incidents[index].assets.source[i].id+'</td><td>'+this.incidents[index].assets.source[i].name+' : '+this.incidents[index].assets.source[i].make+' : '+this.incidents[index].assets.source[i].model+' : '+this.incidents[index].assets.source[i].serialNumber+'</td></tr>';
      }

      printStr += '<tr><td colspan="2">&nbsp;</td></tr>';
      printStr += '<tr><td width="150px"><strong>HUMAN ASSETS</strong></td><td>&nbsp;</td></tr>';

      for (let i = 0; i < this.incidents[index].assets.source.length; i++) {
        if (this.incidents[index].assets.source[i].assetTypeId == 2)
          printStr += '<tr><td width="150px">'+this.incidents[index].assets.source[i].assetId+'</td><td>'+this.incidents[index].assets.source[i].name+' : '+this.incidents[index].assets.source[i].lastName+' : '+this.incidents[index].assets.source[i].serialNumber+'</td></tr>';
      }

      printStr += '<tr><td colspan="2">&nbsp;</td></tr>';
      printStr += '<tr><td width="150px"><strong>ACTIONS</strong></td><td>&nbsp;</td></tr>';

      for (let i = 0; i < this.incidents[index].actionsTaken.source.length; i++) {
          printStr += '<tr><td width="150px">'+this.incidents[index].actionsTaken.source[i].id+'</td><td>'+this.incidents[index].actionsTaken.source[i].dateTimeStamp+' : '+this.incidents[index].actionsTaken.source[i].description+' : '+this.incidents[index].actionsTaken.source[i].assignedResources+'</td></tr>';
      }

      printStr += '<tr><td colspan="2">&nbsp;</td></tr>';
      printStr += '<tr><td width="150px"><strong>NOTIFICATIONS</strong></td><td>&nbsp;</td></tr>';

      for (let i = 0; i < this.incidents[index].escalation.source.length; i++) {
        printStr += '<tr><td width="150px">'+this.incidents[index].escalation.source[i].id+'</td><td>'+this.incidents[index].escalation.source[i].description+'</td></tr>';
      }

      console.log(printStr);

      let emailAttachmentType = 1;
      let emailSubject = 'Incident: ' + this.incidents[index].incidentId;
      let action = 'emailIncident';
      let securityGroupId = '40';
      let sessionToken = '000';

      //this.http.post(`${environment.apiUrl}/sendNotification`, { emailAttachmentType: emailAttachmentType, emailSubject: emailSubject, action: action }).subscribe(
        this.http.post(`${environment.apiUrl}/sendNotification`, { emailAttachmentType: 1, emailSubject: 'Incident: ' + this.incidents[index].incidentId, action: 'emailIncident' }).subscribe(
        (response) => console.log(response),
        (error) => console.log(error)
      );

      /*this.http.post<any>(`${environment.apiUrl}/sendNotification`, { emailAttachmentType: emailAttachmentType, emailSubject: emailSubject, action: action })
        .pipe(tap(responseData => {
          console.log(responseData);
      })
    );*/
    }

  }

}
