import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncidentMainComponent } from './incident-main.component';

describe('IncidentMainComponent', () => {
  let component: IncidentMainComponent;
  let fixture: ComponentFixture<IncidentMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncidentMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncidentMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
