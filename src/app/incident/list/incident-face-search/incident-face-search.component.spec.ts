import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncidentFaceSearchComponent } from './incident-face-search.component';

describe('IncidentFaceSearchComponent', () => {
  let component: IncidentFaceSearchComponent;
  let fixture: ComponentFixture<IncidentFaceSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncidentFaceSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncidentFaceSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
