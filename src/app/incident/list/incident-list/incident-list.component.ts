import { Component, OnInit, ViewChild, TemplateRef} from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

declare var require: any;

export class NgbdModalContent {
  constructor(
    public activeModal: NgbActiveModal
    ) { }
}

@Component({
  selector: 'app-incident-list',
  templateUrl: './incident-list.component.html',
  styleUrls: ['./incident-list.component.css']
})
export class IncidentListComponent implements OnInit {
  @ViewChild('modalContent') modalContent: TemplateRef<any>;

  constructor(
    private modalService: NgbModal
  ) { }

  /* Modal */
  closeResult: string;

  // This function is used in open
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  ngOnInit(): void {
  }

  showModal( theModal ) {
    if (theModal = 'asset') {
      // this.assetModalComponent.openModal();
    }
  }

  // Open  modal
  openModal(content, size) {
    this.modalService.open(content, { size: size }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

}
