import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncidentResearchTrackerComponent } from './incident-research-tracker.component';

describe('IncidentResearchTrackerComponent', () => {
  let component: IncidentResearchTrackerComponent;
  let fixture: ComponentFixture<IncidentResearchTrackerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncidentResearchTrackerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncidentResearchTrackerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
