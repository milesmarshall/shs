import { Component, OnInit } from '@angular/core';

import { GOOGLE_API } from 'app/app.constants';

@Component({
  selector: 'app-incident-research-tracker',
  templateUrl: './incident-research-tracker.component.html',
  styleUrls: ['./incident-research-tracker.component.css']
})
export class IncidentResearchTrackerComponent implements OnInit {

  constructor() { }

  googleAPI: string = GOOGLE_API;
  
  /* Map */
  // Google map lat-long
  lat: number = 51.678418;
  lng: number = 7.809007;
  
  ngOnInit(): void {
  }

}
