import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncidentReportBuilderComponent } from './incident-report-builder.component';

describe('IncidentReportBuilderComponent', () => {
  let component: IncidentReportBuilderComponent;
  let fixture: ComponentFixture<IncidentReportBuilderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncidentReportBuilderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncidentReportBuilderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
