import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IncidentAssetsComponent } from './list/incident-assets/incident-assets.component';

import { ListComponent } from './list/list.component';
import { AddComponent } from './add/add.component';
import { AddIncidentComponent } from './add-incident/add-incident.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'list',
        component: ListComponent,
        data: {
          title: 'Incident List'
        }
      },
      {
        path: 'add',
        component: AddComponent,
        data: {
          title: 'Incident Add'
        }
      },
      {
        path: 'add-incident',
        loadChildren: () => import('./add-incident/add-incident.module').then(m => m.AddIncidentModule)
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IncidentRoutingModule { }
