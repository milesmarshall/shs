<div *ngIf="loading" class="d-flex justify-content-center">
  <div class="spinner-border" role="status">
    <span class="sr-only">Loading...</span>
  </div>
</div>

<div>{{errorMsg}}</div>
<div class="form-group option-buttons">
  <span class="incident-actions float-right mr-0">
    <!-- Buttons with Icon -->
    <button type="button" class="btn btn-raised btn-secondary btn-min-width mr-1 mb-1" placement="top"
      ngbTooltip="Refresh incident list" (click)="getIncidentsData()"><i class="fa fa-refresh"></i> Refresh</button>
    <button type="button" class="btn btn-raised btn-secondary btn-min-width mr-1 mb-1" placement="top"
      ngbTooltip="Clear all new/updated incident status indicators"><i class="fa fa-window-close"></i> Clear</button>
    <button [disabled]="buttonDisabled === true" (click)="openModalBulk(editBulkModal)" type="button"
      class="btn btn-raised btn-secondary btn-min-width mr-1 mb-1" placement="top"
      ngbTooltip="Bulk edit/select indicators"><i class="fa fa-pencil"></i> Bulk Edit</button>
    <button [disabled]="buttonDisabled === true" (click)="openModalViewPrint(viewAssetInfoModal)" type="button"
      class="btn btn-raised btn-secondary btn-min-width mr-1 mb-1" data-toggle="modal" data-target="#viewIncidentModal"
      placement="top" ngbTooltip="View/print highlighted incident"><i class="fa fa-search-plus"></i> Quick
      View/Print</button>
    <button [disabled]="buttonDisabled === true" (click)="openModalEmail(emailIncident)" type="button"
      class="btn btn-raised btn-secondary btn-min-width mr-1 mb-1" data-toggle="modal" data-target="#addEmailModal"
      placement="top" ngbTooltip="Email details of checked incident"><i class="fa fa-envelope"></i> Email</button>
  </span>
</div>

<!-- CHECKBOX FILTERS -->
<div class="col-12 row toggle-filter-options mr-0 ml-0 pl-0 pr-0">
  <!-- toggle filter options -->
  <div class="col-8 ml-0 pl-0">
    <ul class="nav">
      <li class="nav-item mr-3">
        <div class="custom-control custom-checkbox">
          <input type="checkbox" class="custom-control-input" id="chkLoggedByMe" (change)="filterIncidents()">
          <label class="custom-control-label" for="chkLoggedByMe">Logged by me</label>
        </div>
      </li>
      <li class="nav-item mr-3">
        <div class="custom-control custom-checkbox">
          <input type="checkbox" class="custom-control-input" id="chkAssignedToMe" (change)="filterIncidents()">
          <label class="custom-control-label" for="chkAssignedToMe">Assigned to me</label>
        </div>
      </li>
      <li class="nav-item mr-3">
        <div class="custom-control custom-checkbox">
          <input #checkInclClosed type="checkbox" class="custom-control-input" id="chkInclClosed"
            (click)="toggleShowDateSelector()">
          <label class="custom-control-label" for="chkInclClosed">Include closed</label>
        </div>
      </li>
      <li class="nav-item mr-3">
        <div class="custom-control custom-checkbox">
          <input #checkInclCancelled type="checkbox" class="custom-control-input" id="chkInclCancelled"
            (click)="toggleShowDateSelector()">
          <label class="custom-control-label" for="chkInclCancelled">Include cancelled</label>
        </div>
      </li>
    </ul>

    <!-- If closed/cancelled is checked, a date range must be spesified -->
    <!-- MJA TO DO - validate that date was selected OR default date fields to today -->
    <div class="form-group row form-inline col-md-6 " *ngIf="showDateSelector">
      <!-- *ngIf="inclClosed.checked || chkInclCancelled.checked" -->
      <!-- [hidden]="!inclClosed.checked"-->
      <label class="label-control m-1" for="dateStart">From: </label>
      <input type="date" id="dateStart" class="form-control m-1" name="dateStart" data-toggle="tooltip"
        (change)="setDateSelection($event)" data-trigger="hover" data-placement="top" data-title="Start Date">

      <label class="label-control m-1" for="dateEnd">To: </label>
      <input type="date" id="dateEnd" class="form-control m-1" name="dateEnd" data-toggle="tooltip"
        (change)="setDateSelection($event)" data-trigger="hover" data-placement="top" data-title="Date Fixed">

      <button type="button" class="btn btn-raised btn-secondary btn-min-width mr-1 mb-1 m-1"
        ngbTooltip="Get incidents for date range." (click)="getIncidentsData()"><i class="fa fa-refresh"></i>
        Load</button>
    </div>
  </div>
  <!-- end toggle filter options -->

  <!-- FILTER BUTTONS: Face Search, Filter -->
  <div class="col-4 mr-0 pl-0 text-right">

    <button class="btn btn-outline-secondary mr-1" type="button" data-toggle="collapse" data-target=""
      aria-expanded="false" aria-controls="">
      <i class="fa fa-user"></i> Face Search
    </button>
    <button (click)="toggle()" id="bt" class="btn btn-outline-secondary" type="button">
      <i class="fa fa-plus"></i> Filter Incidents
    </button>

  </div>
  <!-- end filter button -->

  <!-- filter options -->
  <ng-container *ngIf="show">
    <div class="w-100">
      <div class="card card-body border">
        <form>
          <div class="form-row">
            <div class="form-group col-md-3">
              <label for="inputState">Report</label>
              <select id="inputState" class="form-control">
                <option selected>All</option>
                <option *ngFor="let item of reports" value="item.id"> {{ item.name }} </option>
              </select>
            </div>
            <div class="form-group col-md-3">
              <label for="inputState">Site</label>
              <select id="inputState" class="form-control">
                <option selected>All</option>
                <option *ngFor="let item of sites" value="item.id"> {{ item.name }} </option>
              </select>
            </div>
            <div class="form-group col-md-3">
              <label for="inputState">Categories</label>
              <select id="inputState" class="form-control">
                <option selected>All</option>
                <option *ngFor="let item of categories" value="item.id"> {{ item.name }} </option>
              </select>
            </div>
            <div class="form-group col-md-3">
              <label for="inputState">Impact</label>
              <select id="inputState" class="form-control">
                <option selected>All</option>
                <option *ngFor="let item of impacts" value="item.id"> {{ item.description }} </option>
              </select>
            </div>
            <div class="form-group col-md-3">
              <label for="inputState">Status</label>
              <select id="inputState" class="form-control">
                <option selected>All</option>
                <option *ngFor="let item of statuses" value="item.id"> {{ item.description }} </option>
              </select>
            </div>
            <div class="form-group col-md-3">
              <label for="inputState">Logged by/Assigned to</label>
              <select id="inputState" class="form-control">
                <option selected>All</option>
                <option *ngFor="let item of resources" value="item.id"> {{ item.firstName }} {{ item.lastName }}
                </option>
              </select>
            </div>
            <div class="form-group col-md-3">
              <label for="inputState">Incidents without GPS</label>
              <select id="inputState" class="form-control">
                <option selected>No</option>
                <option>Yes</option>
              </select>
            </div>
          </div>

          <button type="submit" class="btn btn-primary">Clear</button>
        </form>
      </div>
    </div>
  </ng-container>
  <!-- end filter options -->

</div>
<!-- end filter -->

<!-- filter datatable -->
<section id="filter" class="mb-3">
  <div class="row">
    <div class="col-sm-12">

      <!-- free text filter -->
      <input type='text' id='textFilter' style='padding:8px;margin:15px auto;width:30%;' placeholder='Free Text Search'
        (keyup)='filterIncidents($event)' />

      <ngx-datatable #table [rows]="rows" [limit]="100" columnMode="force" [headerHeight]="50" [footerHeight]="50"
        [loadingIndicator]="loadingIndicator" rowHeight="auto" class="bootstrap" [selected]="selected"
        [selectionType]="SelectionType.checkbox" [selectAllRowsOnPage]="false" [displayCheck]="displayCheck"
        (activate)="onActivate($event)" (select)="onSelect($event)" [externalPaging]="true" [count]="page.totalElements"
        [offset]="page.pageNumber" [limit]="page.size" (page)="getIncidentsData($event)">

        <!-- Checkbox column -->
        <ngx-datatable-column [width]="30" [sortable]="false" [canAutoResize]="false" [draggable]="false"
          [resizeable]="false" [headerCheckboxable]="true" [checkboxable]="true">
        </ngx-datatable-column>

        <ngx-datatable-column name="ID" [width]="50">
          <ng-template let-row="row" ngx-datatable-cell-template>
            {{row.incidentId}}<span *ngIf="row.incidentType==2">-{{row.scheduleInstanceId}}</span>
          </ng-template>
        </ngx-datatable-column>

        <ngx-datatable-column name="Type" [width]="40">
          <ng-template let-row="row" ngx-datatable-cell-template>
            {{ row.incidentType == 1 ? 'Incident' : 'Sched. Task' }}
          </ng-template>
        </ngx-datatable-column>

        <ngx-datatable-column name="Date" [width]="60">
          <ng-template let-row="row" ngx-datatable-cell-template>
            {{ row.incidentDateTime | date:'medium' }}
          </ng-template>
        </ngx-datatable-column>

        <ngx-datatable-column name="Site">
          <ng-template let-row="row" ngx-datatable-cell-template>
            {{row.siteName}} [TO DO: get path]
          </ng-template>
        </ngx-datatable-column>

        <ngx-datatable-column name="Category">
          <ng-template let-row="row" ngx-datatable-cell-template>
            {{row.categoryName}}
          </ng-template>
        </ngx-datatable-column>

        <ngx-datatable-column name="Incident">
          <ng-template let-row="row" ngx-datatable-cell-template>
            {{row.description}}
          </ng-template>
        </ngx-datatable-column>

        <ngx-datatable-column name="Reported/Assigned">
          <ng-template let-row="row" ngx-datatable-cell-template>
            {{row.latestActionResourceId | incidentReportedAssigned:row.reportedResourceId:row.reportedOther}}
          </ng-template>
        </ngx-datatable-column>

        <ngx-datatable-column name="Work Status">
          <ng-template let-row="row" ngx-datatable-cell-template>
            ...TO DO...
          </ng-template>
        </ngx-datatable-column>

        <ngx-datatable-column name="Incident Status">
          <ng-template let-row="row" ngx-datatable-cell-template>
            <!-- TO DO - get icon opacity to 1 -->
            <i class="fa fa-bookmark fa-2" aria-hidden="true" [ngStyle]="styleStatusIndicator(row.statusId)"></i>
            <!-- {color: getStatusIndicatorColor(row.statusId)} -->
            {{row.statusId | incidentStatus }}
          </ng-template>
        </ngx-datatable-column>

        <ngx-datatable-column name="Duration" [width]="50">
          <ng-template let-row="row" ngx-datatable-cell-template>
            {{ row.statusId | incidentDuration:row.statusId:row.modifiedDateTimeStamp:row.dateTimeStamp }}
          </ng-template>
        </ngx-datatable-column>

        <ngx-datatable-column name="Closed" [width]="50">
          <ng-template let-row="row" ngx-datatable-cell-template>
            [TO DO]
          </ng-template>
        </ngx-datatable-column>
      </ngx-datatable>

      <!-- MM
            <input type='text' style='padding:8px;margin:15px auto;width:30%;' placeholder='Free Text Search' (keyup)='updateFilter($event)' />
            <ngx-datatable #table class="bootstrap selection-cell" [selected]="incidentSelected" [selectionType]="'cell'" (select)="onIncidentTableSelect($event)" (activate)="onIncidentTableActivate($event)"  [columns]="columns" [columnMode]="'force'" [headerHeight]="50" [footerHeight]="50" [rowHeight]="'auto'" [limit]="100" [rows]='rows'></ngx-datatable>-->
    </div>
  </div>
</section>
<!-- end filter datatable -->

<!-- view/edit incident popup modal -->
<div class="card">
  <div class="card-content">
    <div class="card-body">
      <ng-template #viewAssetInfoModal let-modal #content let-c="close" let-d="dismiss">
        <div class="modal-header">
          <h4 class="modal-title">View Incident</h4>
          <button type="button" class="close" aria-label="Close" (click)="d('Cross click')">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div id="incident-print-section" class="view-asset-info-modal modal-body">

          <div *ngFor="let sel of incidents; let i = index" class="card border grey" style="margin: 20px">
            <!-- incident data-->
            <div class="row col-12">
              <!-- overview form -->
              <div class="col-md-6">
                <div class="card">
                  <div class="card-header">
                    <h4 class="card-title mb-3" id="basic-layout-tooltip">OVERVIEW</h4>
                  </div>
                  <div class="card-content">
                    <div class="px-3">
                      <form class="form form-horizontal">
                        <div class="form-body">
                          <div class="form-group row">
                            <label class="col-md-3 label-control">Incident Id: </label>
                            <p class="ml-3">{{sel.incidentId}}</p>
                          </div>

                          <div class="form-group row">
                            <label class="col-md-3 label-control">Date/Time: </label>
                            <p class="ml-3">{{ sel.incidentDateTime | date:'medium' }}</p>
                          </div>

                          <div class="form-group row">
                            <label class="col-md-3 label-control">Type: </label>
                            <p class="ml-3">{{ sel.incidentType == 1 ? 'Incident' : 'Sched. Task' }}</p>
                          </div>

                          <div class="form-group row" *ngIf="incident.incidentType == 1">
                            <label class="col-md-3 label-control">Reported By: </label>
                            <p class="ml-3">{{sel.reportedResourceId | resourceName}}</p>
                          </div>

                          <div class="form-group row" *ngIf="incident.incidentType == 2">
                            <label class="col-md-3 label-control">Assigned To: </label>
                            <p class="ml-3">{{sel.reportedResourceId | resourceName}}</p>
                          </div>

                          <div class="form-group row">
                            <label class="col-md-3 label-control">Logged by: </label>
                            <p class="ml-3">{{sel.loggedResourceId | resourceName}}</p>
                          </div>

                          <div class="form-group row">
                            <label class="col-md-3 label-control" for="selStatus">Status: </label>
                            <p class="ml-3">{{sel.statusId }}</p>
                          </div>

                          <div class="form-group row">
                            <label class="col-md-3 label-control" for="selStatus">Closed: </label>
                            <p class="ml-3"></p>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              <!-- end overview form -->

              <!-- sites form -->
              <div class="col-md-6">
                <div class="card">
                  <div class="card-header">
                    <h4 class="card-title mb-3" id="basic-layout-tooltip">&nbsp;</h4>
                  </div>
                  <div class="card-content">
                    <div class="px-3">
                      <form class="form form-horizontal">
                        <div class="form-body">
                          <div class="form-group row">
                            <label class="col-md-3 label-control">Site: </label>
                            <p class="ml-3">{{ sel.siteName }}</p>
                          </div>

                          <div class="form-group row">
                            <label class="col-md-3 label-control">Category: </label>
                            <p class="ml-3">{{ sel.categoryName }}</p>
                          </div>

                          <div class="form-group row">
                            <label class="col-md-3 label-control">Root Cause: </label>
                            <p class="ml-3">{{ sel.rootCauseId }}</p>
                          </div>

                          <div class="form-group row">
                            <label class="col-md-3 label-control">Impact: </label>
                            <p class="ml-3">{{ sel.impact }}</p>
                          </div>

                          <hr />

                          <div class="form-group row">
                            <label class="col-md-3 label-control">Details: </label>
                            <p class="ml-3">{{ sel.description }}</p>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              <!-- end sites form -->
            </div>

            <div class="row col-12">
              <!-- info form -->
              <div class="col-md-6">
                <div class="card">
                  <div class="card-header">
                    <h4 class="card-title mb-3" id="basic-layout-tooltip">ADDITIONAL INFORMATION</h4>
                  </div>
                  <div class="card-content">
                    <div class="px-3">
                      <form class="form form-horizontal">
                        <div class="form-body">
                          <div *ngFor="let fields of incidents[i].customFieldValues.source" class="form-group row">
                            <label class="col-md-3 label-control">{{ fields.fieldId }}: </label>
                            <p class="ml-3">{{ fields.value }}</p>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              <!-- end info form -->

              <!-- status history form -->
              <div class="col-md-6">
                <div class="card">
                  <div class="card-header">
                    <h4 class="card-title mb-3" id="basic-layout-tooltip">STATUS HISTORY</h4>
                  </div>
                  <div class="card-content">
                    <div class="px-3">
                      <form class="form form-horizontal">
                        <div class="form-body">
                          <!--<div class="form-group row">
                                      <label class="col-md-3 label-control">???: </label>
                                      <p class="ml-3"></p>
                                  </div>-->
                          <table class="table">
                            <tr>
                              <th>STATUS</th>
                              <th>DURATION</th>
                              <th>RESOURCE</th>
                            </tr>
                            <tr *ngFor="let res of incidents[i].statusHistory.source">
                              <th>{{ res.statusId }}</th>
                              <th>Duration</th>
                              <th>{{ res.loggedResourceId }}</th>
                            </tr>
                          </table>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              <!-- end status history form -->
            </div>

            <div class="row col-12">
              <!-- info form -->
              <div class="col-md-6">
                <div class="card">
                  <div class="card-header">
                    <h4 class="card-title mb-3" id="basic-layout-tooltip">FIXED ASSETS</h4>
                  </div>
                  <div class="card-content">
                    <div class="px-3">
                      <form class="form form-horizontal">
                        <div class="form-body">
                          <table class="table">
                              <tr>
                                <th>NAME</th>
                                <th>MAKE</th>
                                <th>MODEL</th>
                                <th>SERIAL NUMBER</th>
                              </tr>
                              <tr *ngFor="let res of incidents[i].assets.source">
                                <ng-container *ngIf="res.assetTypeId == 1">
                                  <th>{{ res.name }}</th>
                                  <th>{{ res.make }}</th>
                                  <th>{{ res.model }}</th>
                                  <th>{{ res.serialNumber }}</th>
                                </ng-container>
                              </tr>
                          </table>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              <!-- end info form -->

              <!-- status history form -->
              <div class="col-md-6">
                <div class="card">
                  <div class="card-header">
                    <h4 class="card-title mb-3" id="basic-layout-tooltip">HUMAN ASSETS</h4>
                  </div>
                  <div class="card-content">
                    <div class="px-3">
                      <form class="form form-horizontal">
                        <div class="form-body">
                          <table class="table">
                            <tr>
                              <th>FIRST NAME</th>
                              <th>LAST NAME</th>
                              <th>ASSET NUMBER</th>
                            </tr>
                            <tr *ngFor="let res of incidents[i].assets.source">
                              <ng-container *ngIf="res.assetTypeId == 2">
                                <th>{{ res.name }}</th>
                                <th>{{ res.lastName }}</th>
                                <th>{{ res.serialNumber }}</th>
                              </ng-container>
                            </tr>
                          </table>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              <!-- end status history form -->
            </div>

            <div class="row col-12">
              <!-- info form -->
              <div class="col-md-6">
                <div class="card">
                  <div class="card-header">
                    <h4 class="card-title mb-3" id="basic-layout-tooltip">ACTIONS</h4>
                  </div>
                  <div class="card-content">
                    <div class="px-3">
                      <form class="form form-horizontal">
                        <div class="form-body">
                          <table class="table">
                            <!--<ng-template *ngIf="incident.actionsTaken.source.length != 0; else noActions">-->
                              <tr>
                                <th>DATE & TIME</th>
                                <th>DESCRIPTION</th>
                                <th>RESOURCE</th>
                              </tr>
                              <tr *ngFor="let res of incidents[i].actionsTaken.source">
                                <td>{{ res.dateTimeStamp }}</td>
                                <td>{{ res.description }}</td>
                                <td>{{ res.assignedResources }}</td>
                              </tr>
                            <!--</ng-template>
                            <ng-template #noActions>
                              <tr>
                                <td colspan="3">No Actions</td>
                              </tr>
                            </ng-template>-->
                          </table>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              <!-- end info form -->

              <!-- status history form -->
              <div class="col-md-6">
                <div class="card">
                  <div class="card-header">
                    <h4 class="card-title mb-3" id="basic-layout-tooltip">NOTIFICATIONS</h4>
                  </div>
                  <div class="card-content">
                    <div class="px-3">
                      <form class="form form-horizontal">
                        <div class="form-body">
                          <table class="table">
                            <!--<ng-template *ngIf="incident.escalation.source.length != 0; else noNotices">-->
                              <tr>
                                <th>NOTIFIED ON</th>
                                <th>RESOURCE</th>
                              </tr>
                              <tr *ngFor="let res of incidents[i].escalation.source">
                                <td colspan="2">{{ res.description }}</td>
                              </tr>
                            <!--</ng-template>
                            <ng-template #noNotices>
                              <tr>
                                <td colspan="3">No Notifications</td>
                              </tr>
                            </ng-template>-->
                          </table>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              <!-- end status history form -->
            </div>
            <!-- end incident data -->
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary btn-raised" (click)=print()>Print</button>
          <button type="button" class="btn btn-secondary btn-raised" (click)="c('Close click')">Close</button>
        </div>
      </ng-template>
    </div>
  </div>
</div>
<!-- end view/edit incident popup modal -->

<!-- bulk edit popup modal -->
<div class="card">
  <div class="card-content">
    <div class="card-body">
      <ng-template #editBulkModal let-modal #content let-c="close" let-d="dismiss">
        <div class="modal-header">
          <h4 class="modal-title">Bulk Edit Incidents</h4>
          <button type="button" class="close" aria-label="Close" (click)="d('Cross click')">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <!-- incident data-->
          <div class="row col-12">
            <!-- overview form -->
            <div class="card col-12">
              <div class="card-content">
                <div class="px-3">
                  <form class="form form-horizontal">
                    <div class="form-body">

                      <div class="form-group row col-12">
                        <label class="col-5 label-control">Incidents being edited: </label>
                        <p class="col-7">
                          <ng-container *ngFor="let inc of incident.selected">
                            <span>{{inc.incidentId}}, </span>
                          </ng-container>
                        </p>
                      </div>

                      <div class="form-group row col-12">
                        <label class="col-5 label-control" for="selStatus">Status:</label>
                        <div class="col-7">
                          <select id="selStatus" name="selStatus" class="form-control" (change)="selectStatus($event.target.value)">
                            <option value="none" selected="" disabled="">[Select Status]</option>
                            <option *ngFor="let status of incident.statuses[0]" value="{{status.id}} {{incident.statusId}}" [selected]="status.id === incident.selected[0].statusId">
                              {{status.description}}
                            </option>
                          </select>
                        </div>
                      </div>

                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary btn-raised">Save</button>
          <button type="button" class="btn btn-secondary btn-raised" (click)="c('Close click')">Close</button>
        </div>
      </ng-template>
    </div>
  </div>
</div>

<!-- end bulk edit modal -->

<!-- add email modal -->
<div class="card">
  <div class="card-content">
    <div class="card-body">
      <ng-template #emailIncident let-modal #content let-c="close" let-d="dismiss">
        <div class="modal-header">
          <h4 class="modal-title">Email Incidents</h4>
          <button type="button" class="close" aria-label="Close" (click)="d('Cross click')">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <!-- incident data-->
          <div class="row col-12">
            <!-- overview form -->
            <div class="card col-12">
              <div class="card-content">
                <div class="px-3">
                  <form class="form form-horizontal">
                    <div class="form-body">

                      <div class="form-group row col-12">
                        <label class="col-4 label-control" for="selStatus">Send Email To:</label>
                        <div class="col-8">
                          <div class="list-group checkbox-list-group" style="overflow:scroll; max-height: 121px;">
                            <div *ngFor="let res of incident.resources[0]" class="list-group-item" style="padding: 9px 7px 5px 7px;">&nbsp;
                              <label>
                                <input type="checkbox" style="vertical-align: middle;">
                                <span class="list-group-item-text"><i class="fa fa-fw"></i> {{res.firstName}} {{res.lastName}}</span>
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="form-group row col-12">
                        <label class="col-4 label-control" for="selStatus">Other Recipient:</label>
                        <div class="col-8">
                          <input type="text" id="txtOthers" class="form-control">
                        </div>
                      </div>

                      <div class="form-group row col-12">
                        <label class="col-4 label-control" for="selStatus">Subject:</label>
                        <div class="col-8">
                          <input type="text" id="txtSubject" class="form-control" value="Incident Desk - Incident Data">
                        </div>
                      </div>

                      <div class="form-group row col-12">
                        <label class="col-4 label-control" for="selStatus">Comment:</label>
                        <div class="col-8">
                          <textarea id="txtComments" rows="5" class="form-control"></textarea>
                        </div>
                      </div>

                      <div class="form-group row col-12">
                        <label class="col-4 label-control" for="selStatus">Include Documents:</label>
                        <div class="col-8">
                          <input class="form-check-input ml-0" type="checkbox" id="chkIncludeDocuments" value="option1">
                        </div>
                      </div>

                      <div class="form-group row col-12">
                        <label class="col-4 label-control" for="selStatus">Documents:</label>
                        <div class="col-4">
                          <label class="custom-file">
                            <input type="file" ng2FileSelect [uploader]="uploader">
                            <span class="custom-file-control"></span>
                          </label>
                        </div>
                        <div class="col-2">
                          <input type="radio" class="form-check-input" name="optDocument"> Attachment
                        </div>
                        <div class="col-2">
                          <input type="radio" class="form-check-input" name="optDocument"> Inline
                        </div>
                      </div>

                      <div class="form-group row col-12" style="margin-top: 10px;">
                        <label class="col-4 label-control" for="selStatus">&nbsp;</label>
                        <div class="col-8">
                          <div class="list-group checkbox-list-group" style="overflow:scroll; max-height: 121px;">
                            <div *ngFor="let doc of incident.documents" class="list-group-item" style="padding: 9px 7px 5px 7px;">&nbsp;<label>
                              <input type="checkbox" style="vertical-align: middle;">
                                <span class="list-group-item-text"><i class="fa fa-fw"></i> {{doc}}</span></label>
                            </div>
                          </div>
                        </div>
                      </div>

                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary btn-raised">Send</button>
          <button type="button" class="btn btn-secondary btn-raised" (click)="c('Close click')">Close</button>
        </div>
      </ng-template>
    </div>
  </div>
</div>


<!--<div class="modal fade text-left" id="addEmailModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h4 class="modal-title" id="myModalLabel1">Add Email</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
              <form class="form form-horizontal lg">
                  <div class="form-group row">
                      <label class="col-md-3 label-control" for="projectinput5">Sent Mail To: </label>
                      <div class="container-fluid emailCheckBoxList">
                      <div class="row-fluid">
                          <div class="form-check">
                          <label class="form-check-label ml-2">
                              <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">16 Hout St - Kevin Black
                          </label>
                          </div>
                          <div class="form-check">
                          <label class="form-check-label ml-2">
                              <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">16 Hout St - Kevin Black
                          </label>
                          </div>
                          <div class="form-check">
                          <label class="form-check-label ml-2">
                              <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">16 Hout St - Kevin Black
                          </label>
                          </div>
                      </div>
                      </div>
                  </div>

                  <div class="form-group row">
                      <label class="col-md-3 label-control" for="projectinput5">Other Recipients: </label>
                      <div class="col-md-9">
                      <input type="text" id="projectinput5" class="form-control" name="company">
                      </div>
                  </div>

                  <div class="form-group row">
                      <label class="col-md-3 label-control" for="projectinput5">Subject: </label>
                      <div class="col-md-9">
                      <input type="text" id="projectinput5" class="form-control" name="company">
                      </div>
                  </div>

                  <div class="form-group row">
                      <label class="col-md-3 label-control" for="projectinput9">Comments: </label>
                      <div class="col-md-9">
                      <textarea id="projectinput9" rows="5" class="form-control" name="comment"></textarea>
                      </div>
                  </div>

                  <div class="form-group row">
                      <label class="col-md-3 label-control" for="projectinput5">Include Documents: </label>
                      <div class="form-check form-check-inline">
                      <label class="form-check-label ml-2">
                          <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                      </label>
                      </div>
                  </div>
              </form>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-outline-primary">Send</button>
          </div>
      </div>
  </div>
</div>-->
<!-- end email modal -->