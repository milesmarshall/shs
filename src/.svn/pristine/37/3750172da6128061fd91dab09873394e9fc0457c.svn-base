import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs/observable';
import { map, filter, take, exhaustMap } from 'rxjs/operators';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

// import { AnyCnameRecord } from 'dns';

import { IncidentVO } from 'app/shared/models/incidentVO.model';
import { AuthService } from '../auth/auth.service';
import { LookupsService } from './lookups.service';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class IncidentsService {

  // subject used to open Incident Detail tab after double click of incident in list (listComponent listens for event)
  showIncidentDetailEmitter = new Subject<any>();

  constructor(private httpClient: HttpClient, private authService: AuthService, private lookupsService: LookupsService) { }

  errorHandler(error: HttpErrorResponse) {
    return Observable.throw(error.message || 'Server Error');
  }

  /**
   * A method to get incidents to display in incident list (max 100)
   * @param isLite Light query with limited data or not
   * @param getCount Return total count & status counts of incidents in db
   * @param pageToGet The page to load

   * @param inclClosed Include incidents with status 'closed' in list (excl in default query)
   * @param inclCancelled Include incidents with status 'cancelled' in list (excl in default query)
   * @param dateStart start of date range - required if closed or cancelled checked
   * @param dateStart end of date range - required if closed or cancelled checked
   * @returns {any} An observable containing the incident data
   */
    getIncidents(
      isLite,
      getCount,
      pageToGet,
      inclClosed: boolean = false,
      inclCancelled: boolean = false,
      dateStart: string = '',
      dateEnd: string = '') {

      const headers = new HttpHeaders();
      headers.append('Content-Type', 'application/json');

      const thisInclClosed = (inclClosed ? 1 : 0);
      const thisInclCancelled = (inclCancelled ? 1 : 0);

      // session token & securityGroupId is automatically appended in auth-interceptor service
      const params = new HttpParams()
        .set('isLite', isLite)
        .set('getCount', getCount)
        .set('pageNo', pageToGet)
        .set('inclClosed', thisInclClosed.toString())
        .set('inclCancelled', thisInclCancelled.toString())
        .set('dateStart', dateStart)
        .set('dateEnd', dateEnd);

      return this.httpClient
        .get(
            `${environment.apiUrl}/incidents`,
            { headers: headers, params: params }
        )
        .catch(this.errorHandler);
  }

  getIncidentDetails(view: string, incidentIds): Observable<any> {
    console.log('getIncidentDetails');

    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');

    const params = new HttpParams()
      .set('view', view)
      .set('incidentIds', incidentIds);

    // call returns an array of incident(s)
    return this.httpClient
      .get<any>(
          `${environment.apiUrl}/incidents`,
          { headers: headers, params: params }
      )
      .pipe(
        map(responseData => {
          // extract single incident from response data
          const incident: IncidentVO = responseData.result[0];
          return incident;
        })
      )
      .catch(this.handleError);
  }

  private handleError(error: Response) {
    return Observable.throw(error.statusText);
  }

  public getIncidentFromId(incidentId, incidents:any=[]) {
    console.log('getIncidentFromId');

    if (!incidents)
    {
      var data: any = JSON.parse(sessionStorage.getItem('incidents'));
          data = data[0]['result'];
    }
    else
      var data = incidents;

    for (let i = 0; i < data.length; i++) {
        //console.log(Number(data[i].incidentId) +'=='+ Number(incidentId));

        if (Number(data[i].incidentId) == Number(incidentId)) {
          return data[i];
        }
    }
  }

  public getCategoryFromId(id, field) {
    let data: any = JSON.parse(sessionStorage.getItem('categories'));

    for (let i = 0; i < data.length; i++) {
        if (Number(data[i].id) == Number(id)) {
          if (field == null)
            return data[i];
          else if (field == 'name')
            return data[i].name;
        }
    }
  }

  public getStats(statusId) {
    const data = JSON.parse(sessionStorage.getItem('incident_stats'));

    let stats_total = 0;

    if (data) {
      data.forEach(element => {
          if (statusId == '') {
            stats_total += Number(element.statusCount);
        } else {
            if (element.statusId == statusId)
              stats_total = element.statusCount;
        }
      });
    }

    return stats_total;
  }

  calculateDiffFromToday(dateSent) {
    const currentDate = new Date();
    dateSent = new Date(dateSent);

    return Math.floor((Date.UTC(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate())
      - Date.UTC(dateSent.getFullYear(), dateSent.getMonth(), dateSent.getDate()) )
      / (1000 * 60 * 60 * 24));
  }

  calculateDiffFromDate(dateFrom, dateTo) {
    const currentDate = new Date();
    dateTo = new Date(dateTo);

    return Math.floor((Date.UTC(dateFrom.getFullYear(), dateFrom.getMonth(), dateFrom.getDate())
      - Date.UTC(dateTo.getFullYear(), dateTo.getMonth(), dateTo.getDate()) )
      / (1000 * 60 * 60 * 24));
  }

  // Function to convert seconds into time frame in format d/h/m eg:  10d 5:23
  convertSecondsToTime(seconds: number) {
    let ret = '';

    // var seconds = parseInt(123456, 10);
    const days = Math.floor(seconds / (3600 * 24));
    seconds  -= days * 3600 * 24;
    const hrs = Math.floor(seconds / 3600);
    seconds  -= hrs * 3600;
    const mnts = Math.floor(seconds / 60);
    seconds  -= mnts * 60;

    ret = days.toString() + 'd ' + hrs.toString() + ':' + mnts.toString();

    return ret;
  }

  // Get duration that incident has existed
  getDuration(statusId: number, modifiedDateTimeStamp: string, dateTimeStamp: string) {
    let ret = '';
    let diffSeconds = 0;

    switch (statusId) {
      case 2: // Cancelled
      case 4: // Closed
          if (modifiedDateTimeStamp !== '0000-00-00 00:00:00') {
            // get duration from when incident was created to last time it was edited(closed/cancelled)
            diffSeconds = (new Date(modifiedDateTimeStamp).getTime() - new Date(dateTimeStamp).getTime()) / 1000;
          }
          break;

      default: // all other statusses
        // get duration from when incident was created to now (getTime gets miliseconds)
        diffSeconds = (new Date().getTime() - new Date(dateTimeStamp).getTime()) / 1000;
        break;
    }

    if (diffSeconds > 0) {
      ret = this.convertSecondsToTime(diffSeconds);
    }

    return ret;
  }

  sendNotification(incident: any, emailTo: string, emailComments: string, emailDocuments: any, emailAttachmentType: number, emailSubject: string, action: string, securityGroupId: number, sessionToken: string ) {
    return this.httpClient.post<any>(`${environment.apiUrl}/login`, { incident, emailTo, emailComments, emailDocuments, emailAttachmentType, emailSubject, action, securityGroupId, sessionToken })
      .pipe(tap(responseData => {

        console.log('responseData');
        console.log(responseData);

        //return responseData;
      }));
  }

}
