import { Component, ViewContainerRef, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';

import { IncidentsService } from '../app/shared/services/incidents.service';
import { LookupsService } from '../app/shared/services/lookups.service';
import { AssetsService } from '../app/shared/services/assets.service';
import { ProvidersService } from '../app/shared/services/providers.service';
import { UsersService } from '../app/shared/services/users.service';
import { ModalsService } from '../app/shared/services/modals.service';

import { SITES } from 'app/app.constants';
import { CATEGORIES } from 'app/app.constants';
import { RGRESOURCES } from 'app/app.constants';
import { CUSTOMFIELDS } from 'app/app.constants';
import { IMPACTS } from 'app/app.constants';
import { ROOTCAUSES } from 'app/app.constants';
import { REPORTS } from 'app/app.constants';
import { PREFERENCES } from 'app/app.constants';
import { USERTYPES } from 'app/app.constants';
import { FIELDNAMES } from 'app/app.constants';
import { STATUSES } from 'app/app.constants';

import { INCIDENTS } from 'app/app.constants';
import { INCIDENTS_TODAY } from 'app/app.constants';
import { INCIDENTS_OPEN } from 'app/app.constants';
import { INCIDENTS_HOLD } from 'app/app.constants';
import { INCIDENTS_LENGTH } from 'app/app.constants';

import { ASSETS_FIXED } from 'app/app.constants';
import { ASSETS_PEOPLE } from 'app/app.constants';
import { ASSETS_FIXED_LENGTH } from 'app/app.constants';
import { ASSETS_PEOPLE_LENGTH } from 'app/app.constants';

import { SERVICE_PROVIDERS } from 'app/app.constants';
import { SERVICE_PROVIDERS_LENGTH }  from 'app/app.constants';


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent implements OnInit, OnDestroy {

    subscription: Subscription;
    articles;
    users;
    error = '';
    message = null;

    sites = SITES;
    categories = CATEGORIES;
    rgResources = RGRESOURCES;
    customFields = CUSTOMFIELDS;
    impacts = IMPACTS;
    rootCauses = ROOTCAUSES;
    reports = REPORTS;
    preferences = PREFERENCES;
    userTypes = USERTYPES;
    fieldNames = FIELDNAMES;
    statuses = STATUSES;

    incidents = INCIDENTS;
    incidents_today = INCIDENTS_TODAY;
    incidents_open = INCIDENTS_OPEN;
    incidents_hold = INCIDENTS_HOLD;
    incidents_length = INCIDENTS_LENGTH;

    assets_fixed = ASSETS_FIXED;
    assets_people = ASSETS_PEOPLE;
    assets_fixed_length = ASSETS_FIXED_LENGTH;
    assets_people_length = ASSETS_PEOPLE_LENGTH;

    service_providers = SERVICE_PROVIDERS;
    service_providers_length = SERVICE_PROVIDERS_LENGTH;

    constructor(
        private router: Router,
        private incidentsService: IncidentsService,
        private lookupsService: LookupsService,
        private assetsService: AssetsService,
        private providersService: ProvidersService,
        private usersService: UsersService,
        private modalsService: ModalsService
        ) {
    }

    ngOnInit() {
        //this.modalsService.testModal('damn');

        this.subscription = this.router.events
            .pipe(
                filter(event => event instanceof NavigationEnd)
            )
            .subscribe(() => window.scrollTo(0, 0));

        /* static lookup data
        this.lookupsService.getNews().subscribe((data)=>{
            console.log(data);
            this.articles = data['articles'];
        
            localStorage.setItem('articles', JSON.stringify(this.articles));
            });
         */

        // get incidents
        this.incidentsService.getIncidents('000',40).subscribe((data)=>{
            this.incidents.push(data);
            this.incidents_length = this.incidents[0]['result']['source'].length;

            console.log('incident length:' + this.incidents_length);
            console.log(this.incidents);
        });

        // get assets fixed
        this.assetsService.getAssetsFixed('000',40,0,1,'assetsFixed').subscribe((data)=>{
            this.assets_fixed.push(data);
            this.assets_fixed_length = this.assets_fixed[0]['result'].length;

            console.log(this.assets_fixed_length);
            console.log(ASSETS_FIXED_LENGTH);
            console.log(this.assets_fixed);
        });

        // get assets people
        this.assetsService.getAssetsPeople('000',40,0,1,'assetsPeople').subscribe((data)=>{
            this.assets_people.push(data);
            this.assets_people_length = this.assets_people[0]['result'].length;
            console.log(this.assets_people);
        });

        // get service providers
        this.providersService.getServiceProviders('000',0).subscribe((data)=>{
            this.service_providers.push(data);
            this.service_providers.length = this.service_providers[0]['result'].length;
            console.log(this.service_providers);
        });
        
        // get sites
        this.lookupsService.getLookups('000','40','sites').subscribe((data)=>{
            this.sites.push(data);
            console.log(this.sites);
        });

        // get categories
        this.lookupsService.getLookups('000','40','categories').subscribe((data)=>{
            this.categories.push(data);
            console.log(this.categories);
        });

        // get rgResources
        this.lookupsService.getLookups('000','40','rgResources').subscribe((data)=>{
            this.rgResources.push(data);
            console.log(this.rgResources);
        });

        // get customFields
        this.lookupsService.getLookups('000','40','customFields').subscribe((data)=>{
            this.customFields.push(data);
            console.log(this.customFields);
        });

        // get impacts
        this.lookupsService.getLookups('000','40','impacts').subscribe((data)=>{
            this.impacts.push(data);
            console.log(this.impacts);
        });

        // get rootCauses
        this.lookupsService.getLookups('000','40','rootCauses').subscribe((data)=>{
            this.rootCauses.push(data);
            console.log(this.rootCauses);
        });

        // get reports
        this.lookupsService.getLookups('000','0','reports').subscribe((data)=>{
            this.reports.push(data);
            console.log(this.reports);
        }); 
        
        // get preferences
        this.lookupsService.getLookups('000','0','preferences').subscribe((data)=>{
            this.preferences.push(data);
            console.log(this.preferences);
        });

        // get userTypes
        this.lookupsService.getLookups('000','0','userTypes').subscribe((data)=>{
            this.userTypes.push(data);
            console.log(this.userTypes);
        });

        // get fieldNames
        this.lookupsService.getLookups('000','0','fieldNames').subscribe((data)=>{
            this.fieldNames.push(data);
            console.log(this.fieldNames);
        });

        // get status
        this.lookupsService.getLookups('000','0','status').subscribe((data)=>{
            this.statuses.push(data);
            console.log(this.statuses);
        });

        /* get user data
        this.usersService.getUsers().subscribe((data)=>{
            console.log(data);
            this.users = data['result'];
        
            localStorage.setItem('result', JSON.stringify(this.users));
            });
         */
    }

    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

}