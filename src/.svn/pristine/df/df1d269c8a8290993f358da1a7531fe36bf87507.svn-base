export class User {
    constructor(
        public id: number = 0,
        public username: string,
        public firstName: string = '',
        public lastName: string = '',
        public securitygroupId: number,
        public resourcegroupId: number,
        public typeId: number,
        public isActive: number,
        public allowCRM: number = 0,
        public allowAdd: number = 0,
        public allowAssets: number = 0,
        public allowPredictiveAnalysis: number = 0,
        public allowEasyRoster: number = 0,
        public allowPublicCommunication: number = 0,
        public allowFaceScan: number = 0, // Enable face scanner tab
        public allowResourceMap: number = 0, // Enable Resource map
        public allowRegisterHumanAsset: number = 0, // Enable user to register a human asset based on returned face data
        public limitStatusChange: number = 0,
        public updatePassword: number,
        public allowDepartmentReports: number = 0,
        public systemResource: number = 0,
        public acceptTermsDate: object,
        /*public checked: boolean = false,
        public checkedAction: boolean = false,
        public checkedLoggedWork: boolean = false,
        public isCheckedEmail: boolean = false,
        public isCheckedSMS: boolean = false, */
        public securityGroup: object, //  (TO DO - update type to VO) SecurityGroupVO
        public locationDelta: number = 0,
        public locationActive: number = 0,
        public lastKnownLatitude: number = 0,
        public lastKnownLongitude: number = 0,
        public locationTimestamp: number = 0,

        // public resourceGroup?: object, // (TO DO - update type to VO) Not returned by resources lookup - ONLY populated for logged in user after login
        public idNumber?: string,
        public gender?: object,
        public staffnumber?: string,
        public email?: string,
        public contactNumberMain?: string,
        public contactNumberAlt?: string,
        public cellNumber?: string,
        public employmentType?: string,
        public employmentStatus?: string,
        public jobTitle?: string,
        public resourceGroupName?: string,
        public managerAccess?: number,
        public blacklisted?: number,
        public userEnabled?: number,
        public pushNotificationId?: string,
        public activationcode?: string,
        public activationcodeEmail?: string,
        public cell911One?: string,
        public cell911Two?: string,
        public cell911Three?: string,
        public email911One?: string,
        public email911Two?: string,
        public email911Three?: string,
        public popiAcceptanceTimestamp?: number,
        // public authdata?: string,

        private _sessionToken?: string,
        private _tokenExpirationDate?: Date,
    ) {}

    // user getter to prevent user from overwriting value & also enable validation at time of retrieval
    get sessionToken() {

        // if tokenDate does not exist, or date is AFTER now, return null for token
        if (!this._tokenExpirationDate || new Date() > this._tokenExpirationDate ) {
            return null
        }
        return this._sessionToken
    }

    get tokenExpirationDate() {
        return this._tokenExpirationDate
    }

}
