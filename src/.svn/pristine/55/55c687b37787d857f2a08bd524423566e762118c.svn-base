import { Component, ViewChild, OnInit } from '@angular/core';
import { NgForm, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'app/shared/auth/auth.service';
import { first } from 'rxjs/operators';

@Component({
    selector: 'app-login-page',
    templateUrl: './login-page.component.html',
    styleUrls: ['./login-page.component.scss']
})

export class LoginPageComponent implements OnInit {
    loginForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    error = '';
    message = null;

    // @ViewChild('f') loginForm: NgForm;

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private authService: AuthService) {

        // redirect to home if already logged in
        // if (this.authService.currentUserValue) {
        //    this.router.navigate(['/dashboard/dashboard']);
        // }
    }

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }

    // convenience getter for easy access to form fields (used by validation in HTML)
    get f() {
        return this.loginForm.controls;
    }

    // Login form - onsubmit button click
    onSubmit(thisForm: NgForm) {
        this.message = null;
        this.submitted = true;

        if (!thisForm.valid) {
            return;
        }

        this.loading = true;

        console.log('[onsubmit]username:' + thisForm.value.username);
        console.log('[onsubmit]psw:' + thisForm.value.password);

        this.authService.login( thisForm.value.username, thisForm.value.password)
            .pipe(first())
            .subscribe(
                responseData => {
                    console.log('[login-page] you have been logged in');
                    console.log(responseData);
                    this.loading = false;
                    this.router.navigate([this.returnUrl]);
                },
                error => {
                    console.log('[login-page] ERROR');
                    console.log(error);

                    this.message = 'We were unable to log you in. Please try again.';
                    this.error = error;
                    this.loading = false;
                });

        this.loginForm.reset();
    }

    // On Forgot password link click
    onForgotPassword() {
        this.router.navigate(['forgotpassword'], { relativeTo: this.route.parent });
    }
    // On registration link click
    onRegister() {
        this.router.navigate(['register'], { relativeTo: this.route.parent });
    }
}
