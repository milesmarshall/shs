import { Component, OnInit, ViewChild, ChangeDetectorRef, ElementRef } from '@angular/core';
import { DatatableComponent, SelectionType } from '@swimlane/ngx-datatable';
import { CommonModule } from '@angular/common';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { IncidentsService } from 'app/shared/services/incidents.service';
import { AssetsService } from 'app/shared/services/assets.service';
import { LookupsService } from 'app/shared/services/lookups.service';

import { map } from 'rxjs/operators';
import { constants } from 'buffer';
import { formatDate } from '@angular/common';

import { IncidentAssetsVO } from 'app/shared/models/incidentAssetsVO.model';
import { Page } from 'app/shared/models/page.model';

import { ASSETS_FIXED } from 'app/app.constants';
import { ASSETS_PEOPLE } from 'app/app.constants';
import { INCIDENTS } from 'app/app.constants';

declare var require: any;
//const data: any = require('../../shared/data/company.json');

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  @ViewChild(DatatableComponent) table: DatatableComponent;

  incidents:any[] = INCIDENTS;
  assets_fixed:any[] = ASSETS_FIXED;
  assets_people:any[] = ASSETS_PEOPLE;

  selected:any[] = [];
  SelectionType = SelectionType;
  loadingIndicator = false;
  lastClick = 0; // used to determine dblclick on table
  page = new Page(); // required for paging

  incidentsAssets: IncidentAssetsVO = new IncidentAssetsVO();
  //incidentAssets: IncidentAssets = new IncidentAssets();

  isCollapsed = false;
  public show = false;
  showDateSelector = false;
  loading = false;

  buttonDisabled = true;

  isFetching = false;

  public errorMsg;

  constructor(
      private incidentsService: IncidentsService,
      private lookupsService: LookupsService,
      private assetsService: AssetsService,
      private cd: ChangeDetectorRef
    ) {
    //this.temp = [...data];
    //this.rows = data;
    this.page.pageNumber = 0;
    this.page.size = 50;
  }

  rows_people:any[] = [];
  data_people:any[] = [];
  temp_people:any[] = [];
  rows_fixed:any[] = [];
  data_fixed:any[] = [];
  temp_fixed:any[] = [];

  // Table Column Titles
  /*columns = [
    { prop: 'id' },
    { prop: 'type' },
    { prop: 'date' },
    { prop: 'site' },
    { prop: 'category' },
    { prop: 'description' },
    { prop: 'reported' },
    { prop: 'status' },
    { prop: 'duration' }
  ];*/
  columns_people = [
    { prop: 'name' },
    { prop: 'lastName' },
    { prop: 'assetNumber' },
    { prop: 'idNumber' },
    { prop: 'dateOfBirth' },
    { prop: 'gender' },
    { prop: 'ethnicity' },
    { prop: 'city' },
    { prop: 'registeredBy' }
  ]
  columns_fixed = [
    { prop: 'name' },
    { prop: 'description' },
    { prop: 'assetNumber' },
    { prop: 'serialNumber' },
    { prop: 'make' },
    { prop: 'assetModel' },
    { prop: 'resourceId' }
  ]

  ngOnInit(): void {
    this.loading = true;

    //this.getPeopleData();
    this.getFixedData();
  }

  getPeopleData(event:any = null){
    console.log('getPeopleData');

    this.page.pageNumber = (event) ? event.offset : 0; // event.offset;
    this.isFetching = true;
    this.loadingIndicator = true; // displays table loading indicator

    const pageToGet: number = this.page.pageNumber + 1;

    // MJ : need the correct API call here
    this.assetsService.getAssetsPeople('000',40,0,1,'assetsPeople')
      .pipe(
        map(responseData => {
          console.log(responseData);

          const data:any[] = [];

          if (responseData.hasOwnProperty('result')) {
            for (let i = 0; i < responseData['result'].length; i++) {

              // convert json to VO obj and add to array
              const jsonObj = responseData['result'][i];
              const newIncidentsAssetsPeople = new IncidentAssetsVO().deserialize(jsonObj);
              data.push(newIncidentsAssetsPeople);
              this.assets_people.push(data);
            }
          }

          // set pagination vars
          this.page.totalElements = responseData['count'];
          this.page.totalPages = this.page.totalElements / this.page.size;

          return data;
          })
        )
        .subscribe((data) => {
          this.isFetching = false;
          this.loadingIndicator = false;
          this.temp_people = [...data]; // cache our list - used for filtering
          this.rows_people = data; // push our inital complete list
          this.cd.markForCheck(); // add this to ensure new data is shown on screen

          this.loading = false;
      });
  }

  getFixedData(event:any = null){
    console.log('getFixedData');

    this.page.pageNumber = (event) ? event.offset : 0; // event.offset;
    this.isFetching = true;
    this.loadingIndicator = true; // displays table loading indicator

    const pageToGet: number = this.page.pageNumber + 1;

    // MJ : need the correct API call here
    this.assetsService.getAssetsPeople('000',40,0,1,'assetsFixed')
      .pipe(
        map(responseData => {
          const data = [];

          if (responseData.hasOwnProperty('result')) {
            for (let i = 0; i < responseData['result'].length; i++) {

              // convert json to VO obj and add to array
              const jsonObj = responseData['result'][i];
              const newIncidentsAssetsFixed = new IncidentAssetsVO().deserialize(jsonObj);
              data.push(newIncidentsAssetsFixed);
              this.assets_fixed.push(data);
            }
          }

          // set pagination vars
          this.page.totalElements = responseData['count'];
          this.page.totalPages = this.page.totalElements / this.page.size;

          return data;
          })
        )
        .subscribe((data) => {
          this.isFetching = false;
          this.loadingIndicator = false;
          this.temp_fixed = [...data]; // cache our list - used for filtering
          this.rows_fixed = data; // push our inital complete list
          this.cd.markForCheck(); // add this to ensure new data is shown on screen

          this.loading = false;
      });
  }

  getData() {
    /*this.assetsService.getAssetsPeople('000',40,0,1,'assetsPeople').subscribe((data_people)=>{
        this.assets_people.push(data_people);
        sessionStorage.setItem('assets_people', JSON.stringify(this.assets_people));
      });

      this.assetsService.getAssetsFixed('000',40,0,1,'assetsFixed').subscribe((data_fixed)=>{
        this.assets_fixed.push(data_fixed);
        sessionStorage.setItem('assets_fixed', JSON.stringify(this.assets_fixed));
      });

      console.log('assets_fixed');
      console.log(this.assets_fixed);*/



      //this.data_people = JSON.parse(sessionStorage.getItem("assets_people"));
      //console.log(sessionStorage.getItem("assets_people"));
      //this.populateData_people();

      //this.data_fixed = JSON.parse(sessionStorage.getItem("assets_fixed"));
      //console.log('this.data_fixed');
      //console.log(JSON.parse(sessionStorage.getItem("assets_fixed")));

      //console.log(this.data_fixed);
      //this.populateData_fixed();
  }

  populateData_people(){
    //console.log(this.data_people);

    //for(let i=0; i<this.temp_data[0].count; i++) {
    for(let i=0; i<50; i++) {
      this.data_people.push({
        "name": this.data_people[0]['result'][i].name,
        "lastName": this.data_people[0]['result'][i].lastName,
        "assetNumber": this.data_people[0]['result'][i].assetNumber,
        "idNumber": this.data_people[0]['result'][i].idNumber,
        "dateOfBirth": this.data_people[0]['result'][i].dateOfBirth,
        "gender": this.data_people[0]['result'][i].gender,
        "ethnicity": this.data_people[0]['result'][i].ethnicity,
        "city": this.data_people[0]['result'][i].city,
        "registeredBy": this.data_people[0]['result'][i].resourceId
      });
    }

    this.temp_people = [...this.data_people];
    this.rows_people = this.data_people;
  }

  populateData_fixed(){
    console.log(this.data_fixed);

    //for(let i=0; i<this.temp_data[0].count; i++) {
    for(let i=0; i<50; i++) {
      this.data_fixed.push({
        "name": this.data_fixed[0]['result'][i].name,
        "description": this.data_fixed[0]['result'][i].description,
        "assetNumber": this.data_fixed[0]['result'][i].assetNumber,
        "serialNumber": this.data_fixed[0]['result'][i].serialNumber,
        "make": this.data_fixed[0]['result'][i].make,
        "assetModel": this.data_fixed[0]['result'][i].assetModel,
        "resourceId": this.data_fixed[0]['result'][i].resourceId
      });
    }

    this.temp_fixed = [...this.data_fixed];
    this.rows_fixed = this.data_fixed;
  }

  updateFilter_people(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp_people.filter(function (d) {
        return d.name.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.rows_people = temp;

    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  updateFilter_fixed(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp_fixed.filter(function (d) {
        return d.name.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.rows_fixed = temp;

    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  //isCollapsed = false;
  //public show:boolean = false;
}
