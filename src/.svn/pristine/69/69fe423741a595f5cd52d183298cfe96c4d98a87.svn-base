export class Incident {

    incidentId: number = 0;
    loggedResourceId: Number;
    siteId: Number;
    siteName: string;
    locationId: number = 0;
    categoryId: number;
    categoryName: string;
    thirdPartyClientId: number = 0; // ID of Third Party client that incident was logged for (usually logged via REST API)
    reportedResourceId: number;
    reportedOther: string;
    description: string;
    summary: string;
    actionSummary: string;
    dateTimeStamp: Object;
    incidentDateTime: Object;
    impactId: number;
    rootCauseId: number;
    subCategoryId: number = 0;
    statusId: number;
    modifiedResourceId: number;
    modifiedDateTimeStamp: object;

    incidentType: number = 1; // There are two types of incidents: 1=incident(default), 2=scheduled task
    incidentName: string;
    scheduleFrequency: string;
    scheduleStartDateTime: object;
    scheduleEndDateTime: object;
    scheduleLeadWindow: number;
    partnerId: number = 0; // Id of Partner e.g. COCT

    // The instanceID included here as all scheduled instances are selected as an individual incident to show on the incidents list.
    // As instances share a incidentID, we need a unique identifier if only once instance needs to be updated.
    scheduleInstanceId: number = 0;

    /*The actual instances are only populated when an incident is created or the ENTIRE schedule updated*/
    scheduleInstances = [];

    actionsTaken = [];
    escalation = [];
    assets = [];
    documents = [];
    statusHistory = [];

    // [ArrayElementType("com.id.vo.CustomFieldValuesVO")]
    customFieldValues = [];

    // [ArrayElementType("com.id.vo.LoggedWorkVO")]
    loggedWork = [];

    // [ArrayElementType("com.id.vo.IncidentGPSVO")]
    incidentGPS = [];

    // Third Party Push Notifications - populated on front end when incident is saved
    // [ArrayElementType("com.id.vo.ThirdPartyPushNotificationVO")]
    thirdPartyPushNotifications = [];

    communicationId: number = 0; // only used when incident is saved from PC notification to pass along communicationID to php service
    linkedIncidentDesc: string; // Description of incident that is linked to this one via custom field 'Linked Incident ID'. 
    statusModified: number; // time that current status was activated
    impactStatusEscalationProcessed: number; // ID of last impact status escalation that was processed

    // [ArrayElementType("com.id.vo.FacialrecognitiondataVO")]
    facialRecognitionData = [];

    // [ArrayElementType("com.id.vo.AssignedResourcesVO")]
    assignedResources = [];

    incidentIdHold: number = 0; // temp holder for a new incident temp ID - used for uploding the dcumnets prior to saving the incident
    isChecked: boolean = false; // indicates if incident is checked in incident list view
}
