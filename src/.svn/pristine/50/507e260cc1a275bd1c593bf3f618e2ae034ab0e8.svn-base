import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { WorkflowService } from '../workflow/workflow.service';
import { ColumnMode, SelectionType } from '@swimlane/ngx-datatable'; // site selector
import { ChangeDetectorRef } from '@angular/core';
import { SiteVO } from 'app/shared/models/siteVO.model';
import { LookupsService } from 'app/shared/services/lookups.service';
import { map } from 'rxjs/operators';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { AddIncidentService } from 'app/shared/services/add-incident.service';
import { STEPS } from '../workflow/workflow.model';

@Component({
  selector: 'app-sites',
  templateUrl: './sites.component.html',
  styleUrls: ['./sites.component.css']
})

export class SitesComponent implements OnInit {
  title = 'Please select a site';
  siteTabData: any;  // personal: Personal;
  form: any;
  buttonDisabled = true;

  // site tree
  siteRows = [];
  lastIndex = 15;
  ColumnMode = ColumnMode;
  selectedSite = [];
  SelectionType = SelectionType;
  messages = {emptyMessage: 'Loading sites...'};

  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private addIncidentService: AddIncidentService,
      private workflowService: WorkflowService,
      private cd: ChangeDetectorRef,
      private lookupsService: LookupsService) { }

    ngOnInit() {
      this.siteTabData = this.addIncidentService.getSiteTabData();

      // if user returned to tab, fetch tree from stored form data
      if (this.siteTabData.siteTreeRows.length > 0) {
        this.siteRows = this.siteTabData.siteTreeRows;
        this.selectedSite = this.siteTabData.selectedSite;
        this.cd.markForCheck();
      // ON PAGE OPEN - fetch level 1 sites from local storage
      } else {

        // sites too big to load in local storage
        // this.siteRows = JSON.parse(sessionStorage.getItem('sites'));

        this.getLevel1Sites();
      }
    }

  // save form data
  saveData(form: any) {

      if (!form.valid) {
        return;
      }

      // store tree data in case user returns to tab
      this.siteTabData.siteTreeRows = this.siteRows;
      this.siteTabData.selectedSite = this.selectedSite;
      this.addIncidentService.setSiteTabData(this.siteTabData);

      // as this is the first step, it is valid in order for user to stay here. By default, first invalid step is the second one.
      const firstState = this.workflowService.getFirstInvalidStep(STEPS.categories);
      // this.router.navigateByUrl('/incidents/add-incident/categories', { relativeTo: this.route.parent, skipLocationChange: true });x
      this.router.navigate(['categories'], { relativeTo: this.route.parent, skipLocationChange: true });
  }

  // Get all level 1 sites - sites with parentId = 0
  getLevel1Sites() {

    this.lookupsService.getLookups('sites', 0)
    .pipe(
      map(responseData => {
        const childNodes = [];

        if (responseData.hasOwnProperty('result')) {
          // convert js responsedata to array of typed model objects
          for (let i = 0; i < responseData['result'].length; i++) {
            const jsonObj = responseData['result'][i];
            const newChild = new SiteVO().deserialize(jsonObj);
            newChild.treeStatus = 'collapsed';
            childNodes.push(newChild);
          }
      }

        if (childNodes.length === 0) {
          this.messages = {emptyMessage: 'No sites found.'};
        }

        return childNodes;
      })
    )
    .subscribe((childNodes) => {
        this.siteRows = [...childNodes];
        this.cd.detectChanges();
    });

  }

  // onClick of site tree item
  onTreeAction(event: any) {
    const index = event.rowIndex;
    const row = event.row;

    // if node currently collapsed, open & load children
    if (row.treeStatus === 'collapsed') {
      row.treeStatus = 'loading';

      // if children has not already been loaded, fetch from db
      if (!row.childrenPopulated) {
        this.lookupsService.getLookups('sites', row.id)
        .pipe(
          map(responseData => {

            console.log('GET CHILD SITES');
            console.log(responseData);

            const childNodes = [];

            if (responseData.hasOwnProperty('result')) {
              // convert js responsedata to array of typed model objects
              for (let i = 0; i < responseData['result'].length; i++) {
                const jsonObj = responseData['result'][i];
                const newChild = new SiteVO().deserialize(jsonObj);
                newChild.treeStatus = 'collapsed';
                childNodes.push(newChild);
              }
          }

            return childNodes;
          })
        )
        .subscribe((childNodes) => {
            // this.sitesTemp = [...allRecords]; // cache our list - used for filtering
            // this.rows = allRecords; // push our inital complete list
            // this.cd.markForCheck(); // add this to ensure new data is shown on screen

            row.treeStatus = 'expanded';
            row.childrenPopulated = true;
            this.siteRows = [...this.siteRows, ...childNodes];
            this.cd.detectChanges();
        });
      // if children has already been fetched, open node
      } else {
        row.treeStatus = 'expanded';
        this.siteRows = [...this.siteRows];
        this.cd.detectChanges();
      }

    // if node currently open, collapse node
    } else {
      row.treeStatus = 'collapsed';
      this.siteRows = [...this.siteRows];
      this.cd.detectChanges();
    }
  }

  // On SELECT of site
  onSelect({ selected }) {
    this.selectedSite.splice(0, this.selectedSite.length);
    this.selectedSite.push(...selected);

    console.log('SELECTED SITE: ');
    console.log(this.selectedSite[0].id);

    this.siteTabData.siteId = this.selectedSite[0].id;

    // enable NEXT button in wizard
    this.buttonDisabled = false;
  }

}
